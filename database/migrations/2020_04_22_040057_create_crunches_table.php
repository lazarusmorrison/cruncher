<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrunchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /*
    public function up()
    {
        Schema::connection('baystate-shared')->create('crunches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('city')->index();
            $table->string('name');
            $table->unsignedInteger('affected')->nullable();
            $table->longText('log')->nullable();
            $table->dateTime('ended_at')->nullable();
            $table->timestamps();
        });
    }
    */

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    /*
    public function down()
    {
        Schema::connection('baystate-shared')->dropIfExists('crunches');
    }
    */
}
