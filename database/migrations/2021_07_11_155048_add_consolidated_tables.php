<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConsolidatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /*
    public function up()
    {
        Schema::connection('consolidated')->create('commission_readings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('row_id')->unique();
            $table->unsignedInteger('sync_id')->nullable();
            $table->dateTime('synced_at')->nullable();

            
            $table->string('city')->nullable()->index();
            $table->unsignedInteger('usage_file_id')->index();
            $table->unsignedInteger('file_row')->nullable();
            $table->string('file_name')->nullable();

            $table->string('account_id')->index();
            $table->string('meter_number')->nullable();
            $table->string('customer_name')->nullable();
            $table->text('address')->nullable();
            $table->string('rate_class')->nullable();
            $table->string('base_rate_class')->nullable();
            $table->float('price', 15, 6)->nullable();
            $table->float('customer_payment', 15, 6)->nullable();
            $table->string('load_zone')->nullable();

            $table->date('usage_from_date')->nullable();
            $table->date('usage_to_date')->nullable();
            $table->float('usage_kwh', 15, 6)->nullable();

            $table->date('ldc_read_month')->nullable();
            $table->string('read_cycle')->nullable();
            $table->float('icap', 15, 6)->nullable();

            $table->date('corrected_ldc_read_month')->nullable();
            $table->string('corrected_read_cycle')->nullable();
            $table->float('corrected_icap', 15, 6)->nullable();
            $table->boolean('correction')->nullable();
            
            $table->text('all_fields')->nullable();

            $table->timestamps();
            // $table->index(['sync_id', 'city']);
            // $table->index(['sync_id', 'row_id', 'city']);
        });

        Schema::connection('consolidated')->create('usage_readings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('row_id')->unique();
            $table->unsignedInteger('sync_id')->nullable();
            $table->dateTime('synced_at')->nullable();
            
            $table->string('city')->nullable()->index();
            $table->unsignedInteger('usage_file_id')->index();
            $table->unsignedInteger('file_row')->nullable();
            $table->string('file_name')->nullable();

            $table->string('account_id')->index();
            $table->string('meter_number')->nullable();
            $table->string('customer_name')->nullable();
            $table->text('address')->nullable();
            $table->string('rate_class')->nullable();
            $table->string('base_rate_class')->nullable();
            $table->float('price', 15, 6)->nullable();
            $table->float('customer_payment', 15, 6)->nullable();
            $table->string('load_zone')->nullable();

            $table->date('usage_from_date')->nullable();
            $table->date('usage_to_date')->nullable();
            $table->float('usage_kwh', 15, 6)->nullable();

            $table->date('ldc_read_month')->nullable();
            $table->string('read_cycle')->nullable();
            $table->float('icap', 15, 6)->nullable();

            $table->date('corrected_ldc_read_month')->nullable();
            $table->string('corrected_read_cycle')->nullable();
            $table->float('corrected_icap', 15, 6)->nullable();
            $table->boolean('correction')->nullable();
            
            $table->text('all_fields')->nullable();

            $table->timestamps();
            // $table->index(['sync_id', 'city']);
            // $table->index(['sync_id', 'row_id', 'city']);
        });

        Schema::connection('consolidated')->create('combined_readings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('row_id')->unique();
            $table->unsignedInteger('sync_id')->nullable();
            $table->dateTime('synced_at')->nullable();
            
            $table->string('city')->nullable()->index();
            $table->unsignedInteger('usage_file_id')->index();
            $table->unsignedInteger('file_row')->nullable();
            $table->string('file_name')->nullable();

            $table->string('account_id')->index();
            $table->string('anonymized_id')->nullable()->index();
            $table->string('meter_number')->nullable();
            $table->string('customer_name')->nullable();
            $table->text('address')->nullable();
            $table->string('rate_class')->nullable();
            $table->string('base_rate_class')->nullable();
            $table->float('price', 15, 6)->nullable();
            $table->float('customer_payment', 15, 6)->nullable();
            $table->string('load_zone')->nullable();

            $table->date('usage_from_date')->nullable();
            $table->date('usage_to_date')->nullable();
            $table->float('usage_kwh', 15, 6)->nullable();

            $table->date('ldc_read_month')->nullable();
            $table->string('read_cycle')->nullable();
            $table->float('icap', 15, 6)->nullable();

            $table->date('corrected_ldc_read_month')->nullable();
            $table->string('corrected_read_cycle')->nullable();
            $table->float('corrected_icap', 15, 6)->nullable();
            $table->boolean('correction')->nullable();
            
            $table->text('all_fields')->nullable();

            $table->timestamps();
            // $table->index(['sync_id', 'city']);
            // $table->index(['sync_id', 'row_id', 'city']);
        });
    }

    public function down()
    {
        Schema::connection('consolidated')->dropIfExists('combined_readings');
        Schema::connection('consolidated')->dropIfExists('commission_readings');
        Schema::connection('consolidated')->dropIfExists('usage_readings');
    }
    */
}
