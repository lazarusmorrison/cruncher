<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoadZoneToMonthlyReadings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('monthly_readings', function (Blueprint $table) {
            $table->string('load_zone')->after('base_rate_class')->index()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('monthly_readings', function (Blueprint $table) {
            $table->dropColumn('load_zone');
        });
    }
}
