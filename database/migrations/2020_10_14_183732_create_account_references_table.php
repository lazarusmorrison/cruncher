<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /*
    public function up()
    {
        Schema::connection('baystate-shared')->create('account_references', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account_id')->index();
            $table->string('city')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->date('earliest')->nullable();
            $table->date('latest')->nullable();
            $table->boolean('multiple_cities')->default(false);
            $table->text('city_counts')->nullable();
            $table->unsignedInteger('total_count')->nullable();
            $table->unsignedInteger('valid_count')->nullable();
            $table->dateTime('counted_at')->nullable();
            $table->timestamps();
        });
    }
    */

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    /*
    public function down()
    {
        Schema::dropIfExists('account_references');
    }
    */
}
