<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Crunch extends Model
{
    protected $connection = 'baystate-shared';
    protected $dates = ['created_at', 'updated_at', 'ended_at'];
    // protected $casts = ['log' => 'string'];

    public function addToLog($str)
    {
    	$newline = Carbon::now()->format('Y-m-d H:i:s')." ".$str."\n";
    	echo $newline;
        if ($this->log) {
        	$this->log = $this->log.$newline;
        } else {
            $this->log = $newline;
        }
    	$this->save();
    }
    public function complete()
    {
    	$this->ended_at = Carbon::now();
    	$this->save();
    }
    public function end()
    {
    	$this->complete();
    }
}
