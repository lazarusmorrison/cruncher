<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesUsageReading extends BaseUsage
{
    protected $table = 'sales_usage_readings';
}
