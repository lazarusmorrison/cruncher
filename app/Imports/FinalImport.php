<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class FinalImport implements WithMultipleSheets, WithChunkReading
{
    /**
    * @param Collection $collection
    */
    
    public function sheets(): array
    {
        return [
            'Account Level kWh' => new ALKWHImport(),
        ];
    }
    public function chunkSize(): int
    {
        return 1000;
    }
}
