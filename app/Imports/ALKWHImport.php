<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
Use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithProgressBar;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Carbon\Carbon;
use App\Account;


class ALKWHImport implements ToCollection, WithStartRow
{
    /**
    * @param Collection $collection
    */

    public function collection(Collection $rows)
    {
    	//dd($rows->count());
    	$account_ids = [];
        foreach ($rows as $row) 
        {

            $account_ids[$row[0]]['readings'][] = $row;
            $account_ids[$row[0]]['rate_class'] = $row[1];
            $account_ids[$row[0]]['read_cycle'] = $row[3];

        }
        //dd(collect($account_ids));
        $accounts = Account::whereIn('anonymized_id', collect($account_ids)->keys())->with('usageReadings');
        //dd($accounts->count());
        foreach ($accounts->get() as $account) {
        	if (!$account_ids[$account->anonymized_id]['rate_class']
        		&& !$account_ids[$account->anonymized_id]['read_cycle']) {
        		echo "SKIPPING ".$account->anonymized_id."\n";
        		continue;
        		//dd($account_ids[$account->anonymized_id]);
        	}
        	//echo $account->id." ".$account_ids[$account->anonymized_id]['rate_class']." ".$account_ids[$account->anonymized_id]['read_cycle']."\n";
        	$account->rate_class = $account_ids[$account->anonymized_id]['rate_class'];
        	$account->read_cycle = $account_ids[$account->anonymized_id]['read_cycle'];
        	foreach ($account->usageReadings as $ur) {
        		$ur->corrected_rate_class = $account->rate_class;
        		$ur->corrected_read_cycle = $account->read_cycle;
        		if ($ur->isDirty()) {
        			if ($ur->corrected_ldc_read_month) {
        				echo "\t".$ur->account_id.", ".$ur->corrected_ldc_read_month->format('Y-m-d')."\n";
        			} else {
        				echo "\t".$ur->account_id.", ".$ur->id."\n";	
        			}
                    // so it can be rerun for corrections
        			$ur->monthly_processed_at = null;
        			$ur->save();
        		}
        	}
        	if ($account->rate_class != $account_ids[$account->anonymized_id]['rate_class']) {
        		dd($account_ids[$account->anonymized_id]);
        	}
        	if ($account->isDirty()) {
        		echo $account->anonymized_id.", ".$account->id."\n";
        		$account->save();
        	}
        	
        }
        echo "Another 1000\n";
        //dd($account_ids);
    }

    public function startRow(): int
    {
		return 5;
	}
}
