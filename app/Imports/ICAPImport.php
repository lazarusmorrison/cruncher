<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
Use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Carbon\Carbon;
use App\Account;


class ICAPImport implements ToCollection, WithChunkReading, WithStartRow, WithMultipleSheets
{
    public $year;
    public $start_row;
    public $account_column;
    public $icap_column;

    public $column_names;
    /**
    * @param Collection $collection
    */
    public function __construct($year, $start_row, $account_column, $icap_column, $sheet)
    {
        $this->year = $year;
        $this->start_row = $start_row;
        $this->account_column = $account_column;
        $this->icap_column = $icap_column;
        $this->sheet = $sheet;
    }

    public function sheets(): array
    {
        // Specify the sheet name and associate it with this import class.
        // If you have multiple sheets to import, you can specify them here as well,
        // e.g., 'Sheet2' => new AnotherSheetImport()
        return [
            ''.$this->sheet => $this,
        ];
    }

    public function collection(Collection $rows)
    {
        //dd($this->year);
    	//dd($rows->count());
    	$account_ids = [];
        foreach ($rows as $row) 
        {
            if (!$this->column_names) {
                $this->column_names = $row;
                continue;
            }
            $indexrow = [];
            foreach ($this->column_names as $idx => $colname) {
                $indexrow[$colname] = $row[$idx];
            }
            print_r($indexrow);
            $account_id = str_pad($indexrow[$this->account_column], 10, '0', STR_PAD_LEFT);
            $account_id = str_replace(':001', '', $account_id);
            $account_ids[$account_id] = $indexrow[$this->icap_column];
        }
        //dd($account_ids);

        $accounts = Account::whereIn('id', collect($account_ids)->keys());
        
        //dd($accounts->get()->first());
        //dd($accounts->count());
        foreach ($accounts->get() as $account) {


            $temp_account_id = str_replace(':001', '', $account->id);
            

            if (!isset($account_ids[$temp_account_id])) {
                echo "Account not found: ".$account->id."\n";
                continue;
            }
            $new_icap = $account_ids[$temp_account_id];
            $field = 'icap_'.$this->year;
            //dd($field);

            foreach ($account->usageReadingsByIcapYear($this->year)->get() as $ur) {
                
                if ($ur->corrected_icap."" != $new_icap."" && is_numeric($new_icap)) {
                    //dd($ur, $new_icap);
                    echo "\t".$ur->account_id.", ".$ur->corrected_ldc_read_month->format('Y-m-d')." ".$ur->corrected_icap." vs. ".$new_icap."\n";
                    $ur->corrected_icap = $new_icap;
                    

                    $ur->save();
                }
            }

//            dd(abs($account->$field - $new_icap));

            if ($account->$field."" != $new_icap."" && is_numeric($new_icap)) {
                //dd($account->$field."", $new_icap."");
                //dd(abs($account->$field - $new_icap)." ", '0.0 ');
                echo $account->id.", ".$account->$field.", ".$new_icap."\n";
                $account->$field = $new_icap;
                $account->save();
            }
            
        }
        echo "Another 100\n";

    }
    public function chunkSize(): int
    {
        return 100;
    }
    public function startRow(): int
    {
        return $this->start_row;
    }

}
