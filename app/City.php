<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use DB;

class City extends Model
{
    protected $connection = 'baystate-shared';

    protected static function boot()
    {
    	//dd("Laz");
        static::addGlobalScope('dbexists', function (Builder $builder) {

        	$tables = DB::table('INFORMATION_SCHEMA.SCHEMATA')
                  ->selectRaw("SCHEMA_NAME")
                  ->where("SCHEMA_NAME", 'LIKE', 'bsc_%')
                  ->pluck('SCHEMA_NAME');
            //dd($tables);
            $valid_cities = [];
            foreach ($tables as $table) {
            	$valid_cities[] = str_replace('bsc_', '', $table);
            }

            $builder->whereIn('name', $valid_cities);
        });
        parent::boot();
    }
}
