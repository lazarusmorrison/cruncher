<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsageReading extends BaseUsage
{
    protected $table = 'usage_readings';

    public function getTable()
	{
		if (session('usage_table_override')) {
			return session('usage_table_override');
		}
		return $this->table;
	}
}
