<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AllComparison extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:all_comparison {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('city')) {
            $city = $this->option('city');
        } else {
            dd("forgot city dumbass, like php artisan crunch:all_comparison --city=natick");
        }

        $this->call('crunch:sales',             ['--city' => $city]);
        $this->call('crunch:commission',        ['--city' => $city]);
        $this->call('crunch:master',            ['--city' => $city]);
    }
}
