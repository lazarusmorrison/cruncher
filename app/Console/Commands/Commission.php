<?php

namespace App\Console\Commands;

use DB;
use App\City;
use App\UsageReading;
use Illuminate\Console\Command;

class Commission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:commission {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs every crunch but on commission table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('city')) {
            $cities = City::where('name', strtolower($this->option('city')))->get();
        } else {
            $cities = City::orderBy('name')->get();
        }

        foreach ($cities as $city) {
            echo "\n===========> ".strtoupper($city->name)." <===========\n";
            $db = "bsc_".$city->name;
            DB::purge('mysql');
            config(['database.connections.mysql.database' => $db]);

            session(['usage_table_override' => 'commission_usage_readings']);

            //dd(UsageReading::latest()->first());
            //$this->call('crunch:copy',              ['--city' => $city->name]);
            //$this->call('crunch:accounts',          ['--city' => $city->name]);
            $this->call('crunch:dupes',             ['--city' => $city->name]);
            $this->call('crunch:read_cycle',        ['--city' => $city->name]);
            $this->call('crunch:ldc_month',         ['--city' => $city->name]);
            $this->call('crunch:negatives',         ['--city' => $city->name]);
            $this->call('crunch:use_from_date',     ['--city' => $city->name]);
            $this->call('crunch:accounts',          ['--city' => $city->name]);
            $this->call('crunch:rate_class',        ['--city' => $city->name]);
            $this->call('crunch:icap',              ['--city' => $city->name]);
            //
            $this->call('crunch:yearly_icaps',      ['--city' => $city->name]);
        }
    }
}
