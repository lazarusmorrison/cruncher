<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UsageReading;
use App\SalesUsageReading;
use App\CommissionUsageReading;
use DB;
use App\Crunch;
use Carbon\Carbon;
use App\Traits\CrunchTrait;

class ClearCancelled extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:clear_cancelled {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears out readings with null usage and very long from/to dates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $city = $this->startCrunch('clear_cancelled', $this->option('city'));

        $sales = SalesUsageReading::whereNull('usage_kwh')
                           ->whereRaw('DATEDIFF(usage_to_date, usage_from_date) > 60')
                           ->count();
        echo "Count - S: ".$sales."\n"; 

        SalesUsageReading::whereNull('usage_kwh')
                           ->whereRaw('DATEDIFF(usage_to_date, usage_from_date) > 60')
                           ->delete();

        $comms = CommissionUsageReading::whereNull('usage_kwh')
                           ->whereRaw('DATEDIFF(usage_to_date, usage_from_date) > 60')
                           ->count();
        echo "Count - C: ".$comms."\n"; 

        CommissionUsageReading::whereNull('usage_kwh')
                           ->whereRaw('DATEDIFF(usage_to_date, usage_from_date) > 60')
                           ->delete();

        $usage = UsageReading::whereNull('usage_kwh')
                           ->whereRaw('DATEDIFF(usage_to_date, usage_from_date) > 60')
                           ->count();
        echo "Count - U: ".$usage."\n"; 

        UsageReading::whereNull('usage_kwh')
                           ->whereRaw('DATEDIFF(usage_to_date, usage_from_date) > 60')
                           ->delete();

        echo "Done.\n";

        // UsageReading::whereNull('usage_kwh')
        //             ->where('total_days', '>', 60)
        //             ->delete();
    }
}
