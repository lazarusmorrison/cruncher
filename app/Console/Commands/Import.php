<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\City;
use App\DataImport;
use Carbon\Carbon;
use App\CommissionUsageReading;
use App\SalesUsageReading;
use App\MasterMonthlyAccount;
use Rap2hpoutre\FastExcel\FastExcel;

use App\Traits\CrunchTrait;

class Import extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:import {--city=} {--id=} {--redo_blanks} {--start=} {--master} {--extradata} {--recent}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks data_imports tables and imports anything with a sheet chosen.';

    protected $consecutive_duds = 0;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function getFirstRow($import = null)
    {
      if (!$import) {
        // Test
        $dir = storage_path('app/header_test.xlsx');
      } else {
        $dir = $import->full_file_path;
      }

      $r = 1;
      $first_row = 1;

      try {

        $result = (new FastExcel)->sheet($import->which_sheet)->withoutHeaders()
                         ->import($dir, 
                                  function ($row) use (&$counts, &$r, &$first_row) {

                                    $num_total = count($row);
                                    print_r($row);

                                    $num_blanks = collect($row)->reject(function ($item) {
                                       return (!$item) ? false : true;
                                    })->count();

                                    if ($num_blanks < 3) {
                                      echo 'Row '.$r.' has '.$num_blanks.' blanks'."\n";
                                      $first_row = $r;
                                      $this->thisFunctionIsMeantToStopTheLoop();
                                      return;
                                    }

                                    if ($r >= 20 || $first_row > 1) {
                                      $this->thisFunctionIsMeantToStopTheLoop();
                                      return;
                                    }

                                    $r++;

                                  }
                          );

        } catch (\Exception $e) {
          echo $e->getMessage()."\n";
          echo "Found First Row: ".$first_row."\n";
          //return null;
          //dd($e->getMessage(), $first_row);
        }


        return $first_row;
    }

    public function handle()
    {
        //$this->startCrunch('import', $city_str);
        $cities = City::orderBy('name')->get();

        if ($this->option('city')) {
            $cities = City::where('name', $this->option('city'))->orderBy('name')->get();
        }

        if ($this->option('start')) {
            $cities = City::where('name', '>=', $this->option('start'))->orderBy('name')->get();
        }

        echo "\n****************************** ASSIGN SHEETS ******************************\n\n";
        
        foreach ($cities as $city) {
            echo "===================================> ".strtoupper($city->name)."\n";

            $db = 'bsc_'.strtolower($city->name);
            DB::purge('mysql');
            config(['database.connections.mysql.database' => $db]);

            //$this->startCrunch('import', $city->name);

            
            $missing_sheet_imports = DataImport::where('city_id', $city->id)
                                      ->where('import_done', false)
                                      ->whereNull('which_sheet')
                                      ->get();

            foreach ($missing_sheet_imports as $msi) {
              $previous_sheets = [];
              $matching_imports = DataImport::where('city_id', $city->id)
                                        ->where('import_done', true)
                                        ->where('type', $msi->type)
                                        ->whereNotNull('which_sheet')
                                        ->get();
              foreach ($matching_imports as $mi) {
                $sheet_arr = $mi->sheets;
                //dd($sheet_arr);
                $sheetname = $sheet_arr[$mi->which_sheet - 1];
                //dd($sheetname);
                if (isset($previous_sheets[$sheetname])) {
                  $previous_sheets[$sheetname] += 1;
                } else {
                  $previous_sheets[$sheetname] = 1;
                }
              }
              arsort($previous_sheets);
              $which_sheet = null;
              $which_name = null;
              foreach ($previous_sheets as $ps => $pscount) {
                if (!$msi->sheets) {
                  continue;
                }
                if (is_string($msi->sheets)) {
                  //dd(json_decode($msi->sheets));
                  $msi->sheets = json_decode($msi->sheets);
                  $msi->save();
                }
                try {
                  if (in_array($ps, $msi->sheets)) {
                    foreach ($msi->sheets as $mkey => $msheet) {
                      if ($ps == $msheet) {
                        $which_sheet = $mkey + 1;
                        $which_name = $ps;
                      }
                    }
                  }
                } catch (\Exception $e) {
                  dd($msi, $e->getMessage());
                }
              }
              if ($which_sheet) {
                //dd($msi->sheets, $which_sheet);
                $msi->which_sheet = $which_sheet;
                echo "Setting Sheet to $which_name\n";
                //dd($msi);
                $msi->save();
              }
              
            }

        }
        echo "\n****************************** PREVIEW ******************************\n\n";
        
        foreach ($cities as $city) {
            echo "===================================> ".strtoupper($city->name)."\n";

            $db = 'bsc_'.strtolower($city->name);
            DB::purge('mysql');
            config(['database.connections.mysql.database' => $db]);
            
            if ($this->option('id')) {
              $data_imports = DataImport::where('id', $this->option('id'))->get();
            } else if ($this->option('redo_blanks')) {
              $all_data_imports = DataImport::where('city_id', $city->id)
                                        ->whereNotNull('which_sheet')
                                        ->get();
              $data_imports = collect([]);
              foreach ($all_data_imports as $di) {
                if ($di->type == 'sales' || $di->type == 'commission') {
                  if ($di->usageReadings()->count() < 1) {
                    $data_imports[] = $di;
                  }
                }
                
              }
            } else if ($this->option('recent')) {
              $data_imports = DataImport::where('city_id', $city->id)
                                        ->where('import_done', false)
                                        ->where('month', '>', Carbon::today()->subMonths(14))
                                        ->whereNotNull('which_sheet')
                                        ->get();
            } else {
              $data_imports = DataImport::where('city_id', $city->id)
                                        ->where('import_done', false)
                                        ->whereNotNull('which_sheet')
                                        ->get();
            }
            


            foreach ($data_imports as $data_import) {
                echo strtoupper($data_import->type).": ".$data_import->original_name."\n";
                //dd($data_import);
            }

        }

        echo "\n****************************** STARTING ******************************\n\n";

        foreach ($cities as $city) {
            echo "===================================> ".strtoupper($city->name)."\n";

            $this->startCrunch('import', $city->name);
            
            if ($this->option('id')) {
              $data_imports = DataImport::where('id', $this->option('id'))->get();
            } else if ($this->option('redo_blanks')) {
              $all_data_imports = DataImport::where('city_id', $city->id)
                                        ->whereNotNull('which_sheet')
                                        ->get();
              $data_imports = collect([]);
              foreach ($all_data_imports as $di) {
                if ($di->type == 'sales' || $di->type == 'commission') {
                  if ($di->usageReadings()->count() < 1) {
                    $data_imports[] = $di;
                  }
                }
                
              }
            } else if ($this->option('recent')) {
              $data_imports = DataImport::where('city_id', $city->id)
                                        ->where('import_done', false)
                                        ->where('month', '>', Carbon::today()->subMonths(14))
                                        ->whereNotNull('which_sheet')
                                        ->get();
            } else {
              $data_imports = DataImport::where('city_id', $city->id)
                                        ->where('import_done', false)
                                        ->whereNotNull('which_sheet')
                                        ->get();
            }


            foreach ($data_imports as $data_import) {
                echo "Starting ".strtoupper($data_import->type).": ".$data_import->original_name."\n";

                if (!file_exists($data_import->full_file_path)) {
                  echo "FILE MISING: ".$data_import->full_file_path."\n";
                  continue;
                }

                // try {
                    if ($data_import->type == 'sales') {
                        $this->excelImport_Sales($city, $data_import);
                    }
                    if ($data_import->type == 'commission') {
                        $this->excelImport_Comm($city, $data_import);
                    }
                    if ($data_import->type == 'master') {
                      if ($this->option('master')) {
                        $this->excelImport_Master($city, $data_import);
                      }
                    }
                // } catch (\Exception $e) {
                //     echo "Some big error or something: ".$e->getMessage()."\n";
                // }

                if (strpos('a'.$data_import->name, 'COMBINED') > 0) {
                  echo "FILTERING: php artisan crunch:filter --city=".$city->name." --import=".$data_import->id." --keep=".$city->name."\n";

                  $this->call('crunch:filter',['--city'   => $city->name,
                                               '--import' => $data_import->id,
                                               '--keep'   => $city->name]);
                }
                //dd($data_import);
            }
            $this->endCrunch();
        }
    }

    
    public function excelImport_Comm($city, $import)
    {     

        $rowcount = 0;
        $at_least_one = false;

        $first_row = $this->getFirstRow($import);
        //dd($first_row);
        if (!$first_row) {
          return;
        }

        $import->importIsDone();

        $result = (new FastExcel)->startRow($first_row)
                                 ->sheet($import->which_sheet)
                                 ->import($import->full_file_path,
          function ($row) use (&$rowcount, &$at_least_one, $import, $city) {

          //dd($row);
            if ($this->consecutive_duds > 1000) {
              $this->consecutive_duds = 0;
              throw new \Exception('Too many blank fields', 1);
              return;
            }
            //dd($row);
            $rowcount++;
            $colcount = count($row);

            // print_r($row);
            // return;

            if ($rowcount == 2) {
              //dd($import);
              echo "========================> ".$import->name."\n";
              echo "========================> ROW 1\n";
              print_r($row);
              //dd("Laz");
            }

            //dd($row);

            //print_r($row);

            if ($colcount < 5) {
              echo "skipping incomplete row $rowcount \n";
              print_r($row);
              return;
            }

            
            if ($rowcount % 37 == 0) {
                echo "$rowcount \r";
            }

            $hash = md5(serialize($row));

            $usage_reading = CommissionUsageReading::where('file_row', $rowcount)
                                            ->where('usage_file_id', $import->id)
                                            ->first();

            if (!$usage_reading) {
                $usage_reading = new CommissionUsageReading;
            }

            // META
            $usage_reading->city =                   $city->name;
            $usage_reading->usage_file_id =          $import->id;
            $usage_reading->file_row =               $rowcount;
            $usage_reading->row_hash =               $hash;

            try {
              // ACCOUNT


                foreach ($row as $index => $val) {

                    $this->setValue($usage_reading, $row, $index);
                    //echo $index." - "." ".$usage_reading->account_id."\n";
                }

                //dd($usage_reading, $row);
                //dd($row);
                if (!is_numeric($usage_reading->icap)) {
                  // people putting weird dates into icap column
                  $usage_reading->icap = null;
                }
                if (!is_numeric($usage_reading->read_cycle)) {
                  // people putting weird dates into read_cycle column
                  $usage_reading->read_cycle = null;
                }
                if (strlen($usage_reading->account_id) > 11) {
                  // No long account ids you dummies.
                  $usage_reading->account_id = substr($usage_reading->account_id, 0, 11);
                }

                $usage_reading->all_fields = $row;
              
                //dd($usage_reading, $row);
                $this->nullifyEmpty($usage_reading);

                

                if (!$usage_reading->account_id) {
                  //dd($usage_reading);
                  $this->consecutive_duds++;
                  if (count($usage_reading->all_fields) > 3) {
                    print_r($usage_reading->all_fields);
                    echo $import->id." - ".$import->original_name."\n";
                    echo "****** NO ACCOUNT ID, SKIPPING! ************\n";
                    //dd($usage_reading, count($usage_reading->all_fields));
                  }
                    echo $import->id." - ".$import->original_name."\n";
                    echo "dud in row $rowcount ".print_r($usage_reading->all_fields)."\n";
                    
                    return;
                }
                $this->consecutive_duds = 0;

                $usage_reading->account_id = str_pad($usage_reading->account_id, 10, '0', STR_PAD_LEFT);

                if (!$usage_reading->usage_to_date) {
                    //dd($usage_reading, $row, "Missing Usage To");
                }

                //dd($usage_reading);
                
            } catch (\Exception $e) {
                dd($row, $e->getMessage(), $index);
            }

            // CHECK IF DUPLICATE READING ALREADY THERE

            $dupe = CommissionUsageReading::where('account_id', $usage_reading->account_id)
                                     ->where('usage_kwh', $usage_reading->usage_kwh)
                                     ->where('usage_to_date', $usage_reading->usage_to_date)
                                     ->first();

            if ($dupe) {
              echo "Found duplicate reading for ".$usage_reading->account_id." ".$usage_reading->usage_to_date."\r";
            } else {

              $at_least_one = true;
              $usage_reading->save();
            }

            return;
            
        });


        // if ($at_least_one) {
          $import->setImportCount($rowcount);
          $import->importIsDone();
        // }

    }
    public function excelImport_Sales($city, $import)
    {     

        $rowcount = 0;
        $at_least_one = false;

        $first_row = $this->getFirstRow($import);
        if (!$first_row) {
          return;
        }

        //$import->importIsDone();

        $result = (new FastExcel)->startRow($first_row)
                                 ->sheet($import->which_sheet)
                                 ->import($import->full_file_path,
            function ($row) use (&$rowcount, &$at_least_one, $import, $city) {
            //dd($row);
            if ($this->consecutive_duds > 1000) {
              $this->consecutive_duds = 0;
              $import->importIsDone();
              throw new \Exception('Too many blank fields', 1);
              return;
            }
            //dd($row);
            $rowcount++;
            $colcount = count($row);

            // print_r($row);
            // return;

            if ($rowcount == 2) {
              //dd($import);
              echo "========================> ".$import->name."\n";
              echo "========================> ROW 1\n";
              print_r($row);
              //dd("Laz");
            }

            //dd($row);

            //print_r($row);

            if ($colcount < 5) {
              echo "skipping incomplete row $rowcount \n";
              print_r($row);
              return;
            }

            
            if ($rowcount % 37 == 0) {
                echo "$rowcount \r";
            }

            $hash = md5(serialize($row));

            $usage_reading = null;
            if (!$this->option('extradata')) {
              $usage_reading = SalesUsageReading::where('file_row', $rowcount)
                                            ->where('usage_file_id', $import->id)
                                            ->first();
            }

            if (!$usage_reading) {
                $usage_reading = new SalesUsageReading;
            }

            // META
            $usage_reading->city =                   $city->name;
            $usage_reading->usage_file_id =          $import->id;
            $usage_reading->file_row =               $rowcount;
            $usage_reading->row_hash =               $hash;

            try {
              // ACCOUNT


                foreach ($row as $index => $val) {
                    $this->setValue($usage_reading, $row, $index);
                }

                if ($usage_reading->usage_to_date > Carbon::today()) {
                  dd($usage_reading, $row);
                }
                //dd($usage_reading);
                //dd($row);

                $usage_reading->all_fields = $row;
              
                //dd($usage_reading, $row);
                $this->nullifyEmpty($usage_reading);

                if (!is_numeric($usage_reading->icap)) {
                  // people putting weird dates into icap column
                  $usage_reading->icap = null;
                }
                if (!is_numeric($usage_reading->read_cycle)) {
                  // people putting weird dates into read_cycle column
                  $usage_reading->read_cycle = null;
                }
                if (strlen($usage_reading->account_id) > 11) {
                  // No long account ids you dummies.
                  $usage_reading->account_id = substr($usage_reading->account_id, 0, 11);
                }

                if (!$usage_reading->account_id) {
                  //dd($usage_reading);
                  $this->consecutive_duds++;
                  if (count($usage_reading->all_fields) > 3) {
                    print_r($usage_reading->all_fields);
                    echo $import->id." - ".$import->original_name."\n";
                    echo "****** NO ACCOUNT ID, SKIPPING! ************\n";
                    //dd($usage_reading, count($usage_reading->all_fields));
                  }
                  echo $import->id." - ".$import->original_name."\n";
                    echo "dud in row $rowcount ".print_r($usage_reading->all_fields)."\n";
                    
                    return;
                }
                $this->consecutive_duds = 0;

                $usage_reading->account_id = str_pad($usage_reading->account_id, 10, '0', STR_PAD_LEFT);

                if (!$usage_reading->usage_to_date) {
                  return;
                  // dd($usage_reading, $row, "Missing Usage To");
                }

                //dd($usage_reading);
                
            } catch (\Exception $e) {
                dd($row, $e->getMessage(), $index);
            }

            // CHECK IF DUPLICATE READING ALREADY THERE

            
              if ($this->option('extradata')) {
                $original = SalesUsageReading::where('account_id', $usage_reading->account_id)
                                                  ->where('usage_file_id', $import->id)
                                                  ->first();

                if ($original) {

                  foreach (['icap', 'base_rate_class', 'customer_name'] as $field) {
                    if ($usage_reading->$field != $original->$field) {
                      $original->$field = $usage_reading->$field;
                    }
                    $dirty = $original->getDirty();
                    if (count($dirty) > 0) {
                      //dd($original, $usage_reading, $dirty);
                      //echo "Original: ".$original->account_id."\n";
                      
                      try {
                        print_r($dirty);
                        $original->save();
                      } catch (\Exception $e) {
                        dd($e, $original);
                      }
                      return;
                    
                    }
                    
                  } 
                  
                } else {
                  echo "Extra data, no match found.\n";
                  //dd($usage_reading, $import);
                }

              }
            


            $dupe = SalesUsageReading::where('account_id', $usage_reading->account_id)
                                     ->where('usage_kwh', $usage_reading->usage_kwh)
                                     ->where('usage_to_date', $usage_reading->usage_to_date)
                                     ->first();

            if ($dupe) {
              echo "Found duplicate reading for ".$usage_reading->account_id." ".$usage_reading->usage_to_date."\r";

  
            } else {

              if ($usage_reading->usage_to_date > Carbon::today()) {
                dd($usage_reading, $row);
              }
              if (!$usage_reading->usage_to_date) {
                  dd($usage_reading, $row, "Missing Usage To");
                echo $import->original_name.": Account ".$usage_reading->account_id.": MISSING USAGE TO, IGNORING \n";
                return;
              }
              //dd($row, $usage_reading);
              $at_least_one = true;
              $usage_reading->save();
            }

            return;
            
        });



          $import->setImportCount($rowcount);
          $import->importIsDone();
        

    }
    public function excelImport_Master($city, $import)
    {     
        $rowcount = 0;


        $first_row = $this->getFirstRow($import);
        if (!$first_row) {
          return;
        }

        $import->importIsDone();

        $result = (new FastExcel)->startRow($first_row)
                                 ->sheet($import->which_sheet)
                                 ->import($import->full_file_path, 
          function ($row) use (&$rowcount, $import, $city) {
        
            //dd($row);
            $rowcount++;
            if ($rowcount % 37 == 0) {
                echo $rowcount."\r";
            }

            if ($this->consecutive_duds > 1000) {
              $this->consecutive_duds = 0;
              throw new \Exception('Too many blank fields', 1);
              return;
            }



            $hash = md5(serialize($row));

            //dd($row, $import);

            $master_monthly = MasterMonthlyAccount::where('file_row', $rowcount)
                                            ->where('usage_file_id', $import->id)
                                            ->first();

            if (!$master_monthly) {
                $master_monthly = new MasterMonthlyAccount;
            }

            // META
            $master_monthly->city =                   $city->name;
            $master_monthly->usage_file_id =          $import->id;
            $master_monthly->file_row =               $rowcount;
            $master_monthly->row_hash =               $hash;

            $master_monthly->month = $import->month;

            //dd($row);

            try {
              // ACCOUNT

                foreach ($row as $index => $val) {
                    $this->setValue($master_monthly, $row, $index);
                }
                //dd($master_monthly);

                $master_monthly->all_fields = $row;
              
                //dd($master_monthly, $row);
                $this->nullifyEmpty($master_monthly);

                if (!$master_monthly->account_id) {
                    echo "No account number found.\n";
                    return;
                }

                $master_monthly->account_id = str_pad($master_monthly->account_id, 10, '0', STR_PAD_LEFT);

                //dd($master_monthly);

                if (!$master_monthly->account_id) {
                  //dd($master_monthly);
                  $this->consecutive_duds++;
                  if (count($master_monthly->all_fields) > 3) {
                    print_r($master_monthly->all_fields);
                    echo $import->id." - ".$import->original_name."\n";
                    echo "****** NO ACCOUNT ID, SKIPPING! ************\n";
                    //dd($master_monthly, count($master_monthly->all_fields));
                  }
                  echo $import->id." - ".$import->original_name."\n";
                    echo "dud in row $rowcount ".print_r($master_monthly->all_fields)."\n";
                    
                    return;
                }
                $this->consecutive_duds = 0;
                
            } catch (\Exception $e) {
                dd($row, $e->getMessage(), $index);
            }
            $master_monthly->save();
            return;
            
        });

        $import->setImportCount($rowcount);
        $import->importIsDone();

    }

    function convertExcelDate($dateValue) {    

      $unixDate = ($dateValue - 25569) * 86400;
      return gmdate("Y-m-d", $unixDate);

    }

    public function setValue(&$obj, $row, $index)
    {

      if (!$obj->address) {
        $obj->address = [];
      }
      $address = $obj->address;

      if (is_object($row[$index])) {
        $row[$index] = $row[$index]->format('Y-m-d');
      }

      // Convert weird excel date (44230)
      
      if (stripos($index, 'date')) {
        if (is_numeric($row[$index])) {
          if ($row[$index] > 10000 && $row[$index] < 50000) {
            $row[$index] = $this->convertExcelDate($row[$index]);
          }
        }
      }

      if (strtolower(trim($row[$index])) == 'null') {
        $row[$index] = null;
      }

      if ($index == 'PremiseID') {
        //dd($row);
      }

      if (preg_match('/(\d{1,2})\/(\d{4})/', $row[$index], $output)) {
        // date in format 1/2022, carbon no likey
        $m = $output[1];
        $y = $output[2];
        $row[$index] = $m.'/1/'.$y;
      }

      switch ($index) {
        case "":

          if (strlen($row[$index]) == 11 && is_numeric($row[$index])) {
            $obj->account_id = $row[$index];
            break;
          }
          break;
        case "Utility Account Number":
          $obj->account_id = $row[$index];
          break;
        case "Utility Acnt Number":
          $obj->account_id = $row[$index];
          break;
        case "Service Account No":
          $obj->account_id = $row[$index];
          break;
        case "Service Account":
          $obj->account_id = $row[$index];
          break;
        case "SERVICEACCOUNT":
          $obj->account_id = $row[$index];
          break;
        case "UtilityAccountNumber":
          $obj->account_id = $row[$index];
          break;
        case "LDC Account Number": 
          $obj->account_id = $row[$index];
          break;
        case "LDC ACCOUNT NUMBER":
          $obj->account_id = $row[$index];
          break;
        case "UDCAccountNumber":
          $obj->account_id = $row[$index];
          break;
        case "Accountnumber":
          $obj->account_id = $row[$index];
          break;
        case "AccountNumber":
          $obj->account_id = $row[$index];
          break;
        case "AccountNo": 
          $obj->account_id = $row[$index];
          break;
        case "Accountno": 
          $obj->account_id = $row[$index];
          break;
        case "PremiseID": 
        
          $obj->account_id = $row[$index];

          break;
        case "Account #": 
          $obj->account_id = $row[$index];
          break;
        case "Account .":
          $obj->account_id = $row[$index];
          break;
        case "Account#": 
          $obj->account_id = $row[$index];
          break;
        case "MeterNumber": 
          $obj->meter_number = $row[$index];
          break;
          
        case 'Customer Name':
          $obj->customer_name = $row[$index];
          break;
        case 'Customer Last Name':
          $obj->customer_name = $row[$index];
          break;
        case 'Customer':
          $obj->customer_name = $row[$index];
          break;

        case 'Last Name':
          if (!$obj->customer_name) {
            $obj->customer_name = $row[$index];
          }
          break;
          
        case "Rate Class":
          $obj->rate_class = $row[$index];
          $obj->base_rate_class = substr(str_replace(['-', ' '], '', $row[$index]), 0, 2);
          break;

        case "usage_rate_class":
          $obj->rate_class = $row[$index];
          $obj->base_rate_class = substr(str_replace(['-', ' '], '', $row[$index]), 0, 2);
          break;

        case "ECIUtility Rate Code":
          $obj->rate_class = $row[$index];
          $obj->base_rate_class = substr(str_replace(['-', ' '], '', $row[$index]), 0, 2);
          break;
        case "RATE CLASS":
          $obj->rate_class = $row[$index];
          $obj->base_rate_class = substr(str_replace(['-', ' '], '', $row[$index]), 0, 2);
          break;
        case "ECIUtility Rate Code":
          $obj->rate_class = $row[$index];
          $obj->base_rate_class = substr(str_replace(['-', ' '], '', $row[$index]), 0, 2);
          break;
        case 'ProfileType':
          $obj->rate_class = $row[$index];
          $obj->base_rate_class = substr(str_replace(['-', ' '], '', $row[$index]), 0, 2);
          break;
        case "UtilityLoadProfile":
          $obj->rate_class = $row[$index];
          $obj->base_rate_class = substr(str_replace(['-', ' '], '', $row[$index]), 0, 2);
          break;
        case "Service Street and Number":
          $address['street'] = $row[$index];
          $obj->address = $address;
          break;
        case "Address":
          $address['street'] = $row[$index];
          $obj->address = $address;
          break;
        case "Service Address":
          $address['street'] = $row[$index];
          $obj->address = $address;
          break;
        case "Service Address 1":
          $address['street'] = $row[$index];
          $obj->address = $address;
          break;
        case "SvcAddress":
          $address['street'] = $row[$index];
          $obj->address = $address;
          break;
          

        case "Service City":
          $address['city'] = $row[$index];
          $obj->address = $address;
          break;
        case "SvcCity":
          $address['city'] = $row[$index];
          $obj->address = $address;
          break;
        case "ServiceCity":
          $address['city'] = $row[$index];
          $obj->address = $address;
          break;
        case "Service Town":
          $address['city'] = $row[$index];
          $obj->address = $address;
          break;
        case "City":
          $address['city'] = $row[$index];
          $obj->address = $address;
          break;
        case "Service State":
          $address['state'] = $row[$index];
          $obj->address = $address;
          break;
        case "SvcState":
          $address['state'] = $row[$index];
          $obj->address = $address;
          break;
        case "ServiceState":
          $address['state'] = $row[$index];
          $obj->address = $address;
          break;
        case "State":
          $address['state'] = $row[$index];
          $obj->address = $address;
          break;
        case "Service Zip Code":
          $address['zip'] = str_pad($row[$index], 5, '0', STR_PAD_LEFT);
          $obj->address = $address;
          break;
        case "Service Postal Code":
          $address['zip'] = str_pad($row[$index], 5, '0', STR_PAD_LEFT);
          $obj->address = $address;
          break;
        case "ServiceZip":
          $address['zip'] = str_pad($row[$index], 5, '0', STR_PAD_LEFT);
          $obj->address = $address;
          break;
        case "SvcZip":
          $address['zip'] = str_pad($row[$index], 5, '0', STR_PAD_LEFT);
          $obj->address = $address;
          break;
        case "Zip":
          $address['zip'] = str_pad($row[$index], 5, '0', STR_PAD_LEFT);
          $obj->address = $address;
          break;
        case "ServiceStartDate":
          if ($row[$index]) {
            $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        
        case "Usage From Date":
          if ($row[$index]) {
            $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "Usage from date":
          if ($row[$index]) {
            $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "InvoiceFromDate":
          if ($row[$index]) {
            $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "Invoice from Date":
          if ($row[$index]) {
            $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "Use From Dt.":

          if ($row[$index]) {
              $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "Use From Date":

          if ($row[$index]) {
              $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "From Date":

          if ($row[$index]) {
              $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
          
          
        case "Reading From Date":

          if ($row[$index]) {
              $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "Start":

          if ($row[$index]) {
              $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        // case "Start Date":
        //   if (!$obj->usage_from_date) {
        //     if ($row[$index]) {
        //         $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
        //     }
        //   }
        //   break;
        case "Bill Start Date":
          if (!$obj->usage_from_date) {
            if ($row[$index]) {
                $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
            }
          }
          break;
        case "UsageStart":

          if ($row[$index]) {
              $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "Usage Start":

          if ($row[$index]) {
              $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        // case "Start Date":

        //   if ($row[$index]) {
        //       $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
        //   }
        //   break;
        case "Use From Dt":

          if ($row[$index]) {
              $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "Use From Dt.":
          if ($row[$index]) {
              $obj->usage_from_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "ServiceEndDate":
          if ($row[$index]) {
            $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "Usage To Date":
          if ($row[$index]) {
            $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "Usage to date":
          if ($row[$index]) {
            $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "Usage to Date":
          if ($row[$index]) {
            $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;

        case "Month of Usage End Date":
          // Worcester too special case
          if ($row[$index]) {
            $year = 2022;
            if ((int) date('n', strtotime($row[$index])) > 6) {
              $year = 2021;
            }
            $to_date = Carbon::parse($row[$index].' 15, '.$year);
            //echo $to_date->format('Y-m-d')." ".$row[$index]." ".$year."\n";
            if ($to_date > Carbon::today()) {
              dd($row[$index], $year, (int) date('n', strtotime($row[$index])));
            }
            
            $obj->usage_to_date = $to_date->format('Y-m-d');
            break;
          }
        
        case "Usage End Date":
          if ($row[$index]) {
            $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "Usage End":
          if ($row[$index]) {
            $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        
        case "Use To Date":
          if ($row[$index]) {
            $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "To Date":
          if ($row[$index]) {
            $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "Reading To Date":
          if ($row[$index]) {
            $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "InvoiceToDate":
          if ($row[$index]) {
            $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "End":
          if ($row[$index]) {
            $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        // case "End Date":
        //   if ($row[$index]) {
        //     if (!$obj->usage_to_date) {
        //       $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
        //     }
        //   }
        //   break;
        case "Bill End Date":
          if ($row[$index]) {
            if (!$obj->usage_to_date) {
              $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
            }
          }
          break;
        case "UsageEnd":
          if ($row[$index]) {
            $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "Invoice to Date":
          if ($row[$index]) {
            $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        case "Last Meter Read Date":
          if ($row[$index]) {
            $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
          }
          break;
        // case "End Date":

        //   if ($row[$index]) {
        //       $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
        //   }
        //   break;
        case 'LDCReadDate':
          if ($row[$index]) {
            if (is_object($row[$index])) {
              $obj->usage_to_date = $row[$index];
            } else {
              $obj->usage_to_date = Carbon::parse(substr($row[$index], 0, 19));
            }
          }
          break;
        case "Usage (kWh)":
          $obj->usage_kwh = $row[$index];
          break;
        case "Usage Billed":
          $obj->usage_kwh = $row[$index];
          break;

        case "Usage(kWh)":
          $obj->usage_kwh = $row[$index];
          break;
        case "Use_kwh":
          $obj->usage_kwh = $row[$index];
          break;
        case "Use (kwh)":
          $obj->usage_kwh = $row[$index];
          break;
        case "Use":
          $obj->usage_kwh = $row[$index];
          break;
        case "Usage Value":
          $obj->usage_kwh = $row[$index];
          break;
        case "kWh":
          $obj->usage_kwh = $row[$index];
          break;
        case "Usage":
          $obj->usage_kwh = $row[$index];
          break;
        case "Kwh":
          $obj->usage_kwh = $row[$index];
          break;
        case "Price Per kWh":
          $obj->price = $row[$index];
          break;
        case "Pay Amount":
          $obj->price = $row[$index];
          break;
        case "TotalUsage": 
          $obj->usage_kwh = $row[$index];
          break;
        case "Total Usage": 
          $obj->usage_kwh = $row[$index];
          break;
        case "Last Usage": 
          $obj->usage_kwh = $row[$index];
          break;
        case "LDC Read Month":

          $ldc_month = $row[$index];
          if (!$ldc_month) {
            break;
          }
          if (date('Y-m-d', strtotime($ldc_month)) > 2000) {
            $ldc_month = date('Y-m-d', strtotime($ldc_month));
          } 
          if (is_object($ldc_month)) {
            $ldc_month = $ldc_month->format('Y-m-d');
          }

          if ($ldc_month < 2000) {
            $ldc_month = Carbon::parse(date('Y', strtotime($this->getReadingDate($row)))."-".$row[$index]."-1");
          }
          //dd($ldc_month, $row);
          $obj->ldc_read_month = $ldc_month;
          break;
        case "Read Cycle":
          $obj->read_cycle = $row[$index];
          break;
        case "ReadCycle":
          $obj->read_cycle = $row[$index];
          break;
        case "MeterRead":
          $obj->read_cycle = $row[$index];
          break;
        case "BillCycle":
          $obj->read_cycle = $row[$index];
          break;
        case "Bill Cycle":
          $obj->read_cycle = $row[$index];
          break;
        case "Billing Cycle":
          $obj->read_cycle = $row[$index];
          break;
        case "CAPACITYOBLIGATION":
          $obj->icap = $row[$index];
          break;
        case "ICAP (kW)":
          $obj->icap = $row[$index];
          break;
        case "ICAP (kWh)":
          $obj->icap = $row[$index];
          break;
        case "ICAP (KW)":
          $obj->icap = $row[$index];
          break;
        case "ICAP (kw)":
          $obj->icap = $row[$index];
          break;
        case "ICAP":
          $obj->icap = $row[$index];
          break;
        case "ICAP Tag":
          $obj->icap = $row[$index];
          break;
        case "ICAP(kW)":
          $obj->icap = $row[$index];
          break;
        case "ICAP (kw)":
          $obj->icap = $row[$index];
          break;
        case 'PeakLoadCapacity':
          $obj->icap = $row[$index];
          break;
        case 'Last Capacity Value':
          $obj->icap = $row[$index];
          break;
        case 'Updated Cap Tag':
          $obj->icap = $row[$index];
          break;
        case 'PLC':
          $obj->icap = $row[$index];
          break;
        case 'Load Zone':
          $obj->load_zone = $row[$index];
          break;
        case 'lzshort':
          $obj->load_zone = $row[$index];
          break;
        // Master Account files specific
        case "Account Status":
          $obj->account_status = $row[$index];
          break;
        case "Column1":
          $obj->account_status = $row[$index];
          break;
        case "Acct Status":
          $obj->account_status = $row[$index];
          break;
        case "Account Status (active/inactive)":
          $obj->account_status = $row[$index];
          break;
          
        case "AccountStatus":
          $obj->account_status = $row[$index];
          break;
        case "Account Start Date":
          if ($row[$index]) {
            $obj->account_start_date = date('Y-m-d', strtotime($row[$index]));
          }
          break;
        case "Acct Start Date":
          if ($row[$index]) {
            $obj->account_start_date = date('Y-m-d', strtotime($row[$index]));
          }
          break;
        case "UtilityStartDate":
          if ($row[$index]) {
            $obj->account_start_date = date('Y-m-d', strtotime($row[$index]));
          }
          break;
        case "UtilityEndDate":
          if ($row[$index]) {
            $obj->account_end_date = date('Y-m-d', strtotime($row[$index]));
          }
          break;
        case "Account End Date (Drop Date)":
          if ($row[$index]) {
            $obj->account_end_date = date('Y-m-d', strtotime($row[$index]));
          }
          break;
        case "Acct End Date":
          if ($row[$index]) {
            $obj->account_end_date = date('Y-m-d', strtotime($row[$index]));
          }
          break;
        case "Account End Date (Drop Date)":
          if ($row[$index]) {
            $obj->account_end_date = date('Y-m-d', strtotime($row[$index]));
          }
          break;


      }

      if ($obj->usage_to_date > Carbon::today()) {
        //dd($obj, $row, "In import");
      }

      if (str_contains($obj->usage_kwh, ',')) {
        $obj->usage_kwh = str_replace(',', '', $obj->usage_kwh);
      }



    }

    public function getReadingDate($row)
    {
      $acceptable = ["Use To Date", 'Usage To Date'];
      foreach ($acceptable as $usage_to) {
        if (isset($row[$usage_to])) {
          if (is_object($row[$usage_to])) {
            return $row[$usage_to]->format('Y-m-d');
          }
          return $row[$usage_to];
        }
      }
      return "getReadingDate FAILED!!!";
    }

    public function nullifyEmpty(&$obj)
    {
      foreach ($obj->toArray() as $key => $val) {
        if (isset($obj->$key)) {

          if ($obj->$key === '') {
            //echo "Unsetting $key\n";
            unset($obj->$key);
          }
        }
      }
    }
}





/*
private function importSheet(SheetInterface $sheet, callable $callback = null)
    {
        $headers = [];
        $collection = [];
        $count_header = 0;

        // analyze first 20 rows to figure out where the fuck the real headers are
        // Laz
        dd($sheet, "In vendor folder");
        $tempsheet = clone($sheet);
        $header_counts = [];
        $end_counts = [];
        $header_row = 1;
        foreach ($sheet->getRowIterator() as $k => $row) {
            $headers = $this->toStrings($row);
            $blank_count = 0;
            $end_count = 0;
            foreach ($headers as $header) {
                if ($header == '') {
                    $blank_count++;
                    $end_count++;
                } else {
                    $end_count = 0;
                }
            }
            $header_counts[] = $blank_count."/".count($row);
            $end_counts[] = $end_count."/".count($row);

            if ($k > 19) {
                break;
            }
        }
        //dd($header_counts, $end_counts);
        foreach ($header_counts as $index => $header_count) {
            if ($header_count > 0) {
                // case where blank columns to right of header row
                if ($header_count == $end_counts[$index]) {
                    $header_row = $index + 1;
                    break;
                }
            }
            if ($header_count < 3) {
                $header_row = $index + 1;
                break;
            }
        }
        //dd($header_row);


        if ($this->with_header) {
            foreach ($sheet->getRowIterator() as $k => $row) {

                if ($k < $header_row) {
                    continue;
                }
                if ($k == $header_row) {
                    $headers = $this->toStrings($row);
                    $count_header = count($headers);
                    continue;
                }
                if ($count_header > $count_row = count($row)) {
                    $row = array_merge($row, array_fill(0, $count_header - $count_row, null));
                } elseif ($count_header < $count_row = count($row)) {
                    $row = array_slice($row, 0, $count_header);
                }
                if ($callback) {
                    if ($result = $callback(array_combine($headers, $row))) {
                        $collection[] = $result;
                    }
                } else {
                    $collection[] = array_combine($headers, $row);
                }
            }
        } else {
            foreach ($sheet->getRowIterator() as $row) {
                if ($callback) {
                    if ($result = $callback($row)) {
                        $collection[] = $result;
                    }
                } else {
                    $collection[] = $row;
                }
            }
        }

        return $collection;
    }
    */






