<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UsageReading;
use DB;
use App\Traits\CrunchTrait;

class LdcMonth extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:ldc_month {--city=} {--reset} {--table=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $city = $this->startCrunch('ldc_month', $this->option('city'));

        //$stillnull_ids = UsageReading::whereNull('corrected_ldc_read_month')->pluck('id');
        
        /*
        $null = UsageReading::whereNotNull('corrected_ldc_read_month')
                               ->update(
                            ['corrected_ldc_read_month' => null]
                        );
        */

        $table = 'usage_readings';
        if ($this->option('table')) {
            $table = $this->option('table');
            session(['usage_table_override' => $table]);
        }
        

        if ($this->option('reset')) {
            
            DB::statement("UPDATE $table SET corrected_ldc_read_month = NULL where usage_to_date IS NOT NULL");
            echo "Cleared out corrected_ldc_read_month \n";
        }
        
        while (($count = UsageReading::whereNull('corrected_ldc_read_month')
                           ->whereNotNull('usage_to_date')
                           ->count()) > 0) {
            echo $count." Left to get usage to date ldc month\n";
            $all = UsageReading::whereNull('corrected_ldc_read_month')
                               ->whereNotNull('usage_to_date')
                               ->take(10000)
                               ->update(
                            ['corrected_ldc_read_month' => DB::raw('CONCAT(YEAR(usage_to_date), "-", MONTH(usage_to_date), "-01")')]
                        );
        }

        DB::statement("SET innodb_lock_wait_timeout = 120;");
        
        $lowcount = UsageReading::where('corrected_read_cycle', '<', '10')
                    ->where(DB::raw('DAYOFMONTH(usage_to_date)'), '>', 20)->count();
        echo "Low count: $lowcount\n";
        UsageReading::where('corrected_read_cycle', '<', '10')
                    ->where(DB::raw('DAYOFMONTH(usage_to_date)'), '>', 20)
                    ->update(
                            ['corrected_ldc_read_month' => DB::raw('CONCAT(YEAR(DATE_ADD(usage_to_date, INTERVAL 1 MONTH)), "-",MONTH(DATE_ADD(usage_to_date, INTERVAL 1 MONTH)), "-01")')]
                        );

        $highcount = UsageReading::where('corrected_read_cycle', '>', '15')
                     ->where(DB::raw('DAYOFMONTH(usage_to_date)'), '<', 6)
                     ->count();
        echo "High count: $highcount\n";
        UsageReading::where('corrected_read_cycle', '>', '15')
                     ->where(DB::raw('DAYOFMONTH(usage_to_date)'), '<', 6)
                     ->update(
                            ['corrected_ldc_read_month' => DB::raw('CONCAT(YEAR(DATE_SUB(usage_to_date, INTERVAL 1 MONTH)), "-",MONTH(DATE_SUB(usage_to_date, INTERVAL 1 MONTH)), "-01")')]
                        );

        $this->endCrunch();
    }
}
