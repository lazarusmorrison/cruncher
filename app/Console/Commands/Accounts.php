<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CommissionUsageReading;
use App\UsageReading;
use App\SalesUsageReading;
use App\Account;
use App\MasterMonthlyAccount;
use DB;
use App\Traits\CrunchTrait;

class Accounts extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:accounts {--city=} {--anon}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $city = $this->startCrunch('accounts', $this->option('city'));

        if ($this->option('anon')) {
            $all = Account::all();
            foreach ($all as $acc) {
                // regenerates anonymized_id if needed
                $acc->save();
            }
            return;
        }

        echo "PULLING ACCOUNT IDS FROM USAGE\n";

        $usage_accounts = UsageReading::groupBy('account_id')
                                         ->pluck('account_id');

        echo $usage_accounts->count()." SALES\n";
        $sales_accounts = SalesUsageReading::groupBy('account_id')
                                         ->pluck('account_id');

        echo $sales_accounts->count()." COMMISSION\n";
        $commission_accounts = CommissionUsageReading::groupBy('account_id')
                                         ->pluck('account_id');

        echo $commission_accounts->count()." MASTER\n";
        $active_accounts = MasterMonthlyAccount::groupBy('account_id')
                                         ->pluck('account_id');

        echo $active_accounts->count()." ACCOUNTS\n";
        $keyarr = [];

        echo "Merge 1. ";
        foreach ($usage_accounts as $aid) {
            $keyarr[$aid] = $aid;
        }
        echo "Merge 2. ";
        foreach ($sales_accounts as $aid) {
            $keyarr[$aid] = $aid;
        }
        echo "Merge 3. ";
        foreach ($commission_accounts as $aid) {
            $keyarr[$aid] = $aid;
        }
        echo "Merge 4. ";
        foreach ($active_accounts as $aid) {
            $keyarr[$aid] = $aid;
        }
        ksort($keyarr);

        $accounts = collect($keyarr);
        echo "Total: ".$accounts->count()."\n";
        $rowcount = 0;
        foreach ($accounts->chunk(371) as $chunk) {
            echo $rowcount."\r";
            foreach ($chunk as $accountid) {
                $rowcount++;
                $account = Account::find($accountid);

                if (!$account) {
                    $account = new Account;
                    $account->city = $city;
                    $account->id = $accountid;
                    
                }

                if (!$account->base_rate_class) {
                    $last_usage = UsageReading::where('account_id', $accountid)
                                              ->orderBy('usage_to_date', 'desc')
                                              ->first();
                    if ($last_usage) {
                        $account->read_cycle = $last_usage->corrected_read_cycle;
                        $account->icap = $last_usage->corrected_icap;
                        $account->rate_class = $last_usage->corrected_rate_class;
                        $account->base_rate_class = $last_usage->corrected_rate_class;
                    }
                }
                $account->save();
            }
        }

        
        $this->endCrunch();
    }
}
