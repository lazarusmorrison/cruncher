<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\DataImport;
use App\SalesUsageReading;
use App\CommissionUsageReading;
use App\UsageReading;

class SwitchCity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:switch {--city=} {--import=} {--newcity=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Moves a file and ALL its usage_readings to another database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        ini_set('memory_limit','1G');

        if (!$this->option('city')) {
            dd("Needs --city=");
        }
        if (!$this->option('import')) {
            dd("Needs --import=");
        }
        if (!$this->option('newcity')) {
            dd("Needs --newcity=");
        }
        useCityDatabase($this->option('city'));

        $file = DataImport::find($this->option('import'));

        if (!$file) {
            dd("File id ".$this->option('import')." not found");
        }

        if (!$this->confirm('Is this file right? '.$file->name)) {
            dd("Wrong file id then.");
        }
        
        $sales = $file->salesUsageReadings()->get();
        $comms = $file->commissionUsageReadings()->get();
        $usage = $file->appendixUsageReadings()->get();

        useCityDatabase($this->option('newcity'));


        
        $copied_count = 0;
        $exists_count = 0;
        foreach ($sales as $ur) {
            $exists = SalesUsageReading::where('account_id', $ur->account_id)
                                       ->where('usage_to_date', $ur->usage_to_date)
                                       ->where('usage_kwh', $ur->usage_kwh)
                                       ->exists();
            if ($exists) {
                $exists_count++;
            } else {
                $copied_count++;
            }
        }
        $this->info(strtoupper($this->option('newcity')).' Sales Copied: '.$copied_count);
        $this->info(strtoupper($this->option('newcity')).' Sales Exists: '.$exists_count);

        $copied_count = 0;
        $exists_count = 0;
        foreach ($comms as $ur) {
            //dd($ur);
            $exists = CommissionUsageReading::where('account_id', $ur->account_id)
                                       ->where('usage_to_date', $ur->usage_to_date)
                                       ->where('usage_kwh', $ur->usage_kwh)
                                       ->exists();
            if ($exists) {
                $exists_count++;
            } else {
                $copied_count++;
            }
        }

        $this->info(strtoupper($this->option('newcity')).' Comms Copied: '.$copied_count);
        $this->info(strtoupper($this->option('newcity')).' Comms Exists: '.$exists_count);
        //dd($exists_count, $copied_count);

        $copied_count = 0;
        $exists_count = 0;
        foreach ($usage as $ur) {

            $exists = UsageReading::where('account_id', $ur->account_id)
                                       ->where('usage_to_date', $ur->usage_to_date)
                                       ->where('usage_kwh', $ur->usage_kwh)
                                       ->exists();
            if ($exists) {
                $exists_count++;
            } else {
                $copied_count++;
            }
        }
        
        $this->info(strtoupper($this->option('newcity')).' Usage Copied: '.$copied_count);
        $this->info(strtoupper($this->option('newcity')).' Usage Exists: '.$exists_count);

        if (!$this->confirm('Does this look right?')) {
            dd("Well nevermind then.");
        }

        $newname = str_replace(strtoupper($this->option('city')), 
                               strtoupper($this->option('newcity')),
                               $file->original_name);

        $newfile = DataImport::where('original_name', $newname)->first();
        if (!$newfile) {
            $newfile = $file->replicate();
            $newfile->original_name = $newname;
            $newfile->save();
        }
        
        $copied = 0;
        foreach ($sales as $ur) {
            $exists = SalesUsageReading::where('account_id', $ur->account_id)
                                       ->where('usage_to_date', $ur->usage_to_date)
                                       ->where('usage_kwh', $ur->usage_kwh)
                                       ->exists();
            if (!$exists) {
                $copied++;
                $newur = $ur->replicate();
                $newur->usage_file_id = $newfile->id;
                $newur->save();
            }
        }

        foreach ($comms as $ur) {
            $exists = CommissionUsageReading::where('account_id', $ur->account_id)
                                       ->where('usage_to_date', $ur->usage_to_date)
                                       ->where('usage_kwh', $ur->usage_kwh)
                                       ->exists();
            if (!$exists) {
                $copied++;
                $newur = $ur->replicate();
                $newur->usage_file_id = $newfile->id;
                $newur->save();
            }
        }
        foreach ($usage as $ur) {
            
            $exists = UsageReading::where('account_id', $ur->account_id)
                                       ->where('usage_to_date', $ur->usage_to_date)
                                       ->where('usage_kwh', $ur->usage_kwh)
                                       ->exists();
            if (!$exists) {
                $copied++;
                $newur = $ur->replicate();
                $newur->usage_file_id = $newfile->id;
                $newur->save();
            }
        }
        
        $this->info('Copied '.$copied." readings.");
     
        if ($this->confirm('Want to remove old file now?')) {
            $this->call('crunch:remove_import', ['--city' => $this->option('city'),
                                                 '--import' => $this->option('import')]);
        }   
    }
}
