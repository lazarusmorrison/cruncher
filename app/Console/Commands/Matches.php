<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ReadingMatch;
use App\UsageReading;
use App\CommissionUsageReading;
use App\SalesUsageReading;
use App\Traits\CrunchTrait;
use Carbon\Carbon;
use App\City;

class Matches extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:matches {--city=} {--clear} {--month=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $cities = City::all();
      if ($this->option('city')) {
        $cities = City::where('name', $this->option('city'))->get();
      }

      foreach ($cities as $city) {

        $city_name = $this->startCrunch('matches', $city->name);

        if ($this->option('clear')) {        
            SalesUsageReading::whereNotNull('match_id')->update(['match_id' => null]);
            CommissionUsageReading::whereNotNull('match_id')->update(['match_id' => null]);
        }

        $earliest_commission = CommissionUsageReading::whereNotNull('corrected_ldc_read_month')
                                                    ->orderBy('corrected_ldc_read_month')
                                                     ->first();
        $earliest_month = null;
        if ($earliest_commission) {
          $earliest_month = $earliest_commission->corrected_ldc_read_month;
        }
        //dd($earliest_commission);
        
        if ($earliest_month) {
          if ($month = $this->option('month')) {
              $sales_query = SalesUsageReading::where('corrected_ldc_read_month', '=', $month)
                                            ->whereNull('match_id')
                                            ->ignoreCorrections()
                                            ->with('accountCommissionUsage')
                                            ->orderBy('id', 'desc');
          } else {
              $sales_query = SalesUsageReading::where('corrected_ldc_read_month', '>=', $earliest_month)                          ->whereNotNull('corrected_ldc_read_month')
                                            ->whereNull('match_id')
                                            ->ignoreCorrections()
                                            ->with('accountCommissionUsage')
                                            ->orderBy('id', 'desc');
          }

          $total = $sales_query->count();
          $count = 0;
          $sales_query->chunkById(1000, function($sales) use (&$count, $total) {
            echo "$count/$total\n";
            foreach ($sales as $sale) {
              $count++;
              

              foreach ($sale->accountCommissionUsage as $cu) {

                if ($cu->correction) {
                  continue;
                }
                if ($cu->corrected_ldc_read_month == null) {
                  continue;
                }

               
                if ($cu->usage_to_date == $sale->usage_to_date && $cu->usage_kwh == $sale->usage_kwh) {

                  $cu->match_id = $sale->id;
                  $sale->match_id = $cu->id;
                  $cu->save();
                  $sale->save();

                }

              }
            }
          });
        }


        $this->endCrunch();
      }
    }
}
