<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\City;
use App\Traits\HandyTrait;
use App\DataImport;
use DB;

class ReplaceCorrections extends Command
{
    use HandyTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:replace {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes a file and replaces it with a correction file.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $client_folder = storage_path(config('app.client_folder'));
        $client_folder = str_replace('/Users/fluency/Code/cruncher/storage/app/', 
                                     '/Users/fluency/Sync/BSC/', 
                                     $client_folder);
        $process_folder = storage_path(config('app.processing_folder'));

        if (!$process_folder || !$client_folder) {
            dd($client_folder, $process_folder);
        }

        // choose city
        $cities = City::orderBy('name')->get()->keyBy('name');
        if ($this->option('city')) {
            $cities = City::where('name', $this->option('city'))->get()->keyBy('name');
        }
        // loop cities
        foreach ($cities as $city) {

            echo "\n===========> ".strtoupper($city->name)." <===========\n";
            $db = "bsc_".$city->name;
            DB::purge('mysql');
            config(['database.connections.mysql.database' => $db]);

            $folder_city = str_replace('ugh', '', ucwords($city->name));
            $city_folder = $client_folder.'/'.$folder_city;
            $all_files = glob($city_folder.'/*');

            //dd($city_folder, $all_files);

            $imports = DataImport::take(30)->latest()->orderBy('name')->get();

            foreach ($all_files as $filepath) {

                if (strpos($filepath, '_TO_DELETE_') > 0) {
                    continue;
                }

                $filename = strtoupper(basename($filepath));
                //$this->info($filename);

                // check for files named correction
                if (stripos($filename, 'correction') > 0
                     || stripos($filename, 'corrected') > 0) {

                    $this->info("CORRECTION FILE:");
                    $this->info($filename);

                    // see if old file can be matched
                    //$date = 

                    // show all files
                    
                    $file_array = [];

                    foreach ($imports as $index => $import) {
                        $file_array[$index] = $import->id.' | '.$import->name;
                    }

                    $replace_name = $this->choice(
                        'Which file should be replaced?',
                        array_merge(['HIT RETURN - Skip'], $file_array),
                        0
                    );

                    if (strpos($replace_name, 'Skip') > 0) {
                        continue;
                    }

                    $replace_id = explode(' | ', $replace_name, 2)[0];
                    // verify match with user
                    $this->error("REPLACE: ".$replace_name);
                    $this->info("CORRECT: ".$filename);
                    if ($this->confirm("Are you sure this is right?")) {
                        
                        $replacement = DataImport::find($replace_id);
                        
                        if ($replacement) {
                            $this->call("crunch:remove_import", ['--city' => $city->name,
                                                                 '--import' => $replace_id]);
                        }
                    }


                    // Load new file (or not? regular process will load?)

                }
                

            }
        }
        
    }
}
