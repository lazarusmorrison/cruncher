<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\DataImport;
use App\City;
use App\UsageReading;
use App\SalesUsageReading;
use App\CommissionUsageReading;
use App\MasterMonthlyAccount;
use App\Traits\CrunchTrait;
use App\ReadingMatch;

class FileCounts extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:counts {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds an array of counts to the DataImport objects';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cities = City::orderBy('name')->get();

        if ($this->option('city')) {
            $cities = City::where('name', $this->option('city'))->get();
        }
        echo "\n****************************** COUNTS ******************************\n\n";
        
        foreach ($cities as $city) {

            $city_name = $this->startCrunch('counts', $city->name);

            $imports = DataImport::orderBy('month')->get();

            
            foreach ($imports as $import) {
                $counts = [];
                $counts['Type'] = ucwords($import->type);
                if ($import->type == 'historical') {
                    $model = UsageReading::class;
                }
                if ($import->type == 'sales') {
                    $model = SalesUsageReading::class;
                }
                if ($import->type == 'commission') {
                    $model = CommissionUsageReading::class;
                }
                if ($import->type == 'master') {
                    $model = MasterMonthlyAccount::class;
                }
                $counts['Imported'] = $model::where('usage_file_id', $import->id)->count();

                $counts['Appendix'] = UsageReading::where('usage_file_id', $import->id)->count();

                if ($import->type == 'sales' || $import->type == 'commission') {
                    $counts['Matches'] = $model::where('usage_file_id', $import->id)
                                               ->whereNotNull('match_id')
                                               ->count();
                }

                if ($import->type != 'master') {
                    $counts['Corrected ICAP'] = $model::where('usage_file_id', $import->id)
                                                      ->whereRaw('corrected_icap != icap')
                                                      ->count();
                    $counts['Corrected Rate'] = $model::whereRaw('corrected_rate_class != base_rate_class')
                                                      ->where('usage_file_id', $import->id)
                                                      ->count();
                    $counts['Corrected LDC'] = $model::where('usage_file_id', $import->id)
                                                     ->whereRaw('corrected_ldc_read_month != ldc_read_month')
                                                     ->count();
                    $counts['Corrected Cycle'] = $model::whereRaw('corrected_read_cycle != read_cycle')
                                                       ->where('usage_file_id', $import->id)
                                                       ->count();
                }

                // Unique Account #s in this city

                // Which other cities have these account #s?

                // Master reference for all cities accounts

                $other_cities = [];
                $cities_to_check = City::orderBy('name')->pluck('name');
                foreach ($cities_to_check as $city) {
                    $city_count = $model::where('usage_file_id', $import->id)
                                        ->where('all_fields', 'LIKE', '%'.$city.'%')
                                        ->count();
                    if ($city_count > 0) {
                        $other_cities[$city] = $city_count;
                    }
                }


                $import->counts = $counts;
                //dd($counts);
                $import->save();
            }

        }
    }
}
