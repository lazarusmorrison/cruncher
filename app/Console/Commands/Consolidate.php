<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;

use DB;
use Schema;
use Carbon\Carbon;
use App\City;


class Consolidate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:consolidate {--city=} {--start_city=} {--after=} {--today} {--week} {--month}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Syncs all readings into one master database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public $after_updated_at;


    public function handle()
    {
        // Count city commission_usage_readings
        // Count city sales_usage_readings
        // Count city usage_readings
        // Switch connections, count consolidated for each
        // Compare each
        // Sync using field list below


        if ($this->option('month')) {

            $this->after_updated_at = Carbon::now()->subMonths(1)->subMinutes(1)->toDateTimeString();

        } elseif ($this->option('week')) {

            $this->after_updated_at = Carbon::now()->subWeeks(1)->subMinutes(1)->toDateTimeString();

        } elseif ($this->option('today')) {

            $this->after_updated_at = Carbon::today()->subDays(1)->endOfDay()->toDateTimeString();

        } elseif ($this->option('after')) {

            $this->after_updated_at = Carbon::parse($this->option('after'))->toDateTimeString();

        }

        if (!Schema::connection('baystate-shared')->hasTable('cities')) {

            // Test cities

            $cities = json_decode(json_encode([
                ['id' => 3, 'name' => 'wellfleet'],
                ['id' => 1, 'name' => 'bellingham'],
                ['id' => 2, 'name' => 'acton'],
            ]));

        } elseif ($this->option('city')) {

            $cities = City::where('name', strtolower($this->option('city')))->get();

        } elseif ($this->option('start_city')) {

            $cities = City::where('name', '>=', strtolower($this->option('start_city')))
                          ->orderBy('name')
                          ->get();

        } else {

            $cities = City::orderBy('name')->get();

        }

        if ($this->after_updated_at) {
            echo "\n";
            $this->info('*** Only records with updated_at > '.$this->after_updated_at.' ***');
        }

        $grand_start = Carbon::now();

        foreach ($cities as $city) {
            echo "\n===========> ".strtoupper($city->name)." <===========\n";

            $db = "bsc_".$city->name;
    
            if (!$this->databaseExists($db)) {

                $this->error('No such DB: '.$db.' / skipping.');
                continue;

            }

            $this->switchDBConnection($db);

            if (!Schema::hasTable('commission_usage_readings')
                || !Schema::hasTable('sales_usage_readings')
                || !Schema::hasTable('usage_readings')) {

                $this->error($db.' is missing tables / skipping.');
                continue;
            }

            $city_start = Carbon::now();

            foreach([
                    ['commission_usage_readings' => 'commission_readings'],
                    ['sales_usage_readings'      => 'usage_readings'],
                    ['usage_readings'            => 'combined_readings']
                    ] as $table_pair) {


                $start = Carbon::now();

                $this->syncCityTable($city, $db, $table_pair);

                $this->error('Took '.$this->calculateDuration($start).' minutes');
            }

            $this->error(strtoupper($city->name).' Total: '.$this->calculateDuration($city_start).' minutes');

        }

        echo str_repeat('-', 60)."\n";

        $this->error('GRAND TOTAL: '.$this->calculateDuration($grand_start).' MINUTES');

    }

    public function databaseExists($db)
    {
        return DB::table('INFORMATION_SCHEMA.SCHEMATA')
                  ->selectRaw("SCHEMA_NAME")
                  ->where("SCHEMA_NAME", $db)->first();
    }

    public function switchDBConnection($db)
    {
        DB::purge('mysql');
        config(['database.connections.mysql.database' => $db]);
    }

    public function calculateDuration($start)
    {
        return round(Carbon::now()->diffInSeconds($start)/60,2);
    }

    public function syncCityTable($city, $city_db, $table_pair)
    {
        $sync_db = config('database.connections.consolidated.database');

        $city_table = array_key_first($table_pair);
        $sync_table = $table_pair[$city_table];

        // check for missing columns
        
        if (!Schema::hasColumn($city_table, 'price')) {
            echo "Adding price to ".$city_table."\n";
            Schema::table($city_table, function (Blueprint $table) {
                $table->float('price', 15, 6)->after('address')->nullable();
            });
        }
        if (!Schema::hasColumn($city_table, 'load_zone')) {
            echo "Adding load_zone to ".$city_table."\n";
            Schema::table($city_table, function (Blueprint $table) {
                $table->string('load_zone')->after('price')->nullable();
            });
        }

        $timestamp   = Carbon::now();
        $new_sync_id = DB::connection('consolidated')->table($sync_table)->max('sync_id') + 1;

        $city_count = DB::table($city_db.'.'.$city_table)->count();
        $this->info('City Table: '.$city_table);
        $this->info('     Count: '.number_format($city_count));

        $this->info('Sync Table: '.$sync_table.' ----> Sync ID '.$new_sync_id);
        $this->info('     Count: '.number_format(
                                    DB::table($sync_db.'.'.$sync_table)
                                      ->where('city', $city->name)
                                      ->count()
                                    )
                    );

        //---------------------------------------- INSERT

        $sql = "INSERT IGNORE $sync_db.$sync_table "
                ."(".$this->getFieldsAsCommaDelimited().", sync_id, synced_at, row_id)"
                ." SELECT "
                .$this->getFieldsAsCommaDelimited()
                .", '$new_sync_id', '$timestamp', CONCAT('".$city->id."_', id)"
                ." FROM "
                ."$city_db.$city_table";

        if ($this->after_updated_at) {
            $sql .= " WHERE $city_db.$city_table.updated_at > '$this->after_updated_at'";
        }

        try {

            $affected = DB::connection('consolidated')->update($sql);
            $affected = number_format($affected);
            echo "  inserted: $affected\n";

        } catch (\Exception $e) {

            $this->error($e->getMessage());

        }


        //---------------------------------------- UPDATE 

        $sql = "UPDATE $sync_db.$sync_table AS sync, $city_db.$city_table AS city SET ";

        $setArray = [];
        foreach($this->getFields() as $field) {
            $setArray[] = "sync.$field =  city.$field";
        }

        $setArray[] = "sync.sync_id = '$new_sync_id'";
        $setArray[] = "sync.synced_at = '$timestamp'";

        $sql .= implode(',', $setArray);
        $sql .= " WHERE sync.row_id = CONCAT('".$city->id."_', city.id)
                 AND sync.sync_id != $new_sync_id
                 AND sync.city = '$city->name'";

        if ($this->after_updated_at) {
            $sql .= " AND city.updated_at > '$this->after_updated_at'";

            // So older ones not deleted:

            $sqlB = "UPDATE $sync_db.$sync_table
                     SET sync_id = $new_sync_id,
                         synced_at = '$timestamp'
                     WHERE updated_at <= '$this->after_updated_at'";
        }

        try {

            // Make this a Transaction?

            if(isset($sqlB)) {
                DB::connection('consolidated')->update($sqlB);
            }

            $affected = DB::connection('consolidated')->update($sql);
            $affected = number_format($affected);
            echo "   updated: $affected\n";

        } catch (\Exception $e) {

            $this->error($e->getMessage());

        }

        //---------------------------------------- REMOVE

        $sql = "DELETE FROM $sync_db.$sync_table 
                WHERE sync_id != $new_sync_id
                AND city = '$city->name'";

        if ($this->after_updated_at) {
            $sql .= " AND updated_at > '$this->after_updated_at'";
        }
        
        try {

            $affected = DB::connection('consolidated')->delete($sql);
            $affected = number_format($affected);
            echo "   deleted: $affected\n";

        } catch (\Exception $e) {

            $this->error($e->getMessage());
            
        }

        //---------------------------------------- COUNT CHECK

        $new_sync_count = DB::table($sync_db.'.'.$sync_table)
                            ->where('city', $city->name)
                            ->count();

        echo " new count: ".number_format($new_sync_count);
        
        if ($new_sync_count == $city_count) {
            echo " (MATCH)\n";
        } else {
            
            echo "\t <======== **** NO MATCH\n";
        }

    }

    public function getFieldsAsCommaDelimited() {
        return implode(',', $this->getFields());
    }

    public function getFields()
    {
        return [
            'all_fields',
            'updated_at',
            'created_at',

            'city',
            'usage_file_id',
            'file_row',
            'account_id',
            'meter_number',
            'customer_name',
            'address',
            'rate_class',
            'base_rate_class',
            'price',
            'load_zone',

            'usage_from_date',
            'usage_to_date',
            'usage_kwh',

            'ldc_read_month',
            'read_cycle',
            'icap',

            'corrected_ldc_read_month',
            'corrected_read_cycle',
            'corrected_icap',

            'correction',

        ];
    }
}
