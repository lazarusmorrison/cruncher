<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UsageReading;
use DB;
use App\Crunch;
use Carbon\Carbon;
use App\Traits\CrunchTrait;

class UseFromDate extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:use_from_date {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fills the Use from date when its stupid NULL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $city = $this->startCrunch('use_from_date', $this->option('city'));

        $total_missing = UsageReading::whereNull('usage_from_date')
                                     ->whereNotNull('usage_to_date')
                                     ->count();
        echo "Total Missing: ".$total_missing."\n";


        // ===========================================> Missed in Import

        $missed_count = UsageReading::where('all_fields', 'LIKE', '%Use From Date%')
                                    ->whereNull('usage_from_date')
                                    ->count();
        echo "Had Date Field: ".$missed_count."\n";

        $fixed_count = 0;
        UsageReading::where('all_fields', 'LIKE', '%Use From Date%')
                    ->whereNull('usage_from_date')
                    ->chunkById(10000, function($missed_use_from) use (&$fixed_count) {

            

            foreach ($missed_use_from as $index => $ur) {
                $actual = $ur->all_fields['Use From Date'];
                //dd(Carbon::parse($actual), Carbon::parse('2015-01-01'));
                try {
                    if (Carbon::parse($actual) > Carbon::parse('2015-01-01')) {
                        $ur->usage_from_date = Carbon::parse($actual);
                        $ur->save();
                        $fixed_count++;
                    }
                } catch (\Exception $e) {
                    echo "BAD DATE TIME ".$ur->account_id." ".$actual."\n";
                    //dd($actual, $e->getMessage());
                }
            }
            
            //dd($ufs);
        });
        if ($fixed_count > 0) {
            $this->crunch->addToLog("Total Missing: ".$total_missing);
            $this->crunch->addToLog("Fixed: ".$fixed_count);
        }


        

        // ===========================================> Fill from previous reading

        $account_ids = UsageReading::select('account_id')
                                ->whereNull('usage_from_date')
                                ->whereNotNull('usage_to_date')
                                ->groupBy('account_id')
                                ->pluck('account_id');

        $totalcount = UsageReading::whereNull('usage_from_date')
                                ->whereNotNull('usage_to_date')
                                ->count();
        $count = 0;
        echo "About to fix $totalcount in ".$account_ids->count()."\n";

        foreach ($account_ids as $index => $account_id) {
            if ($index % 100 == 0) {
                echo "Account ".$index."\r";
            }
            $urs = UsageReading::where('account_id', $account_id)
                               ->ignoreCorrections()
                               ->orderBy('corrected_ldc_read_month')
                               ->get();
            
            $lastreading = null;
            foreach ($urs as $ur) {
                if (!$ur->usage_from_date) {
                    if ($lastreading) {
                        $count++;
                        $ur->usage_from_date = $lastreading->usage_to_date;
                        $ur->save();
                    }
                }
                $lastreading = $ur;
            }
        }   
        if ($count > 0) {
            $this->crunch->addToLog("Fixed $count of $totalcount with previous dates");
        }

        // ===========================================> Guess at the rest

        $readings = UsageReading::whereNull('usage_from_date')
                                ->whereNotNull('usage_to_date')
                                ->orderBy('usage_to_date')
                                ->get();

        $totalcount = $readings->count();
        $count = 0;

        foreach ($readings as $reading) {
            $reading->usage_from_date = $reading->usage_to_date->subMonth();
            $reading->save();
            $count++;
        }

        if ($count > 0) {
            $this->crunch->addToLog("Fixed $count of $totalcount with subMonth");
        }


        $this->endCrunch();


    }
}
