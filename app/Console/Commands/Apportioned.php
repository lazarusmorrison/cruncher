<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\MonthlyReading;
use App\UsageReading;
use App\Traits\CrunchTrait;
use Carbon\Carbon;

class Apportioned extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:apportioned {--city=} {--redo_recent} {--clear} {--start=} {--account=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $city = $this->startCrunch('apportioned', $this->option('city'));

        if ($this->option('clear')) {        
            MonthlyReading::truncate();
            UsageReading::whereNotNull('monthly_processed_at')->update(['monthly_processed_at' => null]);
        }
        // if ($this->option('redo_recent')) {        
        //     MonthlyReading::where('')
        //     UsageReading::whereNotNull('monthly_processed_at')->update(['monthly_processed_at' => null]);
        // }
        if ($this->option('account')) {        
            MonthlyReading::where('account_id', $this->option('account'))->delete();
            UsageReading::where('account_id', $this->option('account'))
                        ->whereNotNull('monthly_processed_at')
                        ->update(['monthly_processed_at' => null]);
        }
        $startmonth = Carbon::now()->subMonths(18);
        if ($this->option('start')) {
            $startmonth = $this->option('start');
        }

        $basequery = UsageReading::ignoreCorrections()
                                 ->whereNull('monthly_processed_at')
                                 ->latest();

        $totalcount = $basequery->count();
        $currcount = 0;
        $chunksize = 1000;
        $max = UsageReading::orderBy('id', 'desc')->first()->id;
        //dd($max);
        for ($i=0; $i<=$max; $i+=$chunksize) {
            $readings = UsageReading::ignoreCorrections()
                                    ->whereNull('monthly_processed_at')
                                    ->where('corrected_ldc_read_month', '>=', $startmonth)
                                    ->where('id', '>=', $i)
                                    ->orderBy('id')
                                    ->take($chunksize)
                                    ->get();

            echo " $currcount / $totalcount\r";

            foreach ($readings as $reading) {
                //

                $startdate = $reading->usage_from_date;
                $enddate = $reading->usage_to_date;

                if (!$enddate) {
                    "ERROR: no usage to date ids = ".$reading->account_id.", ".$reading->account_id."\n";
                    continue;
                }
                $totaldays = $enddate->diffInDays($startdate);
                //dd($startdate, $enddate, $totaldays);

                

                $months = [];
                for ($currdate = $startdate->copy(); $currdate < $enddate; $currdate->addDay()) {
                    //echo $currdate->format('Y-m-d')."\n";
                    $month = $currdate->format('Y-m');
                    if (isset($months[$month])) {
                        $months[$month] += 1;
                    } else {
                        $months[$month] = 1;
                    }
                }

                arsort($months);

                $primary_month = key($months);
                //dd($primary_month);
                $reading->primary_month = $primary_month.'-01';
                $reading->total_days = $totaldays;
                $reading->monthly_processed_at = Carbon::now();
                $reading->save();

                foreach ($months as $month => $days) {
                    $monthly_reading = MonthlyReading::where('usage_reading_id', $reading->id)
                                                     ->whereDate('month', $month.'-01')
                                                     ->first();

                    if (!$monthly_reading) {
                        $monthly_reading = new MonthlyReading;
                        $monthly_reading->usage_reading_id = $reading->id;
                        $monthly_reading->month = $month.'-01';
                        $monthly_reading->account_id = $reading->account_id;
                    }
                    $monthly_reading->base_rate_class = $reading->corrected_rate_class;
                    $monthly_reading->load_zone = $reading->load_zone;
                    $monthly_reading->city = $reading->city;
                    $monthly_reading->read_cycle = $reading->read_cycle;
                    $monthly_reading->days = $days;

                    $percentage = $days/$totaldays;
                    $monthly_reading->percentage = $percentage;
                    $monthly_reading->usage_kwh = $percentage * $reading->usage_kwh;
                    $icap = $reading->corrected_icap;
                    if (!$reading->icap) {
                        $icap = 0;
                    }
                    $monthly_reading->icap = $icap;

                    $price = $reading->price;
                    if (!$reading->price) {
                        $price = 0;
                    }
                    $monthly_reading->price = $price;

                    $monthly_reading->save();
                }
                
            }
            $currcount += $chunksize;
        }
        $this->endCrunch();
    }
}
