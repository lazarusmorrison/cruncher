<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UsageReading;
use App\MasterMonthlyAccount;
use DB;
use App\Crunch;
use Carbon\Carbon;
use App\Traits\CrunchTrait;

class ICAP extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:icap {--city=} {--wipe} {--split}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fills the corrected_icap when its stupid NULL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $city = $this->startCrunch('icap', $this->option('city'));

        if ($this->option('wipe')) {
            UsageReading::whereNotNull('corrected_icap')
                        ->update(['corrected_icap' => null ]);
            dd("All Corrected ICAP cleared");
        }

        $total_missing = UsageReading::whereNull('corrected_icap')
                                     ->count();
        echo "Total Missing: ".$total_missing."\n";
        //dd();

        // =======================================> SET TO ORIGINAL ICAP

        while (($count = UsageReading::whereNull('corrected_icap')
                    ->whereNotNull('icap')
                    ->count()) > 0) {
            $set_to_original = UsageReading::whereNull('corrected_icap')
                                           ->whereNotNull('icap')
                                           ->take(10000)
                                           ->update(['corrected_icap' => DB::raw('icap')]);
        }
        
        //dd($set_to_original);

        

        

        // =======================================> ICAP FROM ACCOUNT YEAR

        //$this->call('crunch:yearly_icap --city='.$city);

        for ($year = date('Y'); $year > 2017; $year--) {


            // $mismatch_sql = "select a.id, ur.corrected_ldc_read_month, ur.icap, ur.corrected_icap, a.icap as a_icap, a.icap_2017,a.icap_2018,a.icap_2019 
            //                         from usage_readings ur 
            //                         left join accounts a on a.id = ur.account_id 
            //                         where a.icap_".$year." <> ur.corrected_icap
            //                         and ur.corrected_ldc_read_month >= '".($year)."-06-01'
            //                         and ur.corrected_ldc_read_month <= '".($year+1)."-05-01'";
            
            // $mismatches = DB::select($mismatch_sql);
            // dd($mismatches);
            $urcount = UsageReading::whereNull('corrected_icap')
                               ->where('corrected_ldc_read_month', '>=', ($year).'-06-01')
                               ->where('corrected_ldc_read_month', '<=', ($year+1).'-05-01')
                               ->count();
            echo "$year has ".$urcount." duds.\n";
            $urs = UsageReading::whereNull('corrected_icap')
                               ->where('corrected_ldc_read_month', '>=', ($year).'-06-01')
                               ->where('corrected_ldc_read_month', '<=', ($year+1).'-05-01')
                               ->with('account')
                               ->chunkById(1000, function($urs) use ($year) {
                
                foreach ($urs as $ur) {

                    $masters = MasterMonthlyAccount::whereNotNull('icap')
                                                   ->where('month', '>=', ($year).'-06-01')
                                                   ->where('month', '<=', ($year+1).'-05-01')
                                                   ->where('account_id', $ur->account_id)
                                                   ->pluck('icap');
                    $master_icaps = [];
                    foreach ($masters as $master) {
                        if ($master > 0) {
                            if (isset($master_icaps["".$master])){
                                $master_icaps["".$master] += 1;
                            } else {
                                $master_icaps["".$master] = 1;
                            }
                        }
                    }
                    asort($master_icaps);
                    array_reverse($master_icaps);
                    if (count($master_icaps) > 0) {
                        $correct_icap = array_key_first($master_icaps);
                        //dd($correct_icap);
                        $ur->corrected_icap = $correct_icap;
                        $ur->save();
                        //dd($ur);
                        echo "Updated ".$ur->account_id." from master table to $correct_icap \n";

                        continue;
                    }

                    $field = 'icap_'.$year;
                    if ($ur->account) {
                        if ($ur->account->$field != null) {
                            $ur->corrected_icap = $ur->account->$field;
                            $ur->save();
                            //echo "Updated ".$ur->account_id." ".$ur->account->$field."\n";
                        }
                    } else {
                        //echo "ACCOUNT ".$ur->account_id." MISSING\n";
                    }
                }
                echo "Another 1000 updated.\n";
            });
        }        

        

        // =======================================> ICAP SPLIT BY YEAR 

        if ($this->option('split')) {

            for ($year = date('Y'); $year > 2017; $year--) {
                echo "Starting Split $year\n";
                $this->icapSplitter($year);
            }
        }

        $this->endCrunch();


    }

    public function icapSplitter($mainyear)
    {
        $changedate = Carbon::parse($mainyear.'-06-01');
        //dd($changedate);

        $startrange = $changedate->copy()->startOfYear()->subMonths(3);
        $endrange = $changedate->copy()->endOfYear()->addMonths(3);

        $account_ids = UsageReading::groupBy('account_id')
                                   ->pluck('account_id')
                                   ->unique();
        $count = 0;
        $total = count($account_ids);
        foreach ($account_ids as $account_id) {
            $count++;
            $icaps = UsageReading::where('account_id', $account_id)
                             ->orderBy('corrected_ldc_read_month')
                             ->where('corrected_ldc_read_month', '>=', $startrange)
                             ->where('corrected_ldc_read_month', '<', $endrange)
                             ->whereNotNull('icap')
                             ->pluck('icap');

                             //dd($icaps);

            $unique_icaps = [];
            foreach ($icaps as $icap) {
                $unique_icaps[$icap.""] = 1;
            }

            echo $account_id." has ".count($unique_icaps)." ICAPs\n";

            if (count($unique_icaps) == 1) {
                $icaps = UsageReading::where('account_id', $account_id)
                             ->orderBy('corrected_ldc_read_month')
                             ->where('corrected_ldc_read_month', '>=', $startrange)
                             ->where('corrected_ldc_read_month', '<', $changedate)
                             ->whereNotNull('icap')
                             ->pluck('icap');
                if (count($icaps) > 0) {
                    $low = $icaps->first();
                    $usagereadings = UsageReading::where('account_id', $account_id)
                                         ->where('corrected_ldc_read_month', '>=', $startrange)
                                         ->where('corrected_ldc_read_month', '<', $changedate)
                                         ->orderBy('corrected_ldc_read_month')
                                         ->get();
                    foreach ($usagereadings as $reading) {
                        if ($reading->corrected_icap != $low) {
                            $this->crunch->addToLog($mainyear." Splitter: ".$reading->account_id. ": Corrected LOW, ".$reading->corrected_icap." != $low");
                            $reading->corrected_icap = $low;
                            $reading->save();
                        }
                    }
                }
                $icaps = UsageReading::where('account_id', $account_id)
                             ->orderBy('corrected_ldc_read_month')
                             ->where('corrected_ldc_read_month', '<', $endrange)
                             ->where('corrected_ldc_read_month', '>=', $changedate)
                             ->whereNotNull('icap')
                             ->pluck('icap');
                if (count($icaps) > 0) {
                    $high = $icaps->first();
                    $usagereadings = UsageReading::where('account_id', $account_id)
                                         ->where('corrected_ldc_read_month', '>=', $startrange)
                                         ->where('corrected_ldc_read_month', '<', $changedate)
                                         ->orderBy('corrected_ldc_read_month')
                                         ->get();
                    foreach ($usagereadings as $reading) {
                        if ($reading->corrected_icap != $high) {
                            $this->crunch->addToLog($mainyear." Splitter: ".$reading->account_id. ": Corrected HIGH, ".$reading->corrected_icap." != $high");
                            $reading->corrected_icap = $high;
                            $reading->save();
                        }
                    }
                }
            }

            if (count($unique_icaps) == 2) {

                echo "Processing account ".$account_id.", $count of $total\n";

                $icaps = collect($icaps);
                $low = $icaps->first();
                $high = $icaps->last();

                //dd($low, $high);
                
                // if ($account->account_id == '11041381028') {
                //     //dd($low, $high);  
                // }
                // //
                // if ($account->account_id == '11041381028') {
                //     UsageReading::where('account_id', $account_id)->orderBy('corrected_ldc_month')->get();
                // }

                $usagereadings = UsageReading::where('account_id', $account_id)
                                         ->where('corrected_ldc_read_month', '>=', $startrange)
                                         ->where('corrected_ldc_read_month', '<', $endrange)
                                         ->orderBy('corrected_ldc_read_month')
                                         ->get();

                                         //dd($usagereadings->pluck('icap'), $usagereadings->pluck('corrected_ldc_read_month'), $startrange);

                //dd($usagereadings->pluck('corrected_icap'));
                foreach ($usagereadings as $reading) {

                    //echo $reading->corrected_ldc_read_month." ".$reading->corrected_icap."\n";

                    if ($reading->corrected_ldc_read_month < $changedate) {

                        $diff = abs($reading->corrected_icap - $low);


                        if ($diff) {

                            echo $mainyear." Splitter: ".$reading->account_id. ": Corrected LOW, ".$reading->corrected_icap." != $low DIFF = $diff\n";
                            //dd("WHAT?? ".$reading->corrected_icap." != $low", $reading->corrected_ldc_month, $reading->icap, $reading->corrected_icap, $low, $high);
                            $reading->corrected_icap = $low;
                            $reading->save();

                            
                        }
                        
                    } else {
                        if ((float) $reading->corrected_icap != (float) $high) {
                            $reading->corrected_icap = $high;
                            $reading->save();
                            echo $mainyear." Splitter: ".$reading->account_id. ": Corrected HIGH\n";
                        }
                        
                    }
                }
            }
        }
    }
}
