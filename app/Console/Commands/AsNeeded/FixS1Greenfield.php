<?php

namespace App\Console\Commands\AsNeeded;

use Illuminate\Console\Command;
use App\UsageReading;
use App\SalesUsageReading;
use App\Traits\CrunchTrait;

class FixS1Greenfield extends Command
{
    use CrunchTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:fix_s1 {--type=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets the S1 that shouldnt be S1';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $city = $this->startCrunch('fix_s1', 'greenfield');

        

        $type = 'usage';
        if ($this->option('type')) {
            $type = $this->option('type');
        }
        $query = UsageReading::query();
        if ($type == 'sales') {
            $query = SalesUsageReading::query();
        }
        $readings = $query->where('corrected_rate_class', 'S1')
                          ->where('corrected_icap', '>', 0)
                          ->get();

        foreach ($readings as $reading) {
            if ($reading->corrected_rate_class != $reading->base_rate_class) {
                $reading->corrected_rate_class = $reading->base_rate_class;
                $reading->save();
                echo $reading->account_id." updated to ".$reading->corrected_rate_class."\n";
                break;
            }
            echo $reading->account_id." updated icap to 0\n";
            $reading->corrected_icap = 0;
            $reading->save();
        }

    }
}
