<?php

namespace App\Console\Commands\AsNeeded;

use Illuminate\Console\Command;
use DB;
use App\UsageReading;

class CopyFromDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:copy_from_db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Brings in readings from an existing daabase';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $olddb = 'acton';
        $newdb = 'bsc_acton';

        

        DB::purge('mysql');
        config(['database.connections.mysql.database' => $olddb]);
        $oldur = UsageReading::first();

        $chunksize = 1000;
        $max = UsageReading::max('id');
        $id = 0;

        DB::purge('mysql');
        config(['database.connections.mysql.database' => $newdb]);
        $newur = UsageReading::first();

        $valid_attributes = [];
        foreach ($oldur->getAttributes() as $attr => $val) {
            // dd($key, $attr);
            if (isset($newur->$attr)){
                $valid_attributes[] = $attr;
            }
        }

        $convert_attributes = [];
        $convert_attributes['utility_account_number'] = 'account_id';
        //dd($valid_attributes);

        while ($id <= $max) {

            DB::purge('mysql');
            config(['database.connections.mysql.database' => $olddb]);

            $oldurs = UsageReading::where('id', '>=', $id)->take($chunksize)->orderBy('id')->get();

            DB::purge('mysql');
            config(['database.connections.mysql.database' => $newdb]);

            foreach ($oldurs as $oldur) {
                $ur = UsageReading::where('account_id', $oldur->utility_account_number)
                                  ->where('usage_kwh', $oldur->usage_kwh)
                                  ->where('usage_to_date', $oldur->usage_to_date)
                                  ->first();
                if (!$ur) { 
                    $ur = new UsageReading;
                    foreach ($valid_attributes as $attr) {
                        $ur->$attr = $oldur->$attr;
                    }
                    foreach ($convert_attributes as $oldattr => $newattr) {
                        $ur->$newattr = $oldur->$oldattr;
                    }
                    unset($ur->id);
                    $ur->save();
                } else {
                    echo "Already in DB ".$ur->utility_account_number." ".$ur->usage_to_date->format('n/j/Y')."\n";
                }
            }

            $id += $chunksize;
            echo "$id of $max \r";
        }

        

        //$db->select
    }
}
