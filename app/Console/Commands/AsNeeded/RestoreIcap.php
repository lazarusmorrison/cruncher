<?php

namespace App\Console\Commands\AsNeeded;

use Illuminate\Console\Command;
use App\SalesUsageReading;
use App\UsageReading;
use App\Traits\CrunchTrait;

class RestoreIcap extends Command
{

    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:restore_icap {--city=} {--year=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Used to fix lincoln icap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->option('city')) {
            dd('need city like php artisan crunch:restore_icap --city=lincoln');
        }
        if (!$this->option('year')) {
            dd('need year like php artisan crunch:restore_icap --city=lincoln --year=2022');
        }

        $city = $this->startCrunch('restore_icap', $this->option('city'));

        $start = ($this->option('year')).'-06-01';
        $end   = ($this->option('year')+1).'-05-31';
        $sales_readings = SalesUsageReading::where('corrected_ldc_read_month', '>=', $start)
                                           ->where('corrected_ldc_read_month', '<=', $end)
                                           ->get();

        echo "About to fix ".$sales_readings->count()." readings.\n";

        foreach ($sales_readings as $sr) {
            $ur = UsageReading::where('account_id', $sr->account_id)
                              ->where('usage_to_date', $sr->usage_to_date)
                              ->where('usage_kwh', $sr->usage_kwh)
                              ->first();
            if ($ur) {
                $beforeicap = $ur->corrected_icap;
                $ur->corrected_icap = $sr->corrected_icap;
                $ur->icap = $sr->icap;

                $dirty = $ur->getDirty();
                //dd($dirty, $beforeicap);
                $ur->save();
                echo "Saved ".$ur->id.": ".$ur->account_id."\n";
            }
        }
    }
}
