<?php

namespace App\Console\Commands\AsNeeded;

use Illuminate\Console\Command;
use App\City;
use App\Account;
use App\Traits\CrunchTrait;
use DB;
use App\UsageReading;

class CorrectRateFromTable extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:correct_rate_from_table {--table=} {--city=} {--skipfirst} {--field=} {--account_field=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('city')) {
            $cities = City::where('name', $this->option('city'))->get();
        } else {
            dd("forgot city, dummy, like php artisan crunch:correct_rate_from_table ");
        }

        if ($this->option('table')) {
            $table = $this->option('table');
        } else {
            dd("forgot table, dummy, like php artisan crunch:correct_rate_from_table --table=SALEM_BAD_RATES");
        }

        if ($this->option('field')) {
            $field = $this->option('field');
        } else {
            dd("forgot field, dummy, like php artisan crunch:correct_rate_from_table --field=field3");
        }

        if ($this->option('account_field')) {
            $account_field = $this->option('account_field');
        } else {
            dd("forgot account_field, dummy, like php artisan crunch:correct_rate_from_table --account_field=field1");
        }

        $fixed = 0;
        $skip = true;

        $field = str_replace('+', ' ', $field);
        $account_field = str_replace('+', ' ', $account_field);

        //dd("Ok");

        foreach ($cities as $city) {



            $city_name = $this->startCrunch('correct_rate_from_table', $city->name);

            $bad_account_ids = UsageReading::whereNull('corrected_rate_class')->pluck('account_id')->unique();
            //dd($bad_account_ids);


            $correct_rates = DB::table($table)->whereIn($account_field, $bad_account_ids)->get();

            //dd($correct_rates);
            foreach ($correct_rates as $cr) {

                if ($this->option('skipfirst') && $skip) {
                    $skip = false;
                    continue;
                }
                if (strlen($cr->$field) < 2) {
                    continue;
                }
                $account_id = str_pad($cr->$account_field, 10, '0', STR_PAD_LEFT);
                $corrected_rate_class = substr($cr->$field, 0, 2);

                //dd($account_id, $corrected_rate_class);
                $urs = UsageReading::whereNull('corrected_rate_class')
                                   ->where('account_id', $account_id)
                                   ->get();
                foreach ($urs as $ur) {
                    $ur->corrected_rate_class = $corrected_rate_class;
                    //dd($ur);
                    $fixed++;
                    echo "Fixed: ".$fixed."\r";
                    //dd($ur);
                    $ur->save();
                }

                $account = Account::find($account_id);
                $account->base_rate_class = $corrected_rate_class;
                //dd($account);
                $account->save();
            }
        }
    }
}
