<?php

namespace App\Console\Commands\AsNeeded;

use Illuminate\Console\Command;
use DB;
use App\Crunch;
use App\DataImport;
use App\Traits\CrunchTrait;

class Filter extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:filter {--city=} {--import=} {--import_str=} {--field=} {--remove=} {--keep=} {--table=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes or keeps records based on original field';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $city = $this->startCrunch('filter', $this->option('city'));

        $imports = [];
        $import_id = $this->option('import');

        if ($import_id) {
            $imports = DataImport::where('id', $import_id)->get();
        } else if ($this->option('import_str')) {
            //dd($this->option('import_str'));
            $imports = DataImport::where('name', 'LIKE', '%'.$this->option('import_str').'%')->get();
        }

        //dd($imports);
        $table = "";
        if ($this->option('table')) {
            $table = $this->option('table');
        }

        foreach ($imports as $import) {

            if (!$table) {
                

                if ($import->type == 'commission') {
                    $table = 'commission_usage_readings';
                }
                if ($import->type == 'sales') {
                    $table = 'sales_usage_readings';
                }

                if (!$table) {
                    if (!$this->option('import_str')) {
                        dd("No table defined");
                    }
                    continue;
                }
            }

            if ($this->option('remove')) {
                $remove_str = $this->option('remove');
                if ($this->option('field')) {
                    $remove_str = $this->option('field').'\":\"'.$remove_str;
                }
                //dd($remove_str);
                $to_remove = DB::select('select * from '.$table.' where usage_file_id = '.$import->id.'
                    AND all_fields LIKE "%'.$remove_str.'%"');

                $count = count($to_remove);

                if ($count > 0) {
                    $this->crunch->addToLog("About to delete ".$count." from $table ".$import->name."\n");

                    $delete_query = 'delete from '.$table.' where usage_file_id = '.$import->id.' AND all_fields LIKE "%'.$remove_str.'%"';
                    //dd($delete_query);
                    DB::statement($delete_query);
                    //dd(count($to_keep));
                }
                // do same but for main table
                echo "removing from main\n";
                if (!$this->option('table')) {
                    $to_remove = DB::select('select * from usage_readings where usage_file_id = '.$import->id.'
                        AND all_fields LIKE "%'.$remove_str.'%"');

                    $count = count($to_remove);

                    if ($count > 0) {
                        $this->crunch->addToLog("About to delete ".$count." from usage_readings ".$import->name."\n");

                        $delete_query = 'delete from usage_readings where usage_file_id = '.$import->id.' AND all_fields LIKE "%'.$remove_str.'%"';
                        //dd($delete_query);
                        DB::statement($delete_query);
                        //dd(count($to_keep));
                    }
                }
            }

            if ($this->option('keep')) {
                $keep_str = $this->option('keep');
                if ($this->option('field')) {
                    $keep_str = $this->option('field').'\":\"'.$keep_str;
                }

                $keep_query = 'select COUNT(*) as count from '.$table.' where usage_file_id = '.$import->id.' AND all_fields NOT LIKE "%'.$keep_str.'%"';
                $to_keep = DB::select($keep_query);
                $count = $to_keep[0]->count;

                if ($count > 0) {
                    $this->crunch->addToLog("About to delete ".$count." from $table ".$import->name."\n");

                    $delete_query = 'delete from '.$table.' where usage_file_id = '.$import->id.' AND all_fields NOT LIKE "%'.$keep_str.'%"';
                    //dd($delete_query);
                    DB::statement($delete_query);
                    //dd(count($to_keep));
                }

                // do same but for main table
                echo "removing from main\n";
                if (!$this->option('table')) {
                    $keep_query = 'select COUNT(*) as count from usage_readings where usage_file_id = '.$import->id.' AND all_fields NOT LIKE "%'.$keep_str.'%"';
                    $to_keep = DB::select($keep_query);
                    $count = $to_keep[0]->count;

                    if ($count > 0) {
                        $this->crunch->addToLog("About to delete ".$count." from usage_readings ".$import->name."\n");

                        $delete_query = 'delete from usage_readings where usage_file_id = '.$import->id.' AND all_fields NOT LIKE "%'.$keep_str.'%"';
                        //dd($delete_query);
                        DB::statement($delete_query);
                        //dd(count($to_keep));
                    }
                }
            }
        }

        $this->endCrunch();
        
    }
}
