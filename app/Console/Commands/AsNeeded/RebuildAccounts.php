<?php

namespace App\Console\Commands\AsNeeded;

use Illuminate\Console\Command;
use App\CommissionUsageReading;
use App\UsageReading;
use App\SalesUsageReading;
use App\Account;
use App\MasterMonthlyAccount;
use DB;

class RebuildAccounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:rebuild_accounts {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('city')) {
            $city = strtolower($this->option('city'));
            $db = 'bsc_'.$city;
        } else {
            dd("Forgot city dummy, like php artisan crunch:copy --city=natick");
        }
        DB::purge('mysql');
        config(['database.connections.mysql.database' => $db]);

        echo "ACCOUNTS REBUILD\n";

        Account::truncate();

        $usage_accounts = UsageReading::groupBy('account_id')
                                         ->pluck('account_id');

        $sales_accounts = SalesUsageReading::groupBy('account_id')
                                         ->pluck('account_id');

        $commission_accounts = CommissionUsageReading::groupBy('account_id')
                                         ->pluck('account_id');

        $active_accounts = MasterMonthlyAccount::groupBy('account_id')
                                         ->pluck('account_id');


        $accounts = $usage_accounts->merge($sales_accounts)
                                 ->merge($commission_accounts)
                                 ->merge($active_accounts)
                                 ->unique()
                                 ->sort();

        $rowcount = 0;
        foreach ($accounts->chunk(371) as $chunk) {

            foreach ($chunk as $accountid) {
                $rowcount++;
                $account = new Account;
                $account->city = $city;
                $account->id = $accountid;

                $last_usage = UsageReading::where('account_id', $accountid)
                ->orderBy('usage_to_date', 'desc')
                ->first();
                if ($last_usage) {
                    $account->read_cycle = $last_usage->corrected_read_cycle;
                    $account->icap = $last_usage->corrected_icap;
                    $account->rate_class = $last_usage->corrected_rate_class;
                    $account->base_rate_class = $last_usage->corrected_rate_class;
                }
                $account->save();

            }

        }
    }
}
