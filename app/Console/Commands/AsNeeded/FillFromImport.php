<?php

namespace App\Console\Commands\AsNeeded;

use Illuminate\Console\Command;
use App\MasterMonthlyAccount;
use App\UsageReading;
use App\Account;
use App\Crunch;
use DB;

class FillFromImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:fill_from_import {--city=} {--import=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('city')) {
            $city = strtolower($this->option('city'));
            $db = 'bsc_'.$city;
        } else {
            dd("Forgot city dummy, like php artisan crunch:fill_from_import --city=natick --import=50");
        }

        $import_id = $this->option('import');

        if (!$import_id) {
            dd("Forgot import dummy, like php artisan crunch:fill_from_import --city=natick --import=50");
        }

        DB::purge('mysql');
        config(['database.connections.mysql.database' => $db]);

        // create CRUNCH but don't save unless it succeeds
        $crunch = new Crunch;
        $crunch->city = $city;
        $crunch->name = 'fill_from_import';

        
        $masters = MasterMonthlyAccount::where('usage_file_id', $import_id)->get();

        foreach ($masters as $master) {
            $account_id = $master->account_id;

            if (!is_numeric($account_id)) {
                echo "ERROR:: =====================> $account_id \n";
                if (str_contains($account_id, ':001')) {
                    $account_id = str_replace(':001', '', $account_id);
                }
                if (str_contains($account_id, 'DUP')) {
                    $account_id = str_replace('DUP', '', $account_id);
                }
                if (!is_numeric($account_id)) {
                    dd("BAD ID");
                }
            }

            $year = $master->month->subMonths(5)->year;
            $icap = $master->icap;
            $base_rate_class = $master->base_rate_class;
            $rate_class = $master->rate_class;
            $read_cycle = $master->read_cycle;

            echo "$account_id, $year, $icap, $rate_class, $read_cycle \n";
            //dd();
            $account = Account::find($account_id);

            try {
                if (!$account) {

                    $account = new Account;
                    $account->city = $city;
                    $account->id = $account_id;
                    $account->save();
                }

                if ($icap != null) {
                    $icap_year = 'icap_'.$year;
                    if ($account->$icap_year == null) {
                        $account->$icap_year = $icap;
                        echo $account_id.": ICAP YEAR $year updated to $icap \n";
                        $account->save();
                        
                    }
                }
                if ($rate_class != null) {
                    
                    if ($account->rate_class != $rate_class) {
                        $old = $account->rate_class;
                        $account->rate_class = $rate_class;
                        $account->base_rate_class = $base_rate_class;
                        echo $account_id.": RATE CLASS updated to $rate_class from $old \n";
                        $account->save();
                        
                    }
                }
                if ($read_cycle != null) {
                    
                    if ($account->read_cycle != $read_cycle) {
                        $old = $account->read_cycle;
                        $account->read_cycle = $read_cycle;
                        echo $account_id.": READ CYCLE updated to $read_cycle from $old \n";
                        $account->save();
                        
                    }
                }
            } catch (\Exception $e) {
                dd($account, $e->getMessage());
            }
        }
    }
}
