<?php

namespace App\Console\Commands\AsNeeded;

use Illuminate\Console\Command;
use App\City;
use DB;

class MigrateAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:migrate_all {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Loops through cities and migrates each db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('city')) {
            $cities = City::where('name', strtolower($this->option('city')))->get();
        } else {
            $cities = City::orderBy('name')->get();
        }
        //dd($cities->pluck('name'));
        

        foreach ($cities as $city) {
            $db = 'bsc_'.$city->name;
            DB::purge('mysql');
            config(['database.connections.mysql.database' => $db]);

            try {
                echo "Migrating ".$db."\n";
                $this->call('migrate', ['--force' => true]);
            } catch (\Exception $e) {
                echo $db." FUCKED UP, ".$e->getMessage()."\n";
            }

        }
    }
}
