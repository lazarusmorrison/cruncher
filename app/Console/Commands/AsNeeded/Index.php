<?php

namespace App\Console\Commands\AsNeeded;

use DB;
use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class Index extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cf:index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds Indexes most likely used for Constituents Search page';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public $state;
    public $table;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $tables = [
            'commission_readings',
            'usage_readings',
            'combined_readings',
        ];

        
        $indexes = [

            ['city', 'account_id'],
            ['city', 'corrected_ldc_read_month'],
            ['city', 'corrected_icap'],
            ['city', 'corrected_read_cycle'],

            ['corrected_ldc_read_month'],
            ['sync_id'],

        ];


        foreach ($indexes as $index_arr) {
            foreach ($table as $tablename) {
                $this->addIndexToTable($tablename, $index_arr);
            }
        }

    }


    public function addIndexToTable($tablename, $index_arr)
    {
        //dd($index_arr);
        echo date('Y-m-d h:i:s', time())." - $tablename: ".implode(', ', $index_arr);
        Schema::connection('consolidated')->table($tablename, function (Blueprint $table) use ($index_arr, $tablename) {
            $index_name = 'idx_master_'.implode('_', $index_arr);
            $existing_indexes = collect(DB::select('SHOW INDEXES FROM '.$tablename))
                                    ->pluck('Key_name');
            //dd($existing_indexes);
            if ($existing_indexes->contains($index_name)) {
                echo " => Exists, SKIPPING.\n";

                return;
            } else {
                echo "\n";
            }
            $table->index($index_arr, $index_name);
        });
    }
}
