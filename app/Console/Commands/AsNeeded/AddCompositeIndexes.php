<?php

namespace App\Console\Commands\AsNeeded;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use DB;
use App\City;

class AddCompositeIndexes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:composite {--city=} {--remove}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds Indexes most likely used for Constituents Search page';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cities = City::all();
        if ($this->option('city')) {
            $cities = City::where('name', $this->option('city'))->get();
            
        }

        foreach ($cities as $city) {

            $city_name = $city->name;
            // DB::setFetchMode(\PDO::FETCH_ASSOC);
            DB::purge('mysql');
            config(['database.connections.mysql.database' => 'bsc_'.$city_name]);

            $indexes = [

                ['correction', 'usage_kwh', 'account_id', 'usage_to_date'],
                ['usage_kwh', 'account_id', 'usage_to_date'],

                ['account_id', 'corrected_ldc_read_month'],
                ['account_id', 'corrected_rate_class'],
                ['account_id', 'corrected_read_cycle'],
                
                ['correction', 'corrected_ldc_read_month'],
                ['correction', 'corrected_rate_class'],
                ['correction', 'corrected_icap'],

                ['corrected_ldc_read_month', 'corrected_rate_class'],
                ['corrected_ldc_read_month', 'corrected_icap'],

                ['corrected_icap', 'corrected_rate_class'],
                ['corrected_rate_class', 'corrected_read_cycle'],

                ['corrected_ldc_read_month', 'id', 'monthly_processed_at'],
            ];

            $monthly_indexes = [
                ['usage_reading_id', 'month'],
            ];

            foreach ($indexes as $index_arr) {
                if ($this->option('remove')) {
                    $this->removeIndex($index_arr);
                } else {
                    $this->addIndexToAll($index_arr);
                }
                
            }

           

            foreach ($monthly_indexes as $index_arr) {
                $this->addIndexToMonthly($index_arr);
            }
        }


    }

    public function removeIndex($index_arr) 
    {
        
        $tablename = "usage_readings";
        Schema::table($tablename, function (Blueprint $table) use ($index_arr, $tablename) {

            $index_name = "idx_master_".implode('_', $index_arr);
            $existing_indexes = collect(DB::select("SHOW INDEXES FROM ".$tablename))
                                    ->pluck('Key_name');
            //dd($existing_indexes);
            if (!$existing_indexes->contains($index_name)) {

                echo " => GONE, SKIPPING.\n";
                return;
            } else {
                echo "\n";
            }
            $table->dropIndex($index_name);
        });
    }

    public function addIndexToAll($index_arr) 
    {
        $this->addIndexToTable('usage_readings', $index_arr);
        // $this->addIndexToTable('sales_usage_readings', $index_arr);
        // $this->addIndexToTable('commission_usage_readings', $index_arr);
    }
    public function addIndexToMonthly($index_arr) 
    {
        $this->addIndexToTable('monthly_readings', $index_arr);
    }

    public function addIndexToTable($tablename, $index_arr) 
    {
        //dd($index_arr);
        echo date('Y-m-d h:i:s', time())." - $tablename: ".implode(', ', $index_arr);
        Schema::table($tablename, function (Blueprint $table) use ($index_arr, $tablename) {

            $index_name = "idx_master_".implode('_', $index_arr);
            $existing_indexes = collect(DB::select("SHOW INDEXES FROM ".$tablename))
                                    ->pluck('Key_name');
            //dd($existing_indexes);
            if ($existing_indexes->contains($index_name)) {

                //echo " => Exists, SKIPPING.\n";
                return;
            } else {
                echo "\n";
            }
            $table->index($index_arr, $index_name);
        });
    }
}
