<?php

namespace App\Console\Commands\AsNeeded;

use Illuminate\Console\Command;

use App\Traits\CrunchTrait;
use Carbon\Carbon;
use App\SalesUsageReading;
use App\CommissionUsageReading;
use App\City;

class MarkCityAsCopied extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:mark_as_copied {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cities = [];
        if ($this->option('city') == 'all') {
            $cities = City::orderBy('name')->pluck('name');
        } else {
            $cities[] = $this->option('city');
        }
        
        
        foreach ($cities as $city) {
            $city = $this->startCrunch('mark_as_copied', $city);
            SalesUsageReading::whereNull('copied_at')->update(['copied_at' => Carbon::now()]);
            CommissionUsageReading::whereNull('copied_at')->update(['copied_at' => Carbon::now()]);
            $this->endCrunch();
        }
        
    }
}
