<?php

namespace App\Console\Commands\AsNeeded;

use Illuminate\Console\Command;

class DynegyCorrections extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:dynegy_corrections';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dir = explode('/', __DIR__);
        foreach($dir as $key => $segment) {
            if (!$segment) {
                unset($dir[$key]);
            }
        }
        $userpath = '/'.$dir[1].'/'.$dir[2];

        $localpath = storage_path().'/app/sync_folder/FromPeregrine/';

        if (!file_exists($localpath)) {
            dd($localpath.' does not exist.');
        }

        $cities = [
            'Acton',
            'Bellingham',
            'Cambridge',
            'Chelmsford',
            'Foxboro',
            'Grafton',
            'Greenfield',
            'Lexington',
            'Nantucket',
            'Natick',
            'Newton',
            'Salem',
            'Southboro',
            'Sutton',
            'Swampscott',
            'Walpole',
            'Watertown',
            'Webster',
            'Westboro',
            'Worcester',
        ];
        foreach ($cities as $city) {
            echo "****** STARTING ".strtoupper($city)." ****** \n";
            $local_city_folder = $localpath."/".ucwords($city);
            
            $localfiles = glob($local_city_folder."/*");
            foreach ($localfiles as $localfile) {

                $filename = strtoupper(basename($localfile));

                if (stripos($filename, 'corrected')) {
                    if (stripos($filename, 'TO_DELETE')) {
                        $filename = str_replace('_TO_DELETE_', '', $filename);
                    }
                    if (stripos($filename, '.2021 ')) {
                        $filename = str_replace('.2021 ', '_2021', $filename);
                    }
                    if (stripos($filename, ' _2021')) {
                        $filename = str_replace(' _2021', '_2021_', $filename);
                    }
                    if ($localfile != $local_city_folder."/".$filename) {
                        rename($localfile, $local_city_folder."/".$filename);
                        echo "Renamed to ".$filename."\n";
                    }
                }
            }
            
        }
    }
}
