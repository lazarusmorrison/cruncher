<?php

namespace App\Console\Commands\AsNeeded;

use Illuminate\Console\Command;
use App\Traits\CrunchTrait;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ICAPImport;
use App\DataImport;

class FillICAPFromFile extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:fill_icap_from_file {--city=} {--year=} {--file=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Just imports one file and fills the icap for the correct year';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $city = $this->startCrunch('fill_icap_from_file', $this->option('city'));

        $year = $this->option('year');
        if (!$year) {
            dd('Forgot year, dummy, like php artisan crunch:fill_icap_from_file --city=natick --year=2019');
        }

        $data_import = DataImport::where('month', '<', ($year+1).'-06-01')
                                 ->latest()
                                 ->first();

        if ($this->option('file')) {
            $data_import = DataImport::where('month', '<', ($year+1).'-06-01')
                                 ->where('name', 'LIKE', '%'.$this->option('file').'%')
                                 ->latest()
                                 ->first();
        }
        //dd($data_import);
        echo $data_import->file_path."\n";

        Excel::import(new ICAPImport($year, 1, 'Account #', "ICAP 2023 - 2024", $data_import->sheets[0]), $data_import->file_path);

    }
}
