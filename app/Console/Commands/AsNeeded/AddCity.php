<?php

namespace App\Console\Commands\AsNeeded;

use Illuminate\Console\Command;

class AddCity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:add_city';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds a new city database with all tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // database must already exist
        $city = $this->option('city');

        $db = 'bsc_'.$city;
        DB::purge('mysql');
        config(['database.connections.mysql.database' => $db]);

        try {
            echo "Migrating ".$db."\n";
            $this->call('migrate');
        } catch (\Exception $e) {
            echo $db." FUCKED UP, ".$e->getMessage()."\n";
        }

        
    }
}
