<?php

namespace App\Console\Commands\AsNeeded;

use DB;
use Illuminate\Console\Command;
use App\Imports\FinalImport;
use Maatwebsite\Excel\Facades\Excel;

class JoeFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:joe_file {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('city')) {
            $city = strtolower($this->option('city'));
            $db = 'bsc_'.$city;
        } else {
            dd("Forgot city dummy, like php artisan crunch:joe_file --city=natick");
        }
        DB::purge('mysql');
        config(['database.connections.mysql.database' => $db]);

        Excel::import(new FinalImport, storage_path().'/app/imports/Greenfield_appendixB (Apr 2019 - Mar 2020).xlsx');
        
    }
}
