<?php

namespace App\Console\Commands\AsNeeded;

use Illuminate\Console\Command;
use App\UsageReading;
use App\SalesUsageReading;
use App\CommissionUsageReading;
use DB;
use App\DataImport;

class RemoveImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:remove_import {--city=} {--import=} {--ids=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove Import by id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!file_exists(storage_path().'/app/removed')) {
            mkdir(storage_path().'/app/removed');
        }
        $import_id = null;
        $imports = null;

        if ($this->option('city')) {
            $city = strtolower($this->option('city'));
            $db = 'bsc_'.$city;
        } else {
            dd("Forgot city dummy, like php artisan crunch:remove_import --city=natick");
        }

        DB::purge('mysql');
        config(['database.connections.mysql.database' => $db]);

        if ($this->option('import')) {
            $import_id = strtolower($this->option('import'));
            $imports = DataImport::where('id', $import_id)->get();
        } else if ($this->option('ids')) {
            $ids = explode(',', $this->option('ids'));
            $imports = DataImport::whereIn('id', $ids)->get();
        } else {
            DB::purge('mysql');
            config(['database.connections.mysql.database' => $db]);
            $imports = DataImport::orderBy('month')->get();
            $imports_arr = [];
            foreach ($imports as $import) {
                $imports_arr[] = "ID: ".$import->id." \t ".$import->month->format('Y-m-d')." - ".$import->name;
            }
            $import_name = $this->choice(
                    'Which import should be removed?',
                    array_merge(['Never Mind - Exit'], $imports_arr),
                    0
                );
            $import_arr = explode("\t", $import_name);
            if (isset($import_arr[0]) && count($import_arr) > 1) {
                $import_id = str_replace('ID: ', '', $import_arr[0]);
            }

            $imports = DataImport::where('id', $import_id)->get();
        }
        if (!$imports) {
            dd("No Import IDs Chosen");
        }

        foreach ($imports as $import) {

            $correct_file = $this->confirm("Is this the right file? ".$import->name);

            if (!$correct_file) {
                dd("Whoops. Wrong file chosen, start over!");
            }
            //dd("Laz");
            $usage_count = UsageReading::where('usage_file_id', $import_id)->count();
            $sales_count = SalesUsageReading::where('usage_file_id', $import_id)->count();
            $comms_count = CommissionUsageReading::where('usage_file_id', $import_id)->count();

            echo ucwords($city).": ".$import->name."\n";
            echo "Usage: ".$usage_count."\n";
            echo "Sales: ".$sales_count."\n";
            echo "Comms: ".$comms_count."\n";
            if ($this->confirm('Does this look right? Want to delete these permanently?')) {
                $usage_deleted = UsageReading::where('usage_file_id', $import_id)->delete();
                $sales_deleted = SalesUsageReading::where('usage_file_id', $import_id)->delete();
                $comms_deleted = CommissionUsageReading::where('usage_file_id', $import_id)->delete();

                $import->import_done = false;
                $import->count = null;
                $import->save();

                $import->delete();

                echo "Deleted $usage_deleted, $sales_deleted, $comms_deleted. About to delete actual file.\n";

                $currpath_arr = explode('storage', $import->file_path);

                if (isset($currpath_arr[1])) {
                    $currpath = storage_path().$currpath_arr[1];
                    if (file_exists($currpath)) {
                        $newpath = storage_path().'/app/removed/'.basename($currpath);
                        rename($currpath, $newpath);
                        $this->line('Moved file to storage/app/removed');
                    }else {
                        $this->error('Original file not found.');
                    }
                } else {
                    $this->error('Bad file path: '.$import->file_path);
                }

            }
        }
    }
}
