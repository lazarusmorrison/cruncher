<?php

namespace App\Console\Commands\AsNeeded;

use Illuminate\Console\Command;
use App\City;
use App\DataImport;
use DB;
use Illuminate\Support\Str;

class FixFilePaths extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:fix_file_paths {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cities = City::all();
        if ($this->option('city')) {
            $cities = City::where('name', $this->option('city'))->get();
        }

        foreach ($cities as $city) {

            $city_name = $city->name;

            // DB::setFetchMode(\PDO::FETCH_ASSOC);
            DB::purge('mysql');
            config(['database.connections.mysql.database' => 'bsc_'.$city_name]);

            $files = DataImport::all();

            $itshere_arr = [];
            $missing_arr = [];

            foreach ($files as $file) {
                $full_path = $file->file_path;
                if (Str::startsWith($full_path, 'data_files/')) {
                    $full_path = str_replace('data_files/', 
                                         '/home/forge/crunch.er/storage/app/data_files/', 
                                         $full_path);
                }
                $full_path = str_replace('../baystate/', 
                                         '/home/forge/crunch.er/', 
                                         $full_path);
                $full_path = str_replace('/Users/lazimac/Developer/forge/baystate/', 
                                         '/home/forge/crunch.er/', 
                                         $full_path);

                
                if (!file_exists($full_path)) {
                    $full_path = str_replace('_'.$city_name.'/', 
                                         '_'.ucwords($city_name).'/', 
                                         $full_path);

                    if (!file_exists($full_path)) {
                        $missing_arr[] = $full_path;
                    } else {
                        $itshere_arr[] = $full_path;
                        $file->file_path = $full_path;
                        $file->save();
                    }
                } else {
                    $itshere_arr[] = $full_path;
                    $file->file_path = $full_path;
                    $file->save();
                    
                }
                //echo "\t Missing: $missing_count \tFound: $filecount\r";
            }
            echo ucwords($city_name)."\t Missing: ".count($missing_arr)." \tFound: ".count($itshere_arr)."\n";

            print_r($missing_arr);

        }
    }
}











