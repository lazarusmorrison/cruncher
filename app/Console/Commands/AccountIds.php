<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CommissionUsageReading;
use App\UsageReading;
use App\SalesUsageReading;
use App\Account;
use App\City;
use App\MasterMonthlyAccount;
use DB;
use App\Traits\CrunchTrait;

class AccountIds extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:account_ids {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $cities = City::orderBy('name')->get();

        if ($this->option('city')) {
            $cities = City::where('name', $this->option('city'))->get();
        }
        echo "\n****************************** PREVIEW ******************************\n\n";
        
        foreach ($cities as $city) {

            $city_name = $this->startCrunch('account_ids', $city->name);

            try {
                $u_fixed = 0;
                echo "FIXING LEADING ZEROS\n";
                $urs_query = UsageReading::whereRaw('LENGTH(account_id) < 10');
                echo "Usage account_ids: ".$urs_query->count()."\n";
                $urs_query->chunkById(1000, function ($urs) use (&$u_fixed) {
                    echo count($urs)."\n";
                    foreach ($urs as $ur) {
                        $ur->account_id = str_pad($ur->account_id, 10, '0', STR_PAD_LEFT);
                        $ur->save();
                        $u_fixed++;
                    }
                });
                if ($u_fixed > 0) {
                    $this->crunch->addToLog('Usage Fixed: '.$u_fixed);
                }

                $s_fixed = 0;
                $urs_query = SalesUsageReading::whereRaw('LENGTH(account_id) < 10');
                echo "Sales account_ids: ".$urs_query->count()."\n";
                $urs_query->chunkById(1000, function ($urs) use (&$s_fixed) {
                    echo count($urs)."\n";
                    foreach ($urs as $ur) {
                        $ur->account_id = str_pad($ur->account_id, 10, '0', STR_PAD_LEFT);
                        $ur->save();
                        $s_fixed++;
                    }
                });
                if ($s_fixed > 0) {
                    $this->crunch->addToLog('Commission Fixed: '.$s_fixed);
                }

                $c_fixed = 0;
                $urs_query = CommissionUsageReading::whereRaw('LENGTH(account_id) < 10');
                echo "Commission account_ids: ".$urs_query->count()."\n";
                $urs_query->chunkById(1000, function ($urs) use (&$c_fixed) {
                    echo count($urs)."\n";
                    foreach ($urs as $ur) {
                        $ur->account_id = str_pad($ur->account_id, 10, '0', STR_PAD_LEFT);
                        $ur->save();
                        $c_fixed++;
                    }
                });
                
                    if ($c_fixed > 0) {
                        $this->crunch->addToLog('Commission Fixed: '.$c_fixed);
                    }
                

                $m_fixed = 0;
                $urs_query = MasterMonthlyAccount::whereRaw('LENGTH(account_id) < 10');
                echo "Master account_ids: ".$urs_query->count()."\n";
                $urs_query->chunkById(1000, function ($urs) use (&$m_fixed) {
                    echo count($urs)."\n";
                    foreach ($urs as $ur) {
                        $ur->account_id = str_pad($ur->account_id, 10, '0', STR_PAD_LEFT);
                        $ur->save();
                        $m_fixed++;
                    }
                });
                if ($m_fixed > 0) {
                    $this->crunch->addToLog('Master Fixed: '.$m_fixed);
                }

                $this->crunch->affected = $u_fixed + $s_fixed + $c_fixed;

                $this->endCrunch();
            } catch (\Exception $e) {
                dd($this->crunch, $e->getMessage());
            }
        }
    }
}
