<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UsageReading;
use DB;
use App\Crunch;
use Carbon\Carbon;
use App\Traits\CrunchTrait;

class Negatives extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:negatives {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Marks negative matches as corrections';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $city = $this->startCrunch('negatives', $this->option('city'));

        $total_unmarked = UsageReading::where('correction', false)
                                     ->where('usage_kwh', '<', 0)
                                     ->count();
        echo "Total Unmarked: ".$total_unmarked."\n";

        //dd($total_unmarked);

        // =======================================>

        $negatives = UsageReading::where('correction', false)
                                 ->where('usage_kwh', '<', 0)->get();

        foreach ($negatives as $negative) {
            $dupe = UsageReading::where('account_id', $negative->account_id)
                                ->where('usage_to_date', $negative->usage_to_date)
                                ->where('usage_kwh', abs($negative->usage_kwh))
                                ->where('correction', false)
                                ->first();
            if ($dupe) {
                $negative->correction = true;
                $negative->save();
                $negative->rowProcessed('equalNegatives', ['fix' => 'marked correction.']);

                $dupe->correction = true;
                $dupe->save();
                $dupe->rowProcessed('equalNegatives', ['fix' => 'marked correction.']);

                $this->crunch->addToLog("Account ".$negative->account_id." Marked two matching corrections ".$negative->corrected_ldc_read_month);

            }

        }
       

        $this->endCrunch();

    }
}
