<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\CrunchTrait;
use App\MasterMonthlyAccount;
use App\Account;
use App\City;

class Master extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:master {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if ($this->option('city')) {
            $cities = City::where('name', strtolower($this->option('city')))->get();
        } else {
            $cities = City::orderBy('name')->get();
        }

        foreach ($cities as $city) {

            $city = $this->startCrunch('master', $city->name);

            $acceptable_rate_classes = ['R1', 'R2', 'R3', 'R4', 'R5', 'R6',
                                        'G0', 'G1', 'G2', 'G3', 'G4', 'G5', 
                                        'S1', 'S2', 'S3', 'S4', 'S5', 
                                        'T0', 'T1', 'T2'];
            // Clear out duds
            MasterMonthlyAccount::whereNotNull('corrected_rate_class')
                                ->whereNotIn('corrected_rate_class', $acceptable_rate_classes)
                                ->update(['corrected_rate_class' => null]);

            $nullaccounts = MasterMonthlyAccount::whereNull('corrected_read_cycle')->pluck('account_id')->unique();

            Account::whereIn('id', $nullaccounts)
                   ->whereNotNull('read_cycle')->chunkById(1000, function($accounts) {
                echo "Another 1000... ";
                $fixed = 0;
                foreach ($accounts as $account) {
                    $fixed += MasterMonthlyAccount::whereNull('corrected_read_cycle')
                                        ->where('account_id', $account->id)
                                        ->update(['corrected_read_cycle' => $account->read_cycle]);
                }
                echo "Fixed ".$fixed."\n";

            });

            $nullaccounts = MasterMonthlyAccount::whereNull('corrected_rate_class')->pluck('account_id')->unique();
            Account::whereIn('id', $nullaccounts)
                   ->whereIn('base_rate_class', $acceptable_rate_classes)
                   ->chunkById(1000, function($accounts) {
                echo "Another 1000... ";
                $fixed = 0;
                foreach ($accounts as $account) {
                    $fixed += MasterMonthlyAccount::whereNull('corrected_rate_class')
                                        ->where('account_id', $account->id)
                                        ->update(['corrected_rate_class' => $account->base_rate_class]);
                }
                echo "Fixed ".$fixed."\n";

            });

            // ===========================================> Missed in Import

            

            $missed_count = MasterMonthlyAccount::where('all_fields', 'LIKE', '%RATE CLASS%')
                                        ->whereNull('corrected_rate_class')
                                        ->count();
            echo "Had RC Field: ".$missed_count."\n";

            $fixed_count = 0;
            MasterMonthlyAccount::where('all_fields', 'LIKE', '%RATE CLASS%')
                        ->whereNull('corrected_rate_class')
                        ->chunkById(10000, function($missed_rate) use (&$fixed_count, $acceptable_rate_classes) {
                foreach ($missed_rate as $index => $ur) {
                    //dd($ur->all_fields);
                    $allfields = $ur->all_fields;
                    foreach ($allfields as $key => $val) {
                        $allfields[strtoupper($key)] = $val;
                    }
                    if (isset($allfields['RATE CLASS'])) {
                        $actual = $allfields['RATE CLASS'];
                        if (in_array($actual, $acceptable_rate_classes)) {
                            $ur->corrected_rate_class = $actual;
                            $ur->save();
                            $fixed_count++;
                        }
                    } else {
                        print_r($allfields);
                        echo "RATE CLASS field not found\n";
                    }
                    //dd(Carbon::parse($actual), Carbon::parse('2015-01-01'));
                    
                }
                echo "Fixed: ".$fixed_count."\n";
            });

            $missed_count = MasterMonthlyAccount::where('all_fields', 'LIKE', '%ACCT STATUS%')
                                        ->whereNull('account_status')
                                        ->count();
            echo "Had Status Field: ".$missed_count."\n";

            $fixed_count = 0;
            MasterMonthlyAccount::where('all_fields', 'LIKE', '%ACCT STATUS%')
                        ->whereNull('account_status')
                        ->chunkById(10000, function($missed_status) use (&$fixed_count) {
                foreach ($missed_status as $index => $ur) {
                    //dd($ur->all_fields);
                    $allfields = $ur->all_fields;
                    foreach ($allfields as $key => $val) {
                        $allfields[strtoupper($key)] = $val;
                    }
                    $actual = $allfields['ACCT STATUS'];
                    if ($actual) {
                        $ur->account_status = $actual;
                        $ur->save();
                        $fixed_count++;
                    }
                    //dd(Carbon::parse($actual), Carbon::parse('2015-01-01'));
                    
                }
                echo "Fixed: ".$fixed_count."\n";
            });
        }

    }
}
