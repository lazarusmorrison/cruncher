<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\City;

class SyncRemote extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:sync_remote {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    /*
    LOCAL                                                   REMOTE
    ------------------              (CMDS)                  ---------------------
    Sync/From_Peregrin(/cities)     sync_remote --->        storage/sync_folder/from_peregrin(/cities)
                                    rename                  1. DELETED_ +
                                                            2. copies them ---> Mixed

    Sync/Mixed                                              storage/sync_folder/mixed

    One file:


    1. sync_remote      L       (First does #3)
                        L       Pushes file to REMOTE sync_folder/from_peregrin

    2. rename               R   Copies files to sync_folder/mixed
                            R   Renames (within sync_folder/from_peregrin) TO_DELETE+
                        L       Copies files to dev_folder/mixed
                        L       Rename (within dev_folder/from_peregrin) TO_DELETE


    3. sync_remote      L       Makes list of remote TO_DELETE files
                                Deletes corresponding files (without prefix) locally
                                (Normal sync_remote process will delete REMOTE files)
    */

    public function handle()
    {
        $dir = explode('/', __DIR__);
        foreach($dir as $key => $segment) {
            if (!$segment) {
                unset($dir[$key]);
            }
        }
        $userpath = '/'.$dir[1].'/'.$dir[2];

        $localpath = config('app.live_sync_client');

        $remotepath = '/home/forge/crunch.er/storage/app/sync_folder/FromPeregrine';

        // dd(glob($remotepath.'/*'));

        if (!file_exists($localpath)) {
            dd($localpath.' does not exist.');
        }

        $cities = [
            'Acton',
            'Bellingham',
            'Cambridge',
            'Chelmsford',
            'Foxboro',
            'Grafton',
            'Greenfield',
            'Lexington',
            'Lincoln',
            'Nantucket',
            'Natick',
            'Newton',
            'Salem',
            'Sharon',
            'Southboro',
            'Sutton',
            'Swampscott',
            'Walpole',
            'Watertown',
            'Webster',
            'Westboro',
            'Worcester',
        ];
        if ($this->option('city')) {
            $cities = [$this->option('city')];
        }
        foreach ($cities as $city) {
            echo "****** STARTING ".strtoupper($city)." ****** \n";
            $local_city_folder = $localpath."/".ucwords($city);
            $remote_city_folder = $remotepath."/".ucwords($city);
            //dd($local_city_folder, $remote_city_folder);

            $output = $this->liveExecuteCommand("ssh forge@billygoat.fluency.software 'ls ".$remote_city_folder."'");
            
            $file_arr = explode("\n", $output['output']);
            
            $localfiles = glob($local_city_folder."/*");

            //dd($localfiles, "Laz", $local_city_folder);
            foreach ($localfiles as $localfile) {
                $filename = strtoupper(basename($localfile));
                if (in_array('_TO_DELETE_'.$filename, $file_arr)) {
                    echo "\tSkipping $filename\n";
                    continue;
                }
                echo "YES: ".$localfile."\n";
                //continue;
                $this->info('Syncing file '.$localfile);

                $command = "rsync -zr --progress '".$localfile."' forge@billygoat.fluency.software:".$remote_city_folder.'/';
                echo $command;
                //dd();
                $this->liveExecuteCommand($command);
            }
            
        }


    }

    function liveExecuteCommand($cmd)
    {

        while (@ ob_end_flush()); // end all output buffers if any

        $proc = popen("$cmd 2>&1 ; echo Exit status : $?", 'r');

        $live_output     = "";
        $complete_output = "";

        while (!feof($proc))
        {
            $live_output     = fread($proc, 4096);
            $complete_output = $complete_output . $live_output;
            echo "$live_output";
            @ flush();
        }

        pclose($proc);

        // get exit status
        preg_match('/[0-9]+$/', $complete_output, $matches);

        // return exit status and intended output
        return array (
                        'exit_status'  => intval($matches[0]),
                        'output'       => str_replace("Exit status : " . $matches[0], '', $complete_output)
                     );
    }

    public function drawFishHead()
    {
        echo '
|\    \ \ \ \ \ \ \      __   
|  \    \ \ \ \ \ \ \   | O~-_
|   >----|-|-|-|-|-|-|--|  __/
|  /    / / / / / / /   |__\  
|/     / / / / / / /
        ';
    }
}
