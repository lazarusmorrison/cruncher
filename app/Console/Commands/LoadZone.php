<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UsageReading;
use App\Account;
use DB;
use App\Crunch;
use Carbon\Carbon;
use App\Traits\CrunchTrait;

class LoadZone extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:load_zone {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fills the load_zone when its stupid NULL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $city = $this->startCrunch('load_zone', $this->option('city'));

        $acceptable_load_zones = ['WCMASS', 'SEMASS', 'NEMASSBOST'];
        if ($this->option('city') == 'sutton') {
            $acceptable_load_zones = ['WCMA', 'SEMA'];
        }
        
        $acceptable_load_names = ["Load Zone", "LoadZone"];
        $acceptable_zip_names =  ["Bill Zip", "Zip"];

        $total_missing = Account::whereNull('load_zone')
                                     ->count();
        echo "Total Missing: ".$total_missing."\n";
        //dd();
        $have_zone = 0;
        $missing_zone = 0;
        $count = 0;
        $possible_zones = [];
        $possible_zips = [];

        if ($this->option('city') == 'sutton') {
            $map = [
                '.Z.WCMASS' => 'WCMA',
                '.Z.SEMASS' => 'SEMA',
                'ISNE-WCMA' => 'WCMA',
                'ISNE-SEMA' => 'SEMA',
            ];
        } else {
            $map = [
                '.Z.WCMASS' => 'WCMA',
                '.Z.SEMASS' => 'SEMASS',
                'SEMA'      => 'SEMASS',
            ];
        }

        Account::with('usageReadings', 'masterMonthlies')
               ->chunk(100, function($accounts) use ($acceptable_load_zones, $acceptable_load_names, $acceptable_zip_names, &$have_zone, &$missing_zone, $total_missing, &$count, &$possible_zones, &$possible_zips, $map) {
            //dd($accounts);
            foreach ($accounts as $account) {
                //dd($account);
                $load_zone = null;

                if ($account->usageReadings->count() > 0) {
                    
                    foreach ($account->usageReadings as $ur) {
                        //dd($ur);
                        //$lastur = 
                        if ($ur->all_fields) {
                            $all_fields = [];
                            foreach ($ur->all_fields as $field => $val) {
                                foreach ($map as $before => $after) {
                                    if ($val == $before) {
                                        $all_fields[$field] = str_replace($before, $after, $val);
                                    }
                                }
                            }
                            $ur->all_fields = $all_fields;
                            //dd($ur->all_fields);
                            foreach ($acceptable_load_zones as $lz) {
                                if (in_array($lz, $ur->all_fields)) {
                                    $load_zone = $lz;
                                    echo $account->id." Setting LZ from UR: ".$lz."\n";
                                }
                            }

                            if ($load_zone) {
                                break;
                            }

                            foreach ($acceptable_load_names as $column) {
                                if (isset($ur->all_fields[$column])) {
                                    $possible = $ur->all_fields[$column];
                                    if (isset($possible_zones[$possible])) {
                                        $possible_zones[$possible] = $possible_zones[$possible] + 1;
                                    } else {
                                        $possible_zones[$possible] = 1;
                                    }
                                }
                            }
                            
                        }
                    }
                    
                } 

                if ($load_zone) {
                    $have_zone++;
                    //dd($load_zone);
                    $account->load_zone = $load_zone;
                    $account->save();
                    $account->usageReadings()->update(['load_zone' => $load_zone]);
                    echo $account->id.": Updated ".$account->usageReadings()->count()." readings.\n";
                } else {
                //dd($possible_zips);

                    if ($account->masterMonthlies->count() > 0) {
                        foreach ($account->masterMonthlies as $mm) {
                            //dd($mm);
                            //$lastur = 
                            if ($mm->all_fields) {
                                //dd($mm->all_fields);
                                foreach ($acceptable_load_zones as $lz) {
                                    if (in_array($lz, $mm->all_fields)) {
                                        $load_zone = $lz;
                                        echo $account->id." Setting LZ from MM: ".$lz."\n";
                                    }
                                }
                                if ($load_zone) {
                                    break;
                                }

                                foreach ($acceptable_load_names as $column) {
                                    if (isset($mm->all_fields[$column])) {
                                        $possible = $mm->all_fields[$column];
                                        if (isset($possible_zones[$possible])) {
                                            $possible_zones[$possible] = $possible_zones[$possible] + 1;
                                        } else {
                                            $possible_zones[$possible] = 1;
                                        }
                                    }
                                }

                            }
                        }

                        if ($load_zone) {
                            $have_zone++;
                            //dd($load_zone);
                            $account->load_zone = $load_zone;
                            $account->save();
                            $account->usageReadings()->update(['load_zone' => $load_zone]);
                            echo $account->id.": Updated ".$account->usageReadings()->count()." readings.\n";
                        }
                    }
                }

                //
            }
            
            //echo "\t".($count+=100)."\n";
        });
        print_r($possible_zones);
        print_r($possible_zips);
        echo "Have: $have_zone, Missing: $missing_zone\n";
        //echo "\n Have: $have_zone, Missing: $missing_zone\n";

    }
}
