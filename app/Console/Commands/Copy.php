<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CommissionUsageReading;
use App\SalesUsageReading;
use App\UsageReading;
use DB;
use App\Traits\CrunchTrait;
use Carbon\Carbon;

class Copy extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:copy {--city=} {--type=} {--import=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $city = $this->startCrunch('copy', $this->option('city'));

        $type = $this->option('type');

        $runsales = true;
        $runcomms = true;
        if ($type) {
            if ($type != 'sales') {
                $runsales = false;
            }
            if ($type != 'commission') {
                $runcomms = false;
            }
        }
        if ($runsales) {
            $salesusage = SalesUsageReading::whereNull('copied_at');
            if ($this->option('import')) {
                $salesusage->where('usage_file_id', $this->option('import'));
            }
            $salesusage->orderBy('id', 'desc')->chunkById(100, function ($salesreadings) {

            
                echo "sales_usage_readings id ".$salesreadings->first()->id."\r";
                foreach ($salesreadings as $salesreading) {

                    $usage = UsageReading::where('account_id', $salesreading->account_id)
                                         ->where('usage_kwh', $salesreading->usage_kwh)
                                         ->where('usage_to_date', $salesreading->usage_to_date)
                                         ->first();
                    if (!$usage) {

                        $usagereading = $salesreading->replicate();
                        $usagereading->setTable('usage_readings');
                        //dd($commreading);
                        //DB::enableQueryLog(); // Enable query log
                        unset($usagereading->copied_at);
                        $usagereading->save();
                        $salesreading->copied_at = Carbon::now();
                        $salesreading->save();

                        //dd(DB::getQueryLog()); // Show results of log
                        
                        //dd($usagereading);
                    }
                }
            });
        }
        if ($runcomms) {
            $commusage = CommissionUsageReading::whereNull('copied_at');
            if ($this->option('import')) {
                $commusage->where('usage_file_id', $this->option('import'));
            }
            $commusage->orderBy('id', 'desc')->chunkById(100, function ($commreadings) {

            
                echo "comm_usage_readings id ".$commreadings->first()->id."\r";
                foreach ($commreadings as $commreading) {

                    $usage = UsageReading::where('account_id', $commreading->account_id)
                                         ->where('usage_kwh', $commreading->usage_kwh)
                                         ->where('usage_to_date', $commreading->usage_to_date)
                                         ->first();
                    if (!$usage) {

                        $usagereading = $commreading->replicate();
                        $usagereading->setTable('usage_readings');
                        //dd($commreading);
                        //DB::enableQueryLog(); // Enable query log

                        unset($usagereading->copied_at);
                        $usagereading->save();
                        $commreading->copied_at = Carbon::now();
                        $commreading->save();

                        //dd(DB::getQueryLog()); // Show results of log
                        
                        //dd($usagereading);
                    }
                }
            });
        }
        $this->endCrunch();
    }
}
