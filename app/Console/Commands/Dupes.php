<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Crunch;
use App\City;
use App\Traits\CrunchTrait;

class Dupes extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:dupes {--city=} {--table=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cities = City::orderBy('name')->get();
        $table = 'usage_readings';
        if ($this->option('table')) {
            if ($this->option('table') == 'sales') {
                $table = 'sales_usage_readings';
            }
            if ($this->option('table') == 'commission') {
                $table = 'commission_usage_readings';
            }
        }

        if ($this->option('city')) {
            $cities = City::where('name', $this->option('city'))->get();
        }
        echo "\n****************************** PREVIEW ******************************\n\n";
        
        foreach ($cities as $city) {

            $city_name = $this->startCrunch('dupes', $city->name);

            $dupes = DB::select('select usage_kwh, account_id, usage_to_date, COUNT(*) as count 
                                    from '.$table.' 
                                    group by usage_kwh, account_id, usage_to_date 
                                    having count > 1');
            
            if (count($dupes) > 0) {
                //dd($this->crunch);
                $this->crunch->addToLog('creating temp table');

                DB::statement('create temporary table tmpTable (id int, PRIMARY KEY (id))');

                $this->crunch->addToLog('inserting to temp table');
                DB::statement('insert  into tmpTable
                                        (id)
                                select  id
                                from    '.$table.' yt
                                where   exists
                                        (
                                        select  *
                                        from    '.$table.' yt2
                                        where   yt2.account_id = yt.account_id
                                                and yt2.usage_kwh = yt.usage_kwh
                                                and yt2.usage_to_date = yt.usage_to_date
                                                and yt2.id > yt.id
                                        )');

                $this->crunch->addToLog('DELETING from '.$table.'');
                $response = DB::statement('DELETE FROM '.$table.' USING '.$table.', tmpTable WHERE '.$table.'.id=tmpTable.id');

                $this->endCrunch(count($dupes));
            }
        }
    }
}
