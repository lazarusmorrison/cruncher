<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Crunch;
use App\Account;
use Carbon\Carbon;
use App\Traits\CrunchTrait;

class YearlyIcaps extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:yearly_icaps {--city=} {--rebuild} {--account=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds the yearly icap to the accounts table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $city = $this->startCrunch('yearly_icap', $this->option('city'));

        if ($this->option('rebuild')) {
            echo "Clearing out Yearly ICAPs\n";
            Account::whereNotNull('id')
                   ->update(['icap_2016' => null,
                             'icap_2017' => null,
                             'icap_2018' => null,
                             'icap_2019' => null,
                             'icap_2020' => null]);

            //dd("cleared yearly icaps");
        }

        if ($this->option('account')) {
            $account_id = $this->option('account');
            $accounts = Account::where('id', $account_id)->get();
        } else {
            $accounts = Account::whereNull('icap_2016')
                                ->whereNull('icap_2017')
                                ->whereNull('icap_2018')
                                ->whereNull('icap_2019')
                                ->whereNull('icap_2020')
                                ->whereNull('icap_2021')
                                ->get();
        }

        echo $accounts->count()." with no yearly icaps\n";

        foreach ($accounts as $account) {
            echo "Account ".$account->id." has ".$account->usageReadings()->count()." readings.\n";
            $urs = $account->usageReadings;

            $years = [];
            $years_updated = [];
            foreach ($urs as $ur) {
                $ur_month = $ur->corrected_ldc_read_month;
                //dd($ur);
                if (!$ur_month) {
                    if ($ur->usage_to_date) {
                        $ur_month = $ur->usage_to_date;
                    }
                    if (!$ur_month) {
                        //$ur_month = $ur->usageFile->month->subMonth();
                    }
                    if (!$ur_month) {
                        echo $account->id.": No Month found \n";
                        continue;
                    }
                }
                if ($ur_month > Carbon::parse('2017-01-01')) {
                    $ur_year = $ur_month->copy()->subMonths(5)->year;
                    $icap_str = str_pad($ur->corrected_icap, '0', 8, STR_PAD_RIGHT);
                    if ($icap_str != "") {
                        $years[$ur_year]['icaps'][$icap_str][] = $ur_month->format('Y-m-d');
                        //$years[$ur_year]['readings'][] = $ur;
                    }
                }
            }
            //dd($years);
            foreach ($years as $year => $data) {
                $field = 'icap_'.$year;
                
                if (count($data['icaps']) == 1) {
                    $icap = array_key_first($data['icaps']);
                    //dd($icap);
                    $account->$field = $icap;
                    $years_updated[] = $year;
                    // foreach ($data['readings'] as $reading) {
                    //     $reading->corrected_icap = $icap;
                    //     $reading->save();
                    // }
                } else {
                    $frequent_icap = 0;
                    $maxcount = 0;
                    foreach ($data['icaps'] as $tempicap => $tempmonths) {
                        if (count($tempmonths) > $maxcount) {
                            $frequent_icap = $tempicap;
                        }
                    }
                    $account->$field = $frequent_icap;
                    $years_updated[] = $year;
                    // foreach ($data['readings'] as $reading) {
                    //     $reading->corrected_icap = $icap;
                    //     $reading->save();
                    // }
                }
            }
            unset($account->usageReadings);
            //dd($years_updated);
            if (count($years_updated) > 0) {
                if ($account->icap_2017 && $account->icap_2018) {
                    if ($account->icap_2017 == $account->icap_2018) {
                        
                        if (count($years['2017']['icaps'][$account->icap_2017]) > count($years['2018']['icaps'][$account->icap_2018])) {
                            $account->icap_2018 = null;
                            echo $account->id." Error: 2017 and 2018 match, removed 2018\n";
                        } else if (count($years['2018']['icaps'][$account->icap_2018]) > count($years['2017']['icaps'][$account->icap_2017])) {
                            $account->icap_2017 = null;
                            echo $account->id." Error: 2017 and 2018 match, removed 2017\n";
                        } else {
                            echo $account->id." Error: 2017 and 2018 match, removed both\n";
                            $account->icap_2017 = null;
                            $account->icap_2018 = null;
                        }
                    }
                }
                if ($account->icap_2018 && $account->icap_2019) {
                    
                    if ($account->icap_2018 == $account->icap_2019) {
                        //dd($years, $account->icap_2018, $account->icap_2019);
                        if (count($years['2018']['icaps'][$account->icap_2018]) > count($years['2019']['icaps'][$account->icap_2019])) {
                            $account->icap_2019 = null;
                            echo $account->id." Error: 2018 and 2019 match, removed 2019\n";
                        } else if (count($years['2019']['icaps'][$account->icap_2019]) > count($years['2018']['icaps'][$account->icap_2018])) {
                            $account->icap_2018 = null;
                            echo $account->id." Error: 2018 and 2019 match, removed 2018\n";
                        } else {
                            echo $account->id." Error: 2018 and 2019 match, removed both\n";
                            $account->icap_2018 = null;
                            $account->icap_2019 = null;
                        }
                    }
                }
                if ($account->icap_2019 && $account->icap_2020) {
                    
                    if ($account->icap_2019 == $account->icap_2020) {
                        //dd($years, $account->icap_2019, $account->icap_2020);
                        if (count($years['2019']['icaps'][$account->icap_2019]) > count($years['2020']['icaps'][$account->icap_2020])) {
                            $account->icap_2020 = null;
                            echo $account->id." Error: 2019 and 2020 match, removed 2020\n";
                        } else if (count($years['2020']['icaps'][$account->icap_2020]) > count($years['2019']['icaps'][$account->icap_2019])) {
                            $account->icap_2019 = null;
                            echo $account->id." Error: 2019 and 2020 match, removed 2019\n";
                        } else {
                            echo $account->id." Error: 2019 and 2020 match, removed both\n";
                            $account->icap_2019 = null;
                            $account->icap_2020 = null;
                        }
                    }
                }
                if ($account->icap_2020) {
                    $account->icap = $account->icap_2020;
                } else if ($account->icap_2019) {
                    $account->icap = $account->icap_2019;
                } else if ($account->icap_2018) {
                    $account->icap = $account->icap_2018;
                }
                $account->save();
                echo $city." - ".$account->id.": Updated ".count($years_updated)." years: ".implode(', ', $years_updated)."\n";
            }
        } 
        //dd($accounts->count());

        $this->endCrunch();
    }
}
