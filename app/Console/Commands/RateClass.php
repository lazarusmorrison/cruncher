<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UsageReading;
use App\Account;
use App\MasterMonthlyAccount;
use App\Crunch;
use DB;
use App\Traits\CrunchTrait;

class RateClass extends Command
{
  use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:rate_class {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets all rate classes we can correctly, regardless of creed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $city = $this->startCrunch('rate_class', $this->option('city'));

        $acceptable_rate_classes = ['R1', 'R2', 'R3', 'R4', 'R5', 'R6',
                                    'G0', 'G1', 'G2', 'G3', 'G4', 'G5', 
                                    'S1', 'S2', 'S3', 'S4', 'S5',
                                    'T0', 'T1', 'T2',
                                    '23', '24'];

        // ==================================> CLEAR OUT DUDS

        UsageReading::whereNotIn('corrected_rate_class', $acceptable_rate_classes)
                    ->update(['corrected_rate_class' => null]);

        // ==================================> NOW WE CAN FIX

                    // ==================================> 2. CORRECTED MIGHT BE WRONG
      echo "CORRECTED MIGHT BE WRONG (BASE RATE DIFFERENT RECENTLY)\n";

      $accounts = UsageReading::groupBy('account_id')->pluck('account_id');
      echo "Got ".count($accounts)." accounts!\n";
      foreach ($accounts as $index => $account) {
        $brs = [];
        $crs = [];
        $urs = UsageReading::where('account_id', $account)
                           ->where('corrected_ldc_read_month', '>', '2021-01-01')
                           ->get();

        foreach ($urs as $ur) {
          if (in_array($ur->base_rate_class, $acceptable_rate_classes)) {
            if (isset($brs[$ur->base_rate_class])) {
              $brs[$ur->base_rate_class] += 1;
            } else {
              $brs[$ur->base_rate_class] = 1;
            }
          }
          if (in_array($ur->corrected_rate_class, $acceptable_rate_classes)) {
            if (isset($crs[$ur->corrected_rate_class])) {
              $crs[$ur->corrected_rate_class] += 1;
            } else {
              $crs[$ur->corrected_rate_class] = 1;
            }
          }
        }
        echo "\t $index \r";
        if ($brs != $crs) {
          $bwinner = '';
          $cwinner = '';
          $bmax = 0;
          $cmax = 0;
          if (count($brs) > 0) {
            $bmax = max($brs);
            $bmaxkeys = array_keys($brs, $bmax);
            if (count($bmaxkeys) > 1) {
              echo "NO WINNER";
              $bwinner = $bmaxkeys[0];
            } else {
              $bwinner = $bmaxkeys[0];
            }
          }
          if (count($crs) > 0) {
            $cmax = max($crs);
            $cmaxkeys = array_keys($crs, $cmax);
            if (count($cmaxkeys) > 1) {
              echo "NO WINNER";
              $cwinner = $cmaxkeys[0];
            } else {
              $cwinner = $cmaxkeys[0];
            }
          }

          if ($bwinner != $cwinner) {
            echo "\n$account\n";
            echo "B: $bwinner $bmax\n";
            echo "C: $cwinner $cmax\n";
          }
        }
      }


        $null_rates = UsageReading::whereNull('corrected_rate_class');
        echo "Total Bad: ".$null_rates->count()."\n";

        

        // ==> SET TO BASE    
        echo "Setting to base\n";   
        while (($count = UsageReading::whereIn('base_rate_class', $acceptable_rate_classes)
                    ->whereNull('corrected_rate_class')
                    ->count()) > 0) { 
          echo "$count null corrected left\n";                  
          UsageReading::whereIn('base_rate_class', $acceptable_rate_classes)
                      ->whereNull('corrected_rate_class')
                      ->take(10000)
                      ->update(['corrected_rate_class' => DB::raw('base_rate_class')]);
        }

        echo "Setting to account\n"; 
        // ==================================> 1. PULL FROM ACCOUNT
        UsageReading::whereNull('corrected_rate_class')
                           ->with('account')->chunk(1000, function($urs) {
          foreach ($urs as $ur) {
            if ($ur->account) {
              if ($ur->account->rate_class) {
                $ur->corrected_rate_class = $ur->account->rate_class;
                $ur->save();
              }
            }
          }
          echo "Updated another 1000 from account \n";    
        });



        // ==================================> 2. CORRECTED NULL
        echo "About to correct ".UsageReading::whereNull('corrected_rate_class')->count()."\n";
        $null_corrected = UsageReading::whereNull('corrected_rate_class')
                            ->whereIn('base_rate_class', $acceptable_rate_classes)
                            ->update(['corrected_rate_class' => DB::raw('base_rate_class')]);



        // ==================================> 2. RATE CLASS EXISTS BUT IS WRONG


        $issues_query = UsageReading::whereNotIn('corrected_rate_class', $acceptable_rate_classes)
                              ->where('corrected_rate_class', '!=', "")
                              // ->where('corrected_ldc_read_month', '>', '2018-01-01')
                              ->whereNotNull('base_rate_class');
        //dd($issues->pluck('corrected_rate_class'));
        echo "Has Issues: ".$issues_query->count()."\n";
        
        echo "About to check for problems\n";
        $problems = $issues_query->groupBy('rate_class')->pluck('rate_class');
        echo "Got problems!\n";

        //dd($problems);

        foreach ($problems as $problem) {

          $problem = trim($problem);

          $corrected_count = 0;
          $missed = [];
          echo "$problem Count: ".UsageReading::where('corrected_rate_class', $problem)->count();
            

          $corrected_rate_class = "";
          
          $corrected_rate_class = $this->badRateCorrector($problem);

          if (in_array(str_replace(' ', '', $problem), $acceptable_rate_classes)) {

            $corrected_rate_class = str_replace(' ', '', $problem);
          }
          if (in_array(trim($problem), $acceptable_rate_classes)) {

            $corrected_rate_class = $problem;
          }
          
          if ($corrected_rate_class) {
             $corrected_count += UsageReading::where('rate_class', $problem)->count();
              //dd($corrected_rate_class);
              UsageReading::where('rate_class', $problem)
                          ->update(['corrected_rate_class' => $corrected_rate_class]);
          } else {
            $missed[$problem] = 1;
          }
        
        if ($corrected_count) {
          $this->crunch->addToLog("Corrected $corrected_count of $problem");
        }
      
        echo "$problem: Corrected $corrected_count\n";
      }

      // ==================================> 1.5 NULL CORRECTED BUT HAS BASE
      $urs = UsageReading::whereNull('corrected_rate_class')
                         ->whereNotNull('base_rate_class')
                         ->get();

      foreach ($urs as $ur) {
        $corrected_rate_class = $this->badRateCorrector($ur->base_rate_class);
        if ($corrected_rate_class) {
          $ur->corrected_rate_class = $corrected_rate_class;
          $ur->save();
        }
      }




      // ==================================> 2. CORRECTED RATE CLASS NULL, FINDS ANOTHER READING NOT NULL

      echo "CORRECTED RATE CLASS NULL, FINDS ANOTHER READING NOT NULL";
      
      //dd($accounts->count());
      
        
      foreach ($accounts as $account) {

        $urs = UsageReading::where('account_id', $account)
                           ->where('corrected_ldc_read_month', '>', '2021-01-01')
                           ->get();

        if ($account == '30452810010') {
          //dd($urs);
        }

        $missing = 0;
        $rate_classes = [];
        foreach ($urs as $ur) {
          if (!$ur->corrected_rate_class) {
            $missing++;
          }
          if (!in_array($ur->corrected_rate_class, $acceptable_rate_classes)) {
            if ($ur->corrected_rate_class) {
              echo $ur->corrected_rate_class."\n";
            }
            continue;
          }
          if (isset($rate_classes[$ur->corrected_rate_class])) {
            $rate_classes[$ur->corrected_rate_class] += 1;
          } else {
            $rate_classes[$ur->corrected_rate_class] = 1;
          }
          
        }

        $mms = MasterMonthlyAccount::where('account_id', $account)
                                   ->where('month', '>', '2021-01-01')
                                   ->get();

        //dd($mms);

        if ($account == '30452810010') {
          //dd($mms);
        }
        
        foreach ($mms as $mm) {

          if (!in_array($mm->base_rate_class, $acceptable_rate_classes)) {
            $base_rate_class = $this->badRateCorrector($mm->base_rate_class);
            if ($base_rate_class) {
              $mm->base_rate_class = $base_rate_class;
              $mm->save();
            }
          }
          if ($mm->base_rate_class && in_array($mm->base_rate_class, $acceptable_rate_classes)) {
            if (isset($rate_classes[$mm->base_rate_class])) {
              $rate_classes[$mm->base_rate_class] += 1;
            } else {
              $rate_classes[$mm->base_rate_class] = 1;
            }
          }
          
        }
        
        //dd($rate_classes);

        if (count($rate_classes) == 1) {
          $rc = array_key_first($rate_classes);
          if ($missing > 0) {
            // Dont keep re-saving the ones that are already filled.
            $this->crunch->addToLog("Updating $account to ".$rc);
            UsageReading::where('account_id', $account)
                            ->update(['corrected_rate_class' => $rc]);
          }
          $account_obj = Account::find($account);
          $account_obj->rate_class = $rc;
          $account_obj->save();
        } else if (count($rate_classes) > 1) {
          arsort($rate_classes);
          print_r($rate_classes);
          //dd($rate_classes);
          $rc = array_key_first($rate_classes);
          $this->crunch->addToLog("Updating $account to ".$rc);
          UsageReading::where('account_id', $account)
                          ->update(['corrected_rate_class' => $rc]);
          $account_obj = Account::find($account);
          $account_obj->rate_class = $rc;
          $account_obj->save();
        } 
        //dd($rate_classes);
      }

      echo "DUD COUNT";
      $dudcount = UsageReading::whereNotIn('corrected_rate_class', $acceptable_rate_classes)->count();
      if ($dudcount > 0) {
        $this->crunch->addToLog("Clearing all $dudcount dud values back to null");
        UsageReading::whereNotIn('corrected_rate_class', $acceptable_rate_classes)
                    ->update(['corrected_rate_class' => null]);
      }
      echo "BAD RATES";



      // $bad_rates = Account::whereNull('base_rate_class')->with('usageReadings')->get();
      // foreach ($bad_rates as $br) {
      //   foreach ($br->usageReadings as $ur) {
      //     if (in_array($ur->corrected_rate_class, $acceptable_rate_classes)) {
      //       $br->base_rate_class = $ur->corrected_rate_class;
      //       $br->rate_class = $ur->corrected_rate_class;
      //       //dd($br);
      //       echo "Added ".$ur->corrected_rate_class." to ".$br->id."\n";
      //       $br->save();
      //     }
      //   }
      // }

      $this->endCrunch();
    }
    public function badRateCorrector($problem)
    {
      $corrected_rate_class = '';
      switch (strtoupper($problem)) {
        case 'R-1':
            $corrected_rate_class = 'R1';
            break;
        case 'R-2':
            $corrected_rate_class = 'R2';
            break;
        case 'R-3':
            $corrected_rate_class = 'R3';
            break;
        case 'R-4':
            $corrected_rate_class = 'R4';
            break;
        case 'S-1':
            $corrected_rate_class = 'S1';
            break;
        case 'S-2':
            $corrected_rate_class = 'S2';
            break;
        case 'S-3':
            $corrected_rate_class = 'S3';
            break;
        case 'T-1':
            $corrected_rate_class = 'T1';
            break; 
        case 'T-2':
            $corrected_rate_class = 'T2';
            break;
        case 'G-1':
            $corrected_rate_class = 'G1';
            break; 
        case 'G-2':
            $corrected_rate_class = 'G2';
            break; 
        case 'G-3':
            $corrected_rate_class = 'G3';
            break; 
        case 'RES':
            $corrected_rate_class = 'R1';
            break;
        case 'R':
            $corrected_rate_class = 'R1';
            break;
        case 'RE':
            $corrected_rate_class = 'R1';
            break; 
        case 'COM':
            $corrected_rate_class = 'G1';
            break; 
        case 'C':
            $corrected_rate_class = 'G1';
            break; 
        case 'RESIDENTIAL REGULAR R-1':
            $corrected_rate_class = 'R1';
            break;
    }
    return $corrected_rate_class;
    }
}
