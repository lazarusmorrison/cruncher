<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\City;
use DB;
use App\DataImport;

class ChooseSheets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:sheets {--city=} {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Goes through every import with no sheet chosen, lets you pick';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cities = City::orderBy('name')->get();
        if ($this->option('city')) {
          $cities = City::where('name', 'LIKE', $this->option('city'))->get();
        }
        

        echo "\n****************************** ASSIGN SHEETS ******************************\n\n";
        
        foreach ($cities as $city) {
            echo "===================================> ".strtoupper($city->name)."\n";

            $db = 'bsc_'.strtolower($city->name);
            DB::purge('mysql');
            config(['database.connections.mysql.database' => $db]);
            
            $missing_sheet_imports = DataImport::where('city_id', $city->id)
                                      ->where('import_done', false)
                                      ->whereNull('which_sheet')
                                      ->get();
            if ($this->option('id')) {
              $missing_sheet_imports = DataImport::where('id', $this->option('id'))->get();
            }

            $this->info(strtoupper($city->name)." - ".$missing_sheet_imports->count()." Imports Missing Sheets");

            foreach ($missing_sheet_imports as $key => $msi) {

              //$import->getSheetNames();

                $this->info(($key+1).". ".basename($msi->name));
              $previous_sheets = [];
              $matching_imports = DataImport::where('city_id', $city->id)
                                        ->where('import_done', true)
                                        ->where('type', $msi->type)
                                        ->whereNotNull('which_sheet')
                                        ->get();
              foreach ($matching_imports as $mi) {
                $sheet_arr = $mi->sheets;
                //dd($sheet_arr);
                $sheetname = $sheet_arr[$mi->which_sheet - 1];
                //dd($sheetname);
                if (isset($previous_sheets[$sheetname])) {
                  $previous_sheets[$sheetname] += 1;
                } else {
                  $previous_sheets[$sheetname] = 1;
                }
              }
              arsort($previous_sheets);
              $which_sheet = null;
              $which_name = null;
              foreach ($previous_sheets as $ps => $pscount) {
                if (!$msi->sheets) {
                  continue;
                }
                if (is_string($msi->sheets)) {
                  //dd(json_decode($msi->sheets));
                  $msi->sheets = json_decode($msi->sheets);
                  $msi->save();
                }
                try {
                  if (in_array($ps, $msi->sheets)) {
                    foreach ($msi->sheets as $mkey => $msheet) {
                      if ($ps == $msheet) {
                        $which_sheet = $mkey + 1;
                        $which_name = $ps;
                      }
                    }
                  }
                } catch (\Exception $e) {
                  dd($msi, $e->getMessage(), "Laz");
                }
              }
              if ($which_sheet) {
                //dd($msi->sheets, $which_sheet);
                $msi->which_sheet = $which_sheet;
                echo "Setting Sheet to $which_name\n";
                //dd($msi);
                $msi->save();
                continue;
              }

              if (!$msi->sheets) {
                $this->error('MISSING SHEETS!!!!! BAD IMPORT!!!! trying to find...');
                $msi->file_type = strtolower($msi->file_type);
                $msi->save();
                try {
                  $msi->getSheetNames();
                } catch (\Exception $e) {
                  $this->error($e->getMessage());
                  continue;
                }
              }

              if (!$msi->sheets) {
                $this->error('STILL MISSING');
                continue;
              }

              if (!is_array($msi->sheets)) {
                $msi->sheets = json_decode($msi->sheets);
              }

              $sheet_name = $this->choice(
                    'Which sheet should be processed?',
                    array_merge(['HIT RETURN - Skip'], $msi->sheets),
                    0
                );
              $sheet_index = array_search($sheet_name, $msi->sheets) + 1;
              if ($sheet_index) {
                $msi->which_sheet = $sheet_index;
                $msi->save();
              }
              
            }

        }
    }
}
