<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\City;
use App\DataImport;
use Carbon\Carbon;
use App\Traits\CrunchTrait;

class Upload extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:upload {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks Sync folder to see if any files not uploaded yet.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$this->startCrunch('import', $city_str);

        $processing_folder = storage_path(config('app.processing_folder'));

        $cities = City::orderBy('name')->get();
        if ($this->option('city')) {
            $cities = City::where('name', $this->option('city'))->get();
        }

        foreach ($cities as $city) {
            $this->startCrunch('upload', $city->name);

            if (!$city) {
                dd("Gotta fix city dummy");
            }

            
            //dd($processing_folder);
            $file_city = str_replace('UGH', '', strtoupper($city->name));
            $all_files = glob($processing_folder.'/'.$file_city.'*');
            foreach ($all_files as $file_path) {
                //echo $file_path."\n";
                $file_name = basename($file_path);
                //dd($file_name);
                $name_arr = explode('_', $file_name);
                //dd($name_arr);
                if (count($name_arr) < 3) {
                    echo "Error: file name no good: ".$file_name."\n";
                    continue;
                }
                $city_str = $name_arr[0];
                if ($city_str == 'FOXBOROUGH') {
                    $city_str = 'FOXBORO';
                }
                if ($city_str == 'WESTBOROUGH') {
                    $city_str = 'WESTBORO';
                }
                $month_str = $name_arr[1].'-01';
                $type_str = $name_arr[2];
                $fulltype = "";
                if ($type_str == 'COM') {
                    $fulltype = 'commission';
                }
                if ($type_str == 'USE') {
                    $fulltype = 'sales';
                }
                if ($type_str == 'ACC') {
                    $fulltype = 'master';
                }

                
                
                $data_import = DataImport::where('original_name', $file_name)->first();


                if ($data_import) {
                    if (!$data_import->month) {
                        $data_import->month = $month_str;
                        $data_import->save();
                    }
                }
                
                if (!$data_import) {
                    echo "GONNA ADD DATA IMPORT ".$file_name."\n";

                    $import = new DataImport;
                    $import->city_id                = $city->id;
                    $import->type                   = $fulltype;
                    $import->month                  = $month_str;
                    $import->team_id                = 1;
                    $import->file_path              = $file_path;
                    $import->file_type              = strtolower(pathinfo($file_path, PATHINFO_EXTENSION));
                    $import->name                   = $file_name;
                    $import->original_name          = $file_name;

                    //dd($import);

                    $import->save();

                    //dd($import);

                    try {
                        if(substr($import->file_type,0,3) == 'xls') {
                            $import->getSheetNames();
                        }
                    } catch (\Exception $e) {
                        echo "ERROR: ".$e->getMessage()."\n";
                    }
                    
                }
            }
            $this->endCrunch(); 
        }
    }
}
