<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Rap2hpoutre\FastExcel\FastExcel;
use App\Traits\FileTrait;

// use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;



use App\City;
use App\DataImport;
use DB;

// https://github.com/rap2hpoutre/fast-excel


class CrunchPreviews extends Command
{
    use FileTrait;

    protected $signature = 'crunch:previews {--city=} {--id=} {--refresh}';
    protected $description = '';

    // public $directories = [];
    // public $files = [];
    public $preview_num = 100;
    public $limit = 1000;

    public function __construct()
    {
        parent::__construct();
    }

    ////////////////////////////////////////////////////////////////////////////////////

    // public function recursivelyGetDirectories($dir_to_look_into)
    // {
    //     $this->directories[] = $dir_to_look_into;

    //     $subs = array_filter(glob($dir_to_look_into.'*'), 'is_dir');

    //     foreach($subs as $dir) {
    //         $this->recursivelyGetDirectories($dir.'/');
    //     }
    // }

    // public function getFilesFromAllDirectories($ext)
    // {
    //     foreach($this->directories as $dir) {
    //         $this->files = array_merge($this->files, glob($dir.'*'.$ext));
    //     }
    // }

    ////////////////////////////////////////////////////////////////////////////////////

    public function generatePreview($file, $name = null)
    {
        $full_path = $this->getFullFilePath($file->file_path);

        if (!$name) {
            $name = str_replace($this->main_dir.'/', '', $file->file_path);
            $save_name = $name.'.json';
        } else {
            $save_name = $name.'.json';
        }


        if ($file->file_type != 'xlsx') {
            return null;
        }

        $sheet_names = $this->getSheetsForFile($full_path); // XLSX

        $worksheet_data = [];

        //print_r($sheet_names);

        foreach($sheet_names as $n => $worksheet_name) {

            try {
                $sheet_data =  $this->getRowsForSheet($full_path, $n + 1);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }

            //print_r($sheet_data);

            $worksheet_data[$n.'_'.$worksheet_name] = array_merge(
                [
                    'num'  => $n + 1,
                    'name' => $worksheet_name,
                ], 
               $sheet_data
            );

            $this->info('Done with worksheet '.($n + 1)." of ".count($sheet_names));
        }

        $json = [
                    'id'            => $file->id,
                    'name'          => $name,
                    'file_name'     => basename($full_path),
                    'save_name'     => $save_name,
                    'updated_at'    => now(),
                    'created_at'    => $file->created_at,
                    'size'          => filesize($full_path),
                    'which_sheet'   => $file->which_sheet,
                    'worksheets'    => $worksheet_data
                ];

        return json_encode($json);
    }

    public function savePreview($json)
    {
        $array = json_decode($json, true);
        $file_name = $array['save_name'];

        $store = storage_path().'/app/previews/';
        $dir = str_replace(basename($store.$file_name), '', $store.$file_name);
        $dir = str_replace(storage_path().'/app/', '', $dir);
        $this->createDirectoryIfNotExists($dir);

        file_put_contents($store.$file_name, $json);
    }

    ////////////////////////////////////////////////////////////////////////////////////

    public function getSheetsForFile($file)
    {
        //https://opensource.box.com/spout/docs/

        $sheet_names = [];

        // $reader = ReaderEntityFactory::createReaderFromFile($file);
        // $reader->open($file);
        $reader = ReaderFactory::create(Type::XLSX);
        $reader->open($file);

        foreach($reader->getSheetIterator() as $sheet) {
            $sheet_names[] = $sheet->getName();
        }

        return $sheet_names;
    }

    public function getRowsForSheet($file, $worksheet_num)
    {
        $r = 0;
        $first = [];
        $random = [];

        try {

            $start = microtime(true);

            $result = (new FastExcel)
                        ->sheet($worksheet_num)
                        ->import($file, function ($row) use (&$counts, &$r, &$first, &$random, $start) {

                                $r++;

                                if ($r <= $this->preview_num) {
                                    $first[] = $row;
                                }

                                if (rand(0,10) == 0) {
                                    $random[] = $row;
                                }

                                //$duration = microtime(true) - $start;
                                //echo "\t Worksheet ".$worksheet_num." Line ".$r." ".$duration."\r";

                                if (
                                    //$duration > 600 ||
                                    $r > $this->preview_num || 
                                    $r >= $this->limit || 
                                    count($random) >= $this->preview_num) {

                                    $this->thisFunctionIsMeantToStopTheLoop();
                                    return;
                                }
                                

                            }
                        );

        } catch (\Exception $e) {

            // echo $e->getMessage()."\n";         

        }

        return ['first' => $first, 'random' => $random];
    }

    ////////////////////////////////////////////////////////////////////////////////////

    // public function removeOrphanedPreviews()
    // {
    //     $previews = glob($this->previews_dir.'*');
    //     $needed_files = [];
    //     foreach($this->files as $file) {
    //         $needed_files[] = $this->previews_dir
    //                           .str_replace($this->main_dir, '', $file)
    //                           .'.json';
    //     }

    //     $this->directories = [];
    //     $this->files = [];
    //     $this->recursivelyGetDirectories($this->previews_dir);
    //     $this->getFilesFromAllDirectories('.json');

    //     $ophaned = array_diff($this->files, $needed_files);

    //     foreach($ophaned as $orphan) {

    //         if (substr($orphan, -5) != '.json') { // Double check right kind of file
    //             continue;
    //         }
            
    //         unlink($orphan);
    //     }
    // }

    ////////////////////////////////////////////////////////////////////////////////////

    public function onePreviewFromUsageID()
    {
        // Use Usage ID and City to Get Full Path Then:
        $json = $this->generatePreview($file);
        $this->savePreview($json);
    }

    // public function onePreviewFromFilePath()
    // {
    //     $file = $this->option('full_file_path');
    //     $json = $this->generatePreview($file);
    //     $this->savePreview($json);
    // }

    // public function allPreviewsInMainDirectory()
    // {
    //     $this->directories = [];
    //     $this->files = [];
    //     $this->recursivelyGetDirectories($this->main_dir);
    //     $this->getFilesFromAllDirectories('.xlsx');

    //     foreach($this->files as $file) {
    //         $json = $this->generatePreview($file);
    //         $this->savePreview($json);
    //     }
    // }

    public function goThroughAllCities()
    {
        $cities = City::orderBy('name')->get();

        if ($this->option('city')) {
            $cities = City::where('name', $this->option('city'))->orderBy('name')->get();
        }

        if (env('LOCAL_MACHINE') == 'Slothe') {
            $cities = City::query();
            foreach(['acton', 'bellingham'] as $sloth) {
                $cities = $cities->orWhere('name', $sloth);
            }
            $cities = $cities->orderBy('name')->get();
        }

        foreach ($cities as $city) {

            $city_name = $city->name;

            DB::purge('mysql');
            config(['database.connections.mysql.database' => 'bsc_'.$city_name]);

            foreach (DataImport::all() as $file) {


                $this->getPreviewForDataOneImport($city->name, $file);                
            
            }

        }
           
    }

    public function getPreviewForDataOneImport($city_name, $file)
    {

        $ignores = [
            'nantucket' => [62],
        ];

        if (isset($ignores[$city_name])) { 
            if (in_array($file->id, $ignores[$city_name])) {
                return;
            }
        }
        $full_path = $this->getFullFilePath($file->file_path);

        echo 'Beginning '
                    .$city_name
                    .' '
                    .str_pad($file->id, 4, '0', STR_PAD_LEFT)
                    .' '
                    .basename($file->file_path)."\n";

        if (!file_exists($full_path)) {

            $this->error('Cannot find '.$full_path);

        } else {

            $name = strtolower($city_name).'_'.str_pad($file->id, 4, '0', STR_PAD_LEFT);

            if (file_exists(storage_path().'/app/previews/'.$name.'.json')) {
                if (!$this->option('refresh')) {
                    return;
                }
            }

            $json = $this->generatePreview($file, $name);

            if ($json) {

                $this->savePreview($json);
                $array = json_decode($json, true);

                try {
                    $size = number_format($array['size']/1000000, 2);
                    $basename = $array['file_name'];
                } catch (\Exception $e) {
                    $size = '?';
                    $basename = '?';
                }

                $this->info("Saved ".$name."\t".$size.' MB'."\t".$basename);
            }

        }
    }

    public function getFullFilePath($file)
    {
        if (env('LOCAL_MACHINE') == 'Slothe') {
            return storage_path().'/app/'.$file;
        }
        return $file;
    }

    public function handle()
    {
        // $this->main_dir     = storage_path().'/app/data_files'; // ?

        $this->previews_dir = storage_path().'/app/previews';
        $this->createDirectoryIfNotExists('previews');

        if ($this->option('city') && $this->option('id')) {

            $city_name = $this->option('city');
            $id = $this->option('id');

            DB::purge('mysql');
            config(['database.connections.mysql.database' => 'bsc_'.$city_name]);
            $file = DataImport::find($id);

            if ($file) {
                $this->error('Getting city '. $city_name .' ID '.$id);
                $this->getPreviewForDataOneImport($city_name, $file);
            }

        } else {

            $this->goThroughAllCities();

        }

        //$this->removeOrphanedPreviews();

    }
}
