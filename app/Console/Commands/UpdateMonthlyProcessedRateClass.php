<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\MonthlyReading;
use App\Traits\CrunchTrait;

class UpdateMonthlyProcessedRateClass extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:update_monthly_processed_rate_class {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $city = $this->startCrunch('update_monthly_processed_rate_class', $this->option('city'));
        $missing_rate = MonthlyReading::whereNull('base_rate_class');

        foreach ($missing_rate->with('usageReading')->get() as $mr) {
            if ($mr->usageReading) {
                if ($mr->usageReading->corrected_rate_class) {
                    $mr->base_rate_class = $mr->usageReading->corrected_rate_class;
                    echo "Saving ".$mr->account_id." ".$mr->month." updated to ".$mr->base_rate_class."\n";
                    $mr->save();
                }
            }
        }
        //dd($missing_rate->count());
    }
}
