<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\City;
use Carbon\Carbon;
use App\Traits\HandyTrait;

class RenameAndMove extends Command
{
    use HandyTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:rename {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Renames and moves files to get correct naming convention';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        // ==================> TESTING PREG_MATCHES
        // $test_string = "330.Peregrine Energy Group_011221.xlsx";
        // $cityname = "southborough";
        // if (preg_match("/^(".$cityname.")\s(January|February|March|April|May|June|July|August|September|October|November|December)\s(\d{4})\.xlsx/i", $test_string, $matches)) {
        //     dd($matches);
        // }
        // dd();
        
        
        $client_folder = storage_path(config('app.client_folder'));
        $process_folder = storage_path(config('app.processing_folder'));

        if (!$process_folder || !$client_folder) {
            dd($client_folder, $process_folder);
        }
        //dd($process_folder);
        $cities = City::orderBy('name')->get()->keyBy('name');
        if ($this->option('city')) {
            $cities = City::where('name', $this->option('city'))->get()->keyBy('name');
        }
        //dd($cities);
        foreach ($cities as $city) {

            
            $folder_city = ucwords($city->name);
            if ($city->name != 'southborough') {
                $folder_city = str_replace('ugh', '', $folder_city);
            }
            $city_folder = $client_folder.'/'.$folder_city;
            $all_files = glob($city_folder.'/*');
            //dd($all_files);

            foreach ($all_files as $filepath) {

                if (strpos($filepath, '_TO_DELETE_') > 0) {
                    continue;
                }

                $supplier = "";
                $filename = strtoupper(basename($filepath));

                if (file_exists($city_folder.'/_TO_DELETE_'.$filename)) {
                    // Already dealt with this file, delete it
                    unlink($filepath);
                    echo "\tRemoved already processed: $filename\n";
                    continue;
                }

                if (strpos($filename, 'SEPT ') > 0) {
                    $filename = str_replace('SEPT ', 'SEP ', $filename);
                }

                $file_arr = explode('_', $filename);

                $current_filename_city = null;
                $current_filename_date = null;
                $current_filename_type = null;
                $current_filename_supp = null;

                if (isset($file_arr[0])) {
                    $current_filename_city = $file_arr[0];
                }
                if (isset($file_arr[1])) {
                    $current_filename_date = $file_arr[1];
                }
                if (isset($file_arr[2])) {
                    $current_filename_type = $file_arr[2];
                }
                if (isset($file_arr[3])) {
                    $current_filename_supp = $file_arr[3];
                }

                
                //dd($current_filename_city,$current_filename_date, $current_filename_type, $current_filename_supp);

                $newstring = "";

                // CITY
                $count = 0;
                foreach ($cities as $cityname => $city) {
                    $cityname = strtoupper($cityname);
                    
                    if (str_contains($filename, $cityname)) {
                        $newstring = $cityname."_";
                        $count++;
                        $supplier = $city->supplier;
                        //dd($city);
                        //echo $filename. " $cityname\n";
                    }
                }
                if (!$count) {
                    if (str_contains($filename, 'SOUTHBORO')) {
                        $newstring = "SOUTHBOROUGH_";
                        $supplier = 'FP';
                    } else {
                        echo "CITY MISSING BUT USING ".$city->name.": $filename\n";
                        $newstring = strtoupper($city->name)."_";
                        $count++;
                        $supplier = $city->supplier;
                        //continue;
                    }
                }
                if ($count > 1) {
                    echo "CITY MULTIPLE: $filename\n";
                    continue;
                }

                // MONTH

                $file_month = $this->extractDate($filename);



                $newstring .= $file_month."_";
                //dd($filename, $suggested_month, $newstring);

                // TYPE
                $type = $this->extractType($city, $filename);

                //dd($filename, $type);
                $newstring .= $type."_";

                // SUPPLIER
                $newstring .= $supplier."_";

                
                //dd($filename, $newstring);
                // Skip file if already there
                if (str_contains($filename, $newstring)) {
                    echo "Already has correct naming convention\n";
                    //continue;
                }



                if (in_array($current_filename_supp, ['DE','NE','FP','CNE','PP', 'DYN'])) {
                    $newfilename = str_replace($current_filename_city."_".$current_filename_date."_".$current_filename_type."_".$current_filename_supp."_", '', $filename);
                } else {
                    $newfilename = $filename;
                }

                

                // rename file
                
                //dd($newstring);
                
                if ($type && $supplier && $file_month) {
                    // File has everything, move it on over
                    // MOVE TO MIXED
                    echo "copying ".$filename." to:\n ".$process_folder.'/'.$newstring.$newfilename."\n";
                    //dd();
                    

                    if (config('app.env') == 'production') {
                        copy($filepath, $process_folder.'/'.$newstring.$newfilename);
                        rename($filepath, $city_folder.'/_TO_DELETE_'.$filename);
                    } else {
                        rename($filepath, $process_folder.'/'.$newstring.$newfilename);
                    }
                } else {
                    echo "INCOMPLETE: ".$filename.$type.$supplier.$file_month."\n";
                }

            }
            $all_files = glob($city_folder.'/*');
            echo "\n";
            foreach ($all_files as $filepath) {
                echo basename($filepath)."\n";
            }
        }
    }
}
