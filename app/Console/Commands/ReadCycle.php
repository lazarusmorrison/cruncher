<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UsageReading;
use DB;
use App\Crunch;
use Carbon\Carbon;
use App\Traits\CrunchTrait;

class ReadCycle extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:read_cycle {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fills the corrected_read_cycle when its stupid NULL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $city = $this->startCrunch('read_cycle', $this->option('city'));

        $zero_to_null = UsageReading::where('corrected_read_cycle', '0')
                                    ->update(['corrected_read_cycle' => null]);

        $total_missing = UsageReading::whereNull('corrected_read_cycle')
                                     ->count();
        echo "Total Missing: ".$total_missing."\n";
        //dd();

        // =======================================> 1. SET TO ORIGINAL
        $set_to_original = null;
        while (($count = UsageReading::whereNull('corrected_read_cycle')
                           ->whereNotNull('read_cycle')
                           ->where('read_cycle', '>', 0)
                           ->count()) > 0) {
            echo "$count left, trying another 10000\n";
            $set_to_original = UsageReading::whereNull('corrected_read_cycle')
                                           ->whereNotNull('read_cycle')
                                           ->where('read_cycle', '>', 0)
                                           ->take(10000)
                                           ->update(['corrected_read_cycle' => DB::raw('read_cycle')]);
            
        }
        if ($set_to_original) {
            $this->crunch->addToLog('Set '.$set_to_original." to original.");
        }
        
        

        // ==================================> 2. PULL FROM ACCOUNT
        UsageReading::whereNull('corrected_read_cycle')
                           ->with('account')->chunk(1000, function($urs) {
        
          foreach ($urs as $ur) {
            if ($ur->account) {
              if ($ur->account->read_cycle) {
                $ur->corrected_read_cycle = $ur->account->read_cycle;
                $ur->save();
              }
            }
          }
          echo "Another 1000 updated from account \n"; 
        });

        
        

        // =======================================> FILL BLANKS

        $account_ids = UsageReading::whereNull('corrected_read_cycle')
                                   ->pluck('account_id')
                                   ->unique();
        $corrected = 0;
        foreach ($account_ids as $account_id) {
            $changed = $this->fillBlanksOneAccount($account_id);
            if ($changed) {
                $corrected++;
            }
        }
        if ($corrected) {
            $this->crunch->addTolog('Corrected '.$corrected.' of '.$account_ids->count());
        }

        if ($this->crunch->log) {
            $this->crunch->complete();
        }

    }
    public function fillBlanksOneAccount($account_id)
    {
        //$sales = SalesUsageReading::where('account_id', $account_id)->get();
        //$comms = CommissionUsageReading::where('account_id', $account_id)->get();
        $usage = UsageReading::where('account_id', $account_id)->get();
        //dd($sales->count(), $comms->count(), $usage->count());
        //$master = MasterMonthlyAccount::where('account_id', $account_id)->get();
        //dd($sales->count(), $comms->count(), $usage->count());
        //$usage = $sales->merge($comms)->merge($usage);
        //dd($all);

        $fillfields = [
            // 'base_rate_class', 
            'read_cycle',
            // 'icap'
        ];

        //dd("Laz");

        $to_correct = [];
        $correct_values = [];
        foreach ($usage as $ur) {
            foreach ($fillfields as $field) {

                $corrected_field = "corrected_".$field;
                $corrected_field = str_replace('base_', '', $corrected_field);
                if (is_null($ur->$corrected_field)) {
                    $to_correct[$field][] = $ur;
                } else if ($ur->$corrected_field) {
                    if (isset($correct_values[$field][strval($ur->$corrected_field)])) {
                        $correct_values[$field][strval($ur->$corrected_field)] += 1;
                    } else {
                        $correct_values[$field][strval($ur->$corrected_field)] = 1;
                    }
                } else {
                    if (isset($correct_values[$field][strval($ur->$field)])) {
                        $correct_values[$field][strval($ur->$field)] += 1;
                    } else {
                        $correct_values[$field][strval($ur->$field)] = 1;
                    }
                }
            }
        }

        $numcorrected = 0;
        foreach ($to_correct as $field => $urs) {
            $corrected_field = "corrected_".$field;
            $corrected_field = str_replace('base_', '', $corrected_field);
            if (isset($correct_values[$field])) {
                $options = $correct_values[$field];
                //dd($options);
                if (count($options) > 1) {
                    echo $account_id." conflict: ".$field." has ".count($options)." options:\n";
                    echo "          ";
                    foreach ($options as $option => $optioncount) {
                        echo $option."| (".$optioncount.") ";
                    }
                    echo "\n";
                    //dd("Laz");
                }
                $correct_value = array_search(max($options),$options);

                foreach ($urs as $ur) {
                    if ($correct_value != '') {
                        $ur->$corrected_field = $correct_value;
                        $ur->rowProcessed('FillBlankFields', ['fix' => 'Filled blank '.$field,
                                                'old' => '',
                                                'new' => $correct_value]);
                        
     
                        $ur->save();
                    }

                    $numcorrected++;
                    //echo $account_id." corrected ".$field."\n";
                }
                
            } else {
                echo "$account_id: Cannot correct the ".$field." because no readings have it.\n";
                //dd($to_correct, $correct_values);
            }
            //$ur->$corrected_field = 
        }
        echo $account_id." corrected $numcorrected readings.\n";

        $this->endCrunch($numcorrected);
    }
}
