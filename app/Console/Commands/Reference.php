<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AccountReference;
use App\UsageReading;
use App\MasterMonthlyAccount;
use App\City;
use App\Traits\CrunchTrait;
use Carbon\Carbon;

class Reference extends Command
{
    use CrunchTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crunch:reference {--city=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Accounts reference table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cities = City::orderBy('name')->get();
        if ($this->option('city')) {
            $cities = City::where('name', $this->option('city'))->get();
        }

        foreach ($cities as $city) {

            $city_name = $this->startCrunch('reference', $city->name);

            $account_ids = UsageReading::groupBy('account_id')->pluck('account_id');
            echo "$city_name has ".$account_ids->count()." accounts with usage readings\n";

            $lookup = [];

            $start_time = microtime(-1);
            foreach ($account_ids as $loopkey => $account_id) {
                $lookup[$account_id] = 1;
                if ($loopkey % 100 == 0) {
                    echo "    ".$loopkey."\r";
                }
                $urs = UsageReading::where('account_id', $account_id)->get();
                $total_count = $urs->count();
                $valid_count = 0;
                $later_date = Carbon::parse('2000-01-01');
                $early_date = Carbon::today();
                foreach ($urs as $ur) {
                    if ($ur->usage_to_date != null && $ur->usage_kwh != null) {
                        $valid_count++;
                    }
                    if ($early_date > $ur->usage_to_date) {
                        $early_date = $ur->usage_to_date;
                    }
                    if ($later_date < $ur->usage_to_date) {
                        $later_date = $ur->usage_to_date;
                    }
                }
                if ($early_date == Carbon::today()) {
                    $early_date = null;
                }
                if ($later_date == Carbon::parse('2000-01-01')) {
                    $later_date = null;
                }
                //dd($total_count, $valid_count, $early_date, $later_date);
                $reference = AccountReference::where('account_id', $account_id)->first();
                if (!$reference) {
                    $reference = new AccountReference;
                    $reference->account_id = $account_id;
                    $reference->city_id = $city->id;
                    $reference->city = $city->name;
                }
                $reference->earliest = $early_date;
                $reference->latest = $later_date;
                $reference->total_count = $total_count;
                $reference->valid_count = $valid_count;
                
                $city_counts = [];
                if ($reference->city_counts) {
                    $city_counts = $reference->city_counts;
                }
                $city_counts[$city->name] = $total_count;
                $reference->city_counts = $city_counts;

                if (count($city_counts) > 1) {
                    $reference->multiple_cities = true;
                }

                if ($reference->multiple_cities) {
                    $reference->total_count = array_sum($reference->city_counts);
                }

                $reference->counted_at = Carbon::now();
                $reference->save();
            }
            echo "GET: ".(microtime(-1) - $start_time)."\n";

            $start_time = microtime(-1);
            $master_ids = MasterMonthlyAccount::groupBy('account_id')->pluck('account_id');
            echo "Processing ".$master_ids->count()." master accounts\n";
            $master_only = 0;
            foreach ($master_ids as $loopkey => $master_id) {
                if ($loopkey % 100 == 0) {
                    echo "    ".$loopkey."\r";
                }
                if (isset($lookup[$master_id])) {
                    continue;
                }
                $master_only++;
                $reference = AccountReference::where('account_id', $account_id)->first();
                if ($reference) {
                    continue;
                }

                $reference = new AccountReference;
                $reference->account_id = $account_id;
                $reference->city_id = $city->id;
                $reference->city = $city->name;
                $reference->save();
            }

            echo "MASTER: (".$master_only.")".(microtime(-1) - $start_time)."\n";
        }
    }
}
