<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('crunch:rename')->hourly();
        $schedule->command('crunch:upload')->hourly();
        $schedule->command('crunch:import')->hourly();

        $schedule->command('crunch:sales')->cron('0 */4 * * *');
        $schedule->command('crunch:commission')->cron('0 */4 * * *');

        $schedule->command('crunch:import --master')->daily();
        $schedule->command('crunch:it_all')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
