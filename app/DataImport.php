<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\SalesUsageReading;
use App\CommissionUsageReading;
use App\MasterMonthlyAccount;

use Carbon\Carbon;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception as SpreadsheetReaderException;


class DataImport extends Model
{
    protected $dates = ['month', 'updated_at', 'created_at'];
    protected $casts = ['counts' => 'array', 'sheets' => 'array'];

  public function setDBConnection($connection) 
  {
    $this->connection = $connection;
  }
  public function city()
  {
    return $this->belongsTo(City::class);
  }


  

  public function getSheetNames()
  {
      if (($this->file_type == 'xls') || ($this->file_type == 'xlsx')) {

          $sheet_names = [];

          try {
              // Determine the reader to use based on the file type
              $reader = IOFactory::createReaderForFile($this->full_file_path);

              //dd($reader);
              // Load the spreadsheet file using the determined reader
              $spreadsheet = $reader->load($this->full_file_path);

              // Get the sheet names
              $sheet_names = $spreadsheet->getSheetNames();

              $num_sheets = count($sheet_names);

              if ($num_sheets == 1) {
                  $this->which_sheet = 1;
              }

              $this->sheets = $sheet_names;
              $this->sheet_count = $num_sheets;
              $this->save();
          } catch (SpreadsheetReaderException $e) {
              // Handle the error (e.g., log the error, set an error message, etc.)
              error_log('Error loading file: ' . $e->getMessage());
              // Optionally, you can set default values or handle the error as needed
              $this->sheets = [];
              $this->sheet_count = 0;
          } catch (\Exception $e) {
              // Handle any other exceptions
              error_log('Error: ' . $e->getMessage());
              $this->sheets = [];
              $this->sheet_count = 0;
          }
      }
  }



  public function getSheetNamesOLD()
  {
      // From https://github.com/rap2hpoutre/fast-excel/issues/74

      if(($this->file_type == 'xls') || ($this->file_type == 'xlsx')) {

          $sheet_names = [];

          $reader = ReaderFactory::create(Type::XLSX);

          $reader->open($this->full_file_path);
          //dd($reader);

          foreach($reader->getSheetIterator() as $sheet)  {
            array_push($sheet_names,$sheet->getName());
          }

          $num_sheets = count($sheet_names);

          if ($num_sheets == 1) {
            $this->which_sheet = 1;
          }

          $this->sheets = $sheet_names;
          $this->sheet_count = $num_sheets;
          $this->save();
      }
  }
  public function getSheetCounts()
  {
    if(($this->file_type == 'xls') || ($this->file_type == 'xlsx')) {

          $sheet_counts = [];

          $reader = ReaderFactory::create(Type::XLSX);
          $reader->open($this->full_file_path);

          foreach($reader->getSheetIterator() as $sheet)  {
            $lastrow = $sheet->getHighestRow();
            dd($lastrow);
            array_push($sheet_counts,$lastrow);
          }

          dd($sheet_counts);
      }
  }

  public function reset()
  {
      $this->import_done = false;
      $this->count = null;
      $this->save();
  }

  public function importIsDone()
  {
      $this->import_done = true;
      $this->save();
  }

  public function setImportCount($count)
  {
      $this->count = $count;
      $this->save();
  }


    public function getShortNameAttribute()
    {
      $type = $this->type;
      if ($type == 'sales') {
        $type = 'usage';
      }
      if ($this->month) {
        $month = $this->month->format('M_Y');
      } else {
        $month = 'MULTIMONTH';
      }
      return strtoupper($this->dataFolder->name."_".$month."_".$type);
    }
  public function commissionUsageReadings()
  {
      return $this->hasMany(CommissionUsageReading::class, 'usage_file_id');
  }
  public function salesUsageReadings()
  {
      return $this->hasMany(SalesUsageReading::class, 'usage_file_id');
  }
  public function appendixUsageReadings()
  {
      return $this->hasMany(UsageReading::class, 'usage_file_id');
  }
  public function usageReadings()
  {
        if ($this->type == 'sales') {
            return $this->hasMany(SalesUsageReading::class, 'usage_file_id');
        }
        if ($this->type == 'commission') {
            return $this->hasMany(CommissionUsageReading::class, 'usage_file_id');
        }
  }
    public function firstDate()
    {
        $first = $this->usageReadings()->whereNotNull('usage_to_date')->orderBy('usage_to_date')->first();
        if ($first) {
            return $first->usage_to_date;
        }
    }
    public function lastDate()
    {
        return $this->usageReadings()->orderBy('usage_to_date', 'desc')->first()->usage_to_date;
    }

    public function datafolder() {
        return $this->belongsTo(DataFolder::class,'data_folder_id');
    }

    public function issues()
    {
        if ($this->type == 'commission')    { $model = 'App\CommissionUsageReading';    }
        if ($this->type == 'sales')         { $model = 'App\SalesUsageReading';         }
        if ($this->type == 'master')        { $model = 'App\MasterMonthlyAccount';      }
        if ($this->type == 'historical')    { $model = 'App\UsageReading';              }

        $issues = $model::where('usage_file_id',$this->id)->whereNotNull('issues')->get();

        return $issues;
    }

    public function issuesCount()
    {
        if(!$this->type) {

              return '';

        } else {

            if ($this->type == 'commission')    { $model = 'App\CommissionUsageReading';    }
            if ($this->type == 'sales')         { $model = 'App\SalesUsageReading';         }
            if ($this->type == 'master')        { $model = 'App\MasterMonthlyAccount';      }
            if ($this->type == 'historical')    { $model = 'App\UsageReading';              }

            $issues = $model::where('usage_file_id',$this->id)->whereNotNull('issues')->count();

            return $issues;
            
        }
    }

    public function getFullFilePathAttribute()
    {
      if (strpos('a'.$this->file_path, 'data_files') > 0) {
        $temparr = explode('data_files', 'a'.$this->file_path);
        $folderpath = $temparr[1];
        return storage_path('app/data_files'.$folderpath);
      }
      $processing_folder = storage_path(config('app.processing_folder'));
      return $processing_folder."/".$this->name;
    }

    public function getShortFilePathAttribute()
    {
    	return $this->full_file_path;
    }

    public function suggestedMonthAndDetectedDate()
    {
      $suggested_month    = '';
      $detected_date      = '';

      try {

      if (preg_match("/\d{1,2}\-\d{1,2}\-\d{2}/", $this->name, $matches)) {
         $suggested_month = Carbon::createFromFormat("m-d-y",$matches[0])->format("F Y");
         $detected_date      = $matches[0];
      }
      if (preg_match("/\d{4}\-\d{2}/", $this->name, $matches)) {
         $suggested_month = Carbon::createFromFormat("Y-m",$matches[0])->format("F Y");
         $detected_date      = $matches[0];
      }
      elseif (preg_match("/\d{1,2}\-\d{1,2}\-\d{4}/", $this->name, $matches)) {
         $suggested_month = Carbon::createFromFormat("m-d-Y",$matches[0])->format("F Y");
         $detected_date      = $matches[0];
      }
      elseif (preg_match("/\d{4}\-\d{1,2}\-\d{4}/", $this->name, $matches)) {
         $suggested_month = Carbon::createFromFormat("Y-m-d",$matches[0])->format("F Y");
         $detected_date      = $matches[0];
      }
      elseif (preg_match("/\_20\d{4}/", $this->name, $matches)) {
         $suggested_month = Carbon::createFromFormat("_Ym",$matches[0])->format("F Y");
         $detected_date      = $matches[0];
      }
      elseif (preg_match("/(January|February|March|April|May|June|July|August|September|October|November|December)\_\d{4}/", $this->name, $matches)) {
         $suggested_month = Carbon::createFromFormat("F_Y",$matches[0])->format("F Y");
         $detected_date      = $matches[0];
      }
      elseif (preg_match("/(January|February|March|April|May|June|July|August|September|October|November|December)\s\d{4}/", $this->name, $matches)) {
         $suggested_month = Carbon::createFromFormat("F Y",$matches[0])->format("F Y");
         $detected_date      = $matches[0];
      }
      elseif (preg_match("/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\_\d{4}/", $this->name, $matches)) {
         $suggested_month = Carbon::createFromFormat("F_Y",$matches[0])->format("F Y");
         $detected_date      = $matches[0];
      }
      elseif (preg_match("/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\-\d{4}/", $this->name, $matches)) {
         $suggested_month = Carbon::createFromFormat("F-Y",$matches[0])->format("F Y");
         $detected_date      = $matches[0];
      }
      elseif (preg_match("/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s\d{4}/", $this->name, $matches)) {
         $suggested_month = Carbon::createFromFormat("F Y",$matches[0])->format("F Y");
         $detected_date      = $matches[0];
      } 
      elseif (preg_match("/\s\d{6}(\.)/", $this->name, $matches)) {
        $year = substr($matches[0], 1, 2);
        $month = substr($matches[0], 3, 2);
        //dd($year, $month);
         if ($year > 17 && $year < 24) {
          if ((int)$month < 13) {
            $suggested_month = Carbon::createFromFormat("y-m",$year."-".$month)->format("F Y");
            $detected_date      = $matches[0];
            //dd($suggested_month);
          }
         }
      } 
      elseif (preg_match("/_\d{4}(\.|\_)/", $this->name, $matches)) {
        //dd($matches[0]);
         $year = substr($matches[0], 1, 2);
         $month = substr($matches[0], 3, 2);

         if ($year > 17 && $year < 24) {
          if ((int)$month < 13) {
            $suggested_month = Carbon::createFromFormat("y-m",$year."-".$month)->format("F Y");
            $detected_date      = $matches[0];
            //dd($suggested_month);
          }
         }
         
      
      } else {
        $suggested_month    = 'none';
        $detected_date      = 'none';
      }

      if ($suggested_month != 'none') {
              
          $suggested_month_formatted = Carbon::parse($suggested_month)->format("Y-m-d");

          return ['suggested_month' => $suggested_month, 
                  'suggested_month_formatted' => $suggested_month_formatted, 
                  'detected_date' => $detected_date];

      } else {

          return ['suggested_month' => null, 
                  'suggested_month_formatted' => null, 
                  'detected_date' => null];
      }

    } catch (\Exception $e) {
      dd($e->getMessage(), $matches);
    }



    }

    public function updateCount()
    {
        // $this->count = 0;
        // $this->save();
        return $this->count;
    } 


    public function getIcon()
    {
    	if(!$this->hastable) {
    		switch (pathinfo($this->file_path, PATHINFO_EXTENSION)) {
    			case 'xlsx':
    			return 'fas fa-file-excel';
    			break;

    			case 'xls':
    			return 'fas fa-file-excel';
    			break;

    			case 'csv':
    			return 'fas fa-file-csv';
    			break;

    			default:
    			return 'fas fa-file-alt';
    		}
    	} else {
    		return 'fas fa-table';
    	}

    }
}
