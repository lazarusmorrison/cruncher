<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// class MasterMonthlyAccount extends Model
class MasterMonthlyAccount extends BaseUsage
{
    protected $dates = ['month', 
    					'updated_at', 
    					'created_at', 
    					'account_start_date', 
    					'account_end_date'];

    protected $casts = ['all_fields' => 'array', 
    					'address'    => 'array'];

    protected $fillable = [
                'file_row',
                'usage_file_id',
                'city',
                'row_hash',
                'processes', 'issues',
                'account_id',
                'customer_name',
                'rate_class',
                'base_rate_class',
                'address',
                'all_fields',
                // 'usage_from_date',
                // 'usage_to_date',
                // 'usage_kwh',
                // 'ldc_read_month',
                'read_cycle',
                // 'icap'
                'month',
                'account_status'
            ];


    public function usageFile()
    {
    	return $this->dataImport();
    }
    public function dataImport()
    {
        // to replace usageFile() ?
        return $this->belongsTo(Models\DataImport::class,'usage_file_id','id');
    }

    public function accountSalesUsage()
    {
        return $this->hasMany(SalesUsageReading::class, 'account_id', 'account_id');
    }
    public function accountCommissionUsage()
    {
        return $this->hasMany(CommissionUsageReading::class, 'account_id', 'account_id');
    }

    public function dataFolder()
    {
        // to replace usageFile() ?
        return $this->belongsTo(Models\DataImport::class,'usage_file_id','id');
    }
}
