<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseUsage extends Model
{
    protected $dates = ['updated_at', 
                        'created_at', 
                        'usage_from_date', 
                        'usage_to_date', 
                        'account_start_date', 
                        'account_end_date', 
                        'ldc_read_month',
                        'corrected_ldc_read_month',
                        'primary_month'];

    protected $casts = ['address' => 'array', 'all_fields' => 'array'];

    protected $fillable = [
                'file_row',
                'usage_file_id',
                'city',
                'row_hash',
                'processes', 'issues',
                'account_id',
                'customer_name',
                'rate_class',
                'base_rate_class',
                'address',
                'all_fields',
                'usage_from_date',
                'usage_to_date',
                'usage_kwh',
                'ldc_read_month',
                'read_cycle',
                'icap'
            ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }
    
    public function rowProcessed($process, $correction=null)
    {
        $list = json_decode($this->processes,true);

        $add = ['process' => $process];
        if ($correction) {
            $add = array_merge($add, $correction);
        }
        $list[] = $add;

        $this->processes = json_encode($list);
        if (!$this->corrected_icap) {
            $this->corrected_icap = null;
        }
        try {
            $this->save();
        } catch (\Exception $e) {
            //dd($this);
            echo $this->account_id." ".$e->getMessage();
        }
    }
    public function serviceClass()
    {
        return $this->belongsTo(ServiceClass::class);
    }
    public function scopeIgnoreCorrections($query)
    {
        return $query->where('correction', false);
    }
    public function scopeCorrections($query)
    {
        return $query->where('correction', true);
    }
    public function scopeIgnoreContradictory($query)
    {
        return $query->where('contradictory', false);
    }
    public function scopeContradictory($query)
    {
        return $query->where('contradictory', true);
    }
    public function setTable($table)
    {
        $this->table = $table;
    }
            
    public function usageFile()
    {
        return $this->dataImport();
    }
    public function dataImport()
    {
        // to replace usageFile() ?
        return $this->belongsTo(DataImport::class,'usage_file_id','id');
    }

    public function dataFolder()
    {
        // to replace usageFile() ?
        return $this->belongsTo(DataImport::class,'usage_file_id','id');
    }

    public function accountSalesUsage()
    {
        return $this->hasMany(SalesUsageReading::class, 'account_id', 'account_id');
    }
    public function accountCommissionUsage()
    {
        return $this->hasMany(CommissionUsageReading::class, 'account_id', 'account_id');
    }
    public function masterMonthlies()
    {
        return $this->hasMany(MasterMonthlyAccount::class, 'account_id', 'account_id');
    }

    public function scopeInRange($query, $from, $to)
    {
        return $query->where('usage_to_date', '>=', $from)
                     ->where('usage_to_date', '<=', $to);
    }
    public function save(array $options = Array()) 
    {
        $acceptable_rate_classes = ['R1', 'R2', 'R3', 'R4', 'R5', 'R6',
                                    'G0', 'G1', 'G2', 'G3', 'G4', 'G5', 
                                    'S1', 'S2', 'S3', 'S4', 'S5',
                                    'T0', 'T1', 'T2',
                                    '23', '24'];

        if (!in_array($this->corrected_rate_class, $acceptable_rate_classes)) {
            // They are simply no longer allowed anywhere unless they are good.
            $this->corrected_rate_class = null;
        }
        $this->account_id = str_pad($this->account_id, 10, '0', STR_PAD_LEFT);
        return parent::save($options);
    }
}
