<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public $incrementing = false;
    protected $casts = ['id' => 'string'];
    
    public function generateAnonymousIdAttribute()
    {
        if (strlen($this->anonymized_id) != (strlen($this->city) + 10)) {
            $city = ucwords($this->city);
            $anon_id = substr(md5(str_pad($this->id, 10, "0", STR_PAD_LEFT)), 0, 9);
            $this->anonymized_id = $city."_".$anon_id;
        }
    }
    public function getAnonymousIdAttribute()
    {
        return $this->anonymized_id;
    }
    public function usageReadings()
    {
        return $this->hasMany(UsageReading::class);
    }
    public function usageReadingsByIcapYear($year)
    {
        $start_date = ($year).'-06-01';
        $end_date = ($year+1).'-05-01';
        return $this->usageReadings()
                    ->where('corrected_ldc_read_month', '>=', $start_date)
                    ->where('corrected_ldc_read_month', '<=', $end_date);
    }
    public function salesUsageReadings()
    {
        return $this->hasMany(SalesUsageReading::class)->orderBy('usage_to_date');
    }
    public function commissionUsageReadings()
    {
        return $this->hasMany(CommissionUsageReading::class)->orderBy('usage_to_date');
    }
    public function masterMonthlies()
    {
        return $this->hasMany(MasterMonthlyAccount::class)->orderBy('month');
    }
    public function matches()
    {
    	return $this->hasMany(ReadingMatch::class);
    }
    public function save(array $options = Array()) 
    {
        $this->generateAnonymousIdAttribute();
        return parent::save($options);
    }
}
