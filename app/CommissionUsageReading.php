<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommissionUsageReading extends BaseUsage
{
    protected $table = 'commission_usage_readings';
}
