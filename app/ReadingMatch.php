<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReadingMatch extends Model
{
	protected $dates = ['date', 'updated_at', 'created_at'];
	
    public function salesUsageReading()
    {
    	return $this->belongsTo(SalesUsageReading::class);
    }
    public function commissionUsageReading()
    {
    	return $this->belongsTo(CommissionUsageReading::class);
    }
}
