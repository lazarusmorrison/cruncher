<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountReference extends Model
{
	protected $connection = 'baystate-shared';
    protected $dates = ['created_at', 'updated_at', 'counted_at'];
    protected $casts = ['city_counts' => 'array'];
}
