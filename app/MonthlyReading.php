<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlyReading extends Model
{
    public function usageReading()
    {
    	return $this->belongsTo(UsageReading::class);
    }
}
