<?php

namespace App\Traits;

use Illuminate\Support\Str;
use Carbon\Carbon;


trait FileTrait {

    public $problem_dir;
    public $ready_dir;
    public $synced_dir;

    public function createNecessaryDirectories()
    {
        $this->problem_dir  = 'uploads/problem/';
        $this->special_dir  = 'uploads/special/';
        $this->ready_dir    = 'uploads/ready/';
        $this->synced_dir   = 'uploads/synced/';
        $this->cache_file   = storage_path().'/app/uploads/cache.json';

        foreach($this->getCities() as $city) {
            $this->createDirectoryIfNotExists($this->ready_dir.$city);
            $this->createDirectoryIfNotExists($this->synced_dir.$city);
        }

        $this->createDirectoryIfNotExists($this->problem_dir);
        $this->createDirectoryIfNotExists($this->special_dir);

        if (!file_exists($this->cache_file)) {
            file_put_contents($this->cache_file, json_encode($array = []));
        }
    }

    public function createDirectoryIfNotExists($dir)
    {
         $full_dir = storage_path().'/app/'.$dir;

        if (!file_exists($full_dir)) {
            mkdir($full_dir, 0777, true);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public function getFileNameFromUUID($uuid, $dir = null)
    {

        $dir = (!$dir) ? 'problem' : $dir;
        $search = storage_path().'/app/uploads/'.$dir.'/'.$uuid;
        $files = glob($search.'*');
        $name = trim($files[0]);
        $name = str_replace($search, '', $name);
        return trim($name);
    }


    public function getFullFileNameFromUUID($uuid, $dir = null)
    {
        $dir = (!$dir) ? 'problem' : $dir;
        $search = storage_path().'/app/uploads/'.$dir.'/'.$uuid;
        $files = glob($search.'*');
        $name = trim($files[0]);
        return trim($name);
    }

    public function GetUUID($file)
    {
        return substr($file, 0, 8);
    }

    public function createUUID()
    {
        while(true) {
            $uuid = (string) Str::uuid();
            $uuid = substr($uuid, 0, 8);

            if (!glob(storage_path().'/app/'.$this->problem_dir.$uuid.'*')
                && !glob(storage_path().'/app/'.$this->special_dir.$uuid.'*')) {
                return $uuid;
            }
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////

    public function getCache($full_file_name)
    {
        $cache = json_decode(file_get_contents($this->cache_file), true);
        if (isset($cache[$full_file_name])) {
            return $cache[$full_file_name];
        } else {
            return false;
        }
    }

    public function putCache($full_file_name, $data)
    {
        $cache = json_decode(file_get_contents($this->cache_file), true);
        $cache[$full_file_name] = $data;
        file_put_contents($this->cache_file, json_encode($cache));
    }

    public function clearCacheKey($old, $new = null)
    {
        $cache = json_decode(file_get_contents($this->cache_file), true);
        if ($new) {
            $cache[$new] = $cache[$old];
        }
        unset($cache[$old]);
        file_put_contents($this->cache_file, json_encode($cache));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public function getFiles($dir, $refresh_cache = false)
    {
        $path = storage_path().'/app/'.$dir;
        $files = scandir($path);
        $dataArray = [];
        foreach($files as $f => $file) {
            if ($file == '.') continue;
            if ($file == '..') continue;

            $full_name = $path.$file;

            $cached = $this->getCache($full_name);

            if ($cached && !$refresh_cache) {

                $count = $cached['count'];
                $filesize = $cached['size'];

            } else {

                try {
                    $count = count(file($path.$file));
                } catch (\Exception $e) {
                    $count = null;
                }

                try {
                    $filesize = filesize($path.$file);
                    $filesize = round($filesize/1000000, 2);
                } catch (\Exception $e) {
                    $filesize = null;
                }

                $this->putCache($full_name, ['count' => $count, 'size' => $filesize]);

            }

            try {
                $date = Carbon::parse(filemtime($path.$file))->toDateTimeString();
            } catch (\Exception $e) {
                $date = null;
            }

            if (
                stripos($dir, $this->problem_dir) !== false
                || stripos($dir, $this->special_dir) !== false
               ) {
                $uuid = substr($file, 0, 8);
                // $file = trim(substr($file, 8));
            } else {
                $uuid = null;
            }

            $name_ok = $this->fileNameHasEverything($file);

            $has_city = $this->getCityFromFileName($file);
            $has_type = $this->getTypeFromFileName($file);
            $has_date = $this->getDateFromFileName($file);
            $has_ext = $this->checkValidFileExtensionFromFileName($file);

            $dataArray[$f] = ['uuid'        => $uuid,
                              'name'        => $file,
                              'full_name'   => $full_name, 
                              'lines'       => $count, 
                              'size'        => $filesize, 
                              'date'        => $date,
                              'name_ok'     => $name_ok,
                              'has_city'    => $has_city,
                              'has_type'    => $has_type,
                              'has_date'    => $has_date,
                              'has_ext'     => $has_ext,
                            ];
        }
        return $dataArray;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

	public function fileNameHasEverything($file_name)
	{
        $city       = $this->getCityFromFileName($file_name);
        $type       = $this->getTypeFromFileName($file_name);
        $date       = $this->getDateFromFileName($file_name);
  		$extension  = $this->checkValidFileExtensionFromFileName($file_name);
  		
        if ($city && $city == $this->city && $type && $date && $extension) {
			return true;
		}

		return false;
	}

    public function checkValidFileExtensionFromFileName($file_name)
    {
        $extension = pathinfo($file_name, PATHINFO_EXTENSION);
        if (in_array(strtolower($extension), ['xlsx', 'xls', 'csv'])) {
            $extension = true;
        } else {
            $extension = false;
        }
        return $extension;
    }

	public function getCityFromFileName($file_name)
    {
    	$cities = $this->getCities();

        foreach($cities as $city) {
            if (stripos($file_name, $city) !== false) {
                return $city;
            }
        }

        return null;
    }

    public function getTypeFromFileName($file_name)
    {
        foreach(['commission', 'usage', 'master'] as $type) {
            if (stripos($file_name, $type) !== false) {
                return $type;
            }
        }

        return $this->extractType($file_name);
    }

    public function getDateFromFileName($file_name)
    {
        // $pattern = "/".'([0-9]{6})'."/";
        // $match = preg_match($pattern, $file_name);

        return $this->extractDate($file_name);
    }

    public function extractType($filename)
    {
        $type = null;
        if (stripos($filename, 'commission') !== false
                    || stripos($filename, 'combined') !== false 
                    || stripos($filename, '30.Peregrine Energy Group') !== false
                    || stripos($filename, '_COM_') !== false) 
            {  $type = 'COM'; }

        if (stripos($filename, 'payment report') !== false 
                    || stripos($filename, '_COM_') !== false) 
            {  $type = 'COM'; }

        //dd($filename, $type);

        if (stripos($filename, 'master') !== false 
                        || stripos($filename, 'monthly_report') !== false
                        || stripos($filename, 'AccountDetails') !== false
                        || stripos($filename, 'Account Details') !== false
                        || stripos($filename, 'Accounts') !== false
                        || stripos($filename, '_ACC_') !== false) 
            { $type = 'ACC'; }
        
        if (stripos($filename, 'sales') !== false 
                    || stripos($filename, 'aggregation') !== false
                    || stripos($filename, '_USE_') !== false
                    || stripos($filename, 'Peregrine_Energy_Group_Lincoln') !== false
                    || stripos($filename, 'Peregrine_Energy_Group.Lincoln') !== false
                    || stripos($filename, 'monthly report') !== false) 
            { $type = 'USE'; }
        
        if (stripos($filename, 'usage') !== false) 
            { $type = 'USE'; }
        
        if (stripos($filename, 'historical') !== false) 
            { $type = 'HIS'; }

        return $type;
    }

    public function extractDate($filename) {
        $file_month = null;
        $suggested_month = null;
        
        try {

          if (preg_match("/\d{1,2}\-\d{1,2}\-\d{2}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("m-d-y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
             //dd($filename, $detected_date, $suggested_month);
          }
          elseif (preg_match("/\s+\d{6}(\.)/", $filename, $matches)) {
            //dd($matches[0]);
             $year = substr(trim($matches[0]), 4, 2);
             $month = substr(trim($matches[0]), 0, 2);

             if ($year > 17 && $year < 24) {
              if ((int)$month < 13) {
                $suggested_month = Carbon::createFromFormat("y-m",$year."-".$month)->format("F Y");
                $detected_date      = $matches[0];
                //dd($suggested_month);
              }
             }
             
          
          }
          elseif (preg_match("/\d{4}\-\d{2}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("Y-m",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\d{6}\_/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("Ym_",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\_\d{6}\./", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("_mdy.",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\d{1,2}\.\d{4}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("m.Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\s\d{1}\.\d{1,2}\.\d{2}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("n.j.y",trim($matches[0]))->format("F Y");
             $detected_date      = trim($matches[0]);
             //dd($detected_date, $suggested_month);
          }
          elseif (preg_match("/\d{2}\.\d{2}\.\d{2}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("m.d.y",trim($matches[0]))->format("F Y");
             $detected_date      = trim($matches[0]);
             //dd($detected_date, $suggested_month);
          }
          elseif (preg_match("/\d{1,2}\-\d{1,2}\-\d{4}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("m-d-Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\d{4}\-\d{1,2}\-\d{4}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("Y-m-d",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\d{1,2}\.\d{1,2}\.\d{4}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("m.d.Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\_20\d{4}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("_Ym",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/(January|February|March|April|May|June|July|August|September|October|November|December)\_\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F_Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/(January|February|March|April|May|June|July|August|September|October|November|December)\.\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F.Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/(January|February|March|April|May|June|July|August|September|October|November|December)\s\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/(January|February|March|April|May|June|July|August|September|October|November|December)\s\s\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F  Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Sept|Oct|Nov|Dec)\_\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F_Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Sept|Oct|Nov|Dec)\s\s\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F  Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }

          elseif (preg_match("/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Sept|Oct|Nov|Dec)\-\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F-Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Sept|Oct|Nov|Dec)\s\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          } 
          elseif (preg_match("/\s\d{6}(\.)/", $filename, $matches)) {
            $year = substr($matches[0], 1, 2);
            $month = substr($matches[0], 3, 2);
            //dd($year, $month);
             if ($year > 17 && $year < 24) {
              if ((int)$month < 13) {
                $suggested_month = Carbon::createFromFormat("y-m",$year."-".$month)->format("F Y");
                $detected_date      = $matches[0];
                //dd($suggested_month);
              }
             }
          } 
          elseif (preg_match("/_\d{4}(\.|\_)/", $filename, $matches)) {
            //dd($matches[0]);
             $year = substr($matches[0], 1, 2);
             $month = substr($matches[0], 3, 2);

             if ($year > 17 && $year < 24) {
              if ((int)$month < 13) {
                $suggested_month = Carbon::createFromFormat("y-m",$year."-".$month)->format("F Y");
                $detected_date      = $matches[0];
                //dd($suggested_month);
              }
             }
             
          
          }
          
          elseif (preg_match("/_\d{2}(\.|\_)\d{2}(\.|\_)\d{4}/", $filename, $matches)) {
            //dd($matches[0]);
             $year = substr($matches[0], 7, 4);
             $month = substr($matches[0], 1, 2);
             //dd($year, $month);
             if ($year > 17 && $year < 24) {
              if ((int)$month < 13) {
                $suggested_month = Carbon::createFromFormat("y-m",$year."-".$month)->format("F Y");
                $detected_date      = $matches[0];
                //dd($suggested_month);
              }
             }
             
          
          } else {
            $suggested_month    = 'none';
            $detected_date      = 'none';
          }

          //dd($suggested_month);

          if ($suggested_month != 'none') {
                  
              $file_month = Carbon::parse($suggested_month)->format("Y-m");

              
          }

        } catch (\Exception $e) {
          dd($e->getMessage(), $matches, $filename);
        }
        return $file_month;
    }    
}