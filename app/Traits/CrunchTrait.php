<?php

namespace App\Traits;
use DB;
use App\Crunch;

trait CrunchTrait {

	public $crunch;

	public function startCrunch($crunch_name, $option_city)
	{
		if ($option_city) {
            $city = strtolower($option_city);
            $db = 'bsc_'.$city;
            if (!$this->databaseExists($db)) {
                if (config('app.env') == 'production') {
                    dd("City DB Missing: ".$db);
                } else {
                    return null;
                }
                
            }
        } else {
            dd("Forgot city dummy, like php artisan crunch:$crunch_name --city=natick");
        }
        DB::purge('mysql');
        config(['database.connections.mysql.database' => $db]);

        echo "CRUNCHING ".strtoupper($option_city)." ".strtoupper($crunch_name)."\n";

        $crunch = new Crunch;
        $crunch->name = $crunch_name;
        $crunch->city = $city;
        $this->crunch = $crunch;

        return $option_city;
	}
	public function endCrunch($affected = null)
	{
		if ($this->crunch->log) {
			if ($affected) {
				$this->crunch->affected = $affected;
			}
        	$this->crunch->complete();
      	}
	}
    public function databaseExists($db)
    {
        //dd("LAz2");
        return DB::table('INFORMATION_SCHEMA.SCHEMATA')
                  ->selectRaw("SCHEMA_NAME")
                  ->where("SCHEMA_NAME", $db)->first();
    }
}