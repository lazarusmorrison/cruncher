<?php

namespace App\Traits;
use DB;
use App\Crunch;
use Carbon\Carbon;

trait HandyTrait {

    public function extractDate($filename) {
    	$file_month = null;
        try {

          if (preg_match("/\d{1,2}\-\d{1,2}\-\d{2}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("m-d-y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
             //dd($filename, $detected_date, $suggested_month);
          }
          elseif (preg_match("/\d{4}\-\d{2}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("Y-m",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\s+(0|1)\d{3}2\d{1}\_/", $filename, $matches)) {
            //dd($matches[0]);
             $month = substr(trim($matches[0]), 0, 2);
             $year = substr(trim($matches[0]), 4, 2);
             

             if ($year > 17 && $year < 24) {
              if ((int)$month < 13) {
                $suggested_month = Carbon::createFromFormat("y-m",$year."-".$month)->format("F Y");
                $detected_date      = $matches[0];
                //dd($suggested_month);
              }
             }
             
          
          }
          elseif (preg_match("/\d{6}\_/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("Ym_",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          
          elseif (preg_match("/\_2\d{5}\./", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("_ymd.",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\_0\d{5}\./", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("_mdy.",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\s0\d{5}\./", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("mdy.",trim($matches[0]))->format("F Y");
             $detected_date      = trim($matches[0]);
          }
          elseif (preg_match("/\_\d{6}\./", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("_mdy.",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\s\d{6}\./", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat(" mdy.",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\-\s\d{6}\./", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("- mdy.",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\d{1,2}\.\d{4}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("m.Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\s\d{1}\.\d{1,2}\.\d{2}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("n.j.y",trim($matches[0]))->format("F Y");
             $detected_date      = trim($matches[0]);
             //dd($detected_date, $suggested_month);
          }
          elseif (preg_match("/\d{2}\.\d{2}\.\d{2}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("m.d.y",trim($matches[0]))->format("F Y");
             $detected_date      = trim($matches[0]);
             //dd($detected_date, $suggested_month);
          }
          elseif (preg_match("/\d{1,2}\-\d{1,2}\-\d{4}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("m-d-Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\d{4}\-\d{1,2}\-\d{4}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("Y-m-d",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\d{1,2}\.\d{1,2}\.\d{4}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("m.d.Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/\_20\d{4}/", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("_Ym",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/(January|February|March|April|May|June|July|August|September|October|November|December)\_\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F_Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/(January|February|March|April|May|June|July|August|September|October|November|December)\.\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F.Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/(January|February|March|April|May|June|July|August|September|October|November|December)\s\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/(January|February|March|April|May|June|July|August|September|October|November|December)\s\s\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F  Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Sept|Oct|Nov|Dec)\_\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F_Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Sept|Oct|Nov|Dec)\s\s\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F  Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }

          elseif (preg_match("/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Sept|Oct|Nov|Dec)\-\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F-Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          }
          elseif (preg_match("/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Sept|Oct|Nov|Dec)\s\d{4}/i", $filename, $matches)) {
             $suggested_month = Carbon::createFromFormat("F Y",$matches[0])->format("F Y");
             $detected_date      = $matches[0];
          } 
          elseif (preg_match("/\s\d{6}(\.)/", $filename, $matches)) {
            $year = substr($matches[0], 1, 2);
            $month = substr($matches[0], 3, 2);
            //dd($year, $month);
             if ($year > 17 && $year < 24) {
              if ((int)$month < 13) {
                $suggested_month = Carbon::createFromFormat("y-m",$year."-".$month)->format("F Y");
                $detected_date      = $matches[0];
                //dd($suggested_month);
              }
             }
          } 
          elseif (preg_match("/_\d{4}(\.|\_)/", $filename, $matches)) {
            //dd($matches[0]);
             $year = substr($matches[0], 1, 2);
             $month = substr($matches[0], 3, 2);

             if ($year > 17 && $year < 24) {
              if ((int)$month < 13) {
                $suggested_month = Carbon::createFromFormat("y-m",$year."-".$month)->format("F Y");
                $detected_date      = $matches[0];
                //dd($suggested_month);
              }
             }
             
          
          }
          elseif (preg_match("/_\d{2}(\.|\_)\d{2}(\.|\_)\d{4}/", $filename, $matches)) {
            //dd($matches[0]);
             $year = substr($matches[0], 7, 4);
             $month = substr($matches[0], 1, 2);
             //dd($year, $month);
             if ($year > 17 && $year < 24) {
              if ((int)$month < 13) {
                $suggested_month = Carbon::createFromFormat("y-m",$year."-".$month)->format("F Y");
                $detected_date      = $matches[0];
                //dd($suggested_month);
              }
             }
             
          
          } else {
            $suggested_month    = 'none';
            $detected_date      = 'none';
          }

          //dd($suggested_month);

          if ($suggested_month != 'none') {
                  
              $file_month = Carbon::parse($suggested_month)->format("Y-m");

              
          }

        } catch (\Exception $e) {
          dd($e->getMessage(), $matches, $filename);
        }
        return $file_month;
    }

    public function extractType($city, $filename)
    {
        $type = null;
        if (stripos($filename, 'commission') !== false
                    || stripos($filename, 'combined') !== false 
                    || stripos($filename, '30.Peregrine Energy Group') !== false
                    || stripos($filename, '_COM_') !== false) 
            {  $type = 'COM'; }

        if (stripos($filename, 'payment report') !== false 
                    || stripos($filename, '_COM_') !== false) 
            {  $type = 'COM'; }

        //dd($filename, $type);

        if (stripos($filename, 'master') !== false 
                        || stripos($filename, 'monthly_report') !== false
                        || stripos($filename, 'AccountDetails') !== false
                        || stripos($filename, 'Account Details') !== false
                        || stripos($filename, 'Accounts') !== false
                        || stripos($filename, '_ACC_') !== false) 
            { $type = 'ACC'; }
        
        if (stripos($filename, 'sales') !== false 
                    || stripos($filename, 'aggregation') !== false
                    || stripos($filename, '_USE_') !== false
                    || stripos($filename, 'Peregrine_Energy_Group_Lincoln') !== false
                    || stripos($filename, 'Peregrine_Energy_Group.Lincoln') !== false
                    || stripos($filename, 'monthly report') !== false) 
            { $type = 'USE'; }
        
        if (stripos($filename, 'usage') !== false) 
            { $type = 'USE'; }
        
        if (stripos($filename, 'historical') !== false) 
            { $type = 'HIS'; }

        if (!$type) {
            // Format "City Month Year.xlsx", like in Bellingham
            if (preg_match("/(".$city->name.")\s(January|February|March|April|May|June|July|August|September|October|November|December)\s(\d{4})\.xlsx/i", $filename, $matches)) {
                $type = 'USE';
            }
        }

        return $type;
    }
}